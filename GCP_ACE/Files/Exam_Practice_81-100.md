81. You are operating a Google Kubernetes Engine (GKE) cluster for your company where different teams can run non-production workloads. Your Machine Learning (ML) team needs access to Nvidia Tesla P100 GPUs to train their models. You want to minimize effort and cost. What should you do?

    A. Ask your ML team to add the accelerator: gpu annotation to their pod specification.
    
    B. Recreate all the nodes of the GKE cluster to enable GPUs on all of them.

    C. Create your own Kubernetes cluster on top of Compute Engine with nodes that have GPUs. Dedicate this cluster to your ML team.

    <b>D. Add a new, GPU-enabled, node pool to the GKE cluster. Ask your ML team to add the cloud.google.com/gke -accelerator: nvidia-tesla-p100 nodeSelector to their pod specification.</b>

<i>D. Add a new, GPU-enabled, node pool to the GKE cluster. Ask your ML team to add the cloud.google.com/gke -accelerator: nvidia-tesla-p100 nodeSelector to their pod specification.</i>

82. Your VMs are running in a subnet that has a subnet mask of 255.255.255.240. The current subnet has no more free IP addresses and you require an additional 10 IP addresses for new VMs. The existing and new VMs should all be able to reach each other without additional routes. What should you do?

    <b>A. Use gcloud to expand the IP range of the current subnet.</b>

    B. Delete the subnet, and recreate it using a wider range of IP addresses.

    C. Create a new project. Use Shared VPC to share the current network with the new project.

    D. Create a new subnet with the same starting IP but a wider range to overwrite the current subnet.

<i>A: Expand the existing subnet.
https://cloud.google.com/sdk/gcloud/reference/compute/networks/subnets/expand-ip-range</i>


83. Your organization uses G Suite for communication and collaboration. All users in your organization have a G Suite account. You want to grant some G Suite users access to your Cloud Platform project. What should you do?

    A. Enable Cloud Identity in the GCP Console for your domain.

    <b>B. Grant them the required IAM roles using their G Suite email address.</b>

    C. Create a CSV sheet with all users' email addresses. Use the gcloud command line tool to convert them into Google Cloud Platform accounts.

    D. In the G Suite console, add the users to a special group called cloud-console-users@yourdomain.com. Rely on the default behavior of the Cloud Platform to grant users access if they are members of this group.

<i>B is correct: To actively adopt the Organization resource, the G Suite or Cloud Identity super admins need to assign the Organization Administrator Cloud IAM role to a user or group</i>

84. You have a Google Cloud Platform account with access to both production and development projects. You need to create an automated process to list all compute instances in development and production projects on a daily basis. What should you do?

    <b>A. Create two configurations using gcloud config. Write a script that sets configurations as active, individually. For each configuration, use gcloud compute instances list to get a list of compute resources.</b>

    B. Create two configurations using gsutil config. Write a script that sets configurations as active, individually. For each configuration, use gsutil compute instances list to get a list of compute resources.

    C. Go to Cloud Shell and export this information to Cloud Storage on a daily basis.

    D. Go to GCP Console and export this information to Cloud SQL on a daily basis.

85. You have a large 5-TB AVRO file stored in a Cloud Storage bucket. Your analysts are proficient only in SQL and need access to the data stored in this file. You want to find a cost-effective way to complete their request as soon as possible. What should you do?

    A. Load data in Cloud Datastore and run a SQL query against it.

    B. Create a BigQuery table and load data in BigQuery. Run a SQL query on this table and drop this table after you complete your request.

    <b>C. Create external tables in BigQuery that point to Cloud Storage buckets and run a SQL query on these external tables to complete your request.</b>

    D. Create a Hadoop cluster and copy the AVRO file to NDFS by compressing it. Load the file in a hive table and provide access to your analysts so that they can run SQL queries.

<i>Breaking down the question into key points -


1. 5-TB AVRO file stored in a Cloud Storage bucket.
2. Analysts are proficient only in SQL
3. cost-effective way to complete their request as soon as possible


A. ....Load data in Cloud Datastore... (Not Correct because Cloud Datastore is not a good option to run SQL Queries)

B. ...Load data in BigQuery.... (Not Cost Effective because loading the data which is already present in the bucket into BigQuery again is expensive)

C. Create external tables in BigQuery that point to Cloud Storage buckets and run a SQL query on these external tables to complete your request.
(This is the right answer as it meets all the requirements from the question)

D. Create a Hadoop cluster and copy the AVRO file to NDFS by compressing it. Load the file in a hive table and provide access to your analysts so that they can run SQL queries.
(Too roundabout and indirect. Not the right option)</i>

86. You need to verify that a Google Cloud Platform service account was created at a particular time. What should you do?

    <b>A. Filter the Activity log to view the Configuration category. Filter the Resource type to Service Account.</b>

    B. Filter the Activity log to view the Configuration category. Filter the Resource type to Google Project.

    C. Filter the Activity log to view the Data Access category. Filter the Resource type to Service Account.

    D. Filter the Activity log to view the Data Access category. Filter the Resource type to Google Project.

<i>Correct Answer is A.
Filter the Activity log to view the Configuration category. Filter the Resource type to Service Account.</i>

87. You deployed an LDAP server on Compute Engine that is reachable via TLS through port 636 using UDP. You want to make sure it is reachable by clients over that port. What should you do?

    A. Add the network tag allow-udp-636 to the VM instance running the LDAP server.

    B. Create a route called allow-udp-636 and set the next hop to be the VM instance running the LDAP server.

    <b>C. Add a network tag of your choice to the instance. Create a firewall rule to allow ingress on UDP port 636 for that network tag.</b>

    D. Add a network tag of your choice to the instance running the LDAP server. Create a firewall rule to allow egress on UDP port 636 for that network tag.

<i>C is correct
You tag the instances ,then create ingress firewall rules to allow udp on desired port for target-tags name applied to instances></i>

88. You need to set a budget alert for use of Compute Engineer services on one of the three Google Cloud Platform projects that you manage. All three projects are linked to a single billing account. What should you do?

    <b>A. Verify that you are the project billing administrator. Select the associated billing account and create a budget and alert for the appropriate project.</b>

    B. Verify that you are the project billing administrator. Select the associated billing account and create a budget and a custom alert.

    C. Verify that you are the project administrator. Select the associated billing account and create a budget for the appropriate project.

    D. Verify that you are project administrator. Select the associated billing account and create a budget and a custom alert.

89. You are migrating a production-critical on-premises application that requires 96 vCPUs to perform its task. You want to make sure the application runs in a similar environment on GCP. What should you do?

    <b>A. When creating the VM, use machine type n1-standard-96.</b>

    B. When creating the VM, use Intel Skylake as the CPU platform.

    C. Create the VM using Compute Engine default settings. Use gcloud to modify the running instance to have 96 vCPUs.

    D. Start the VM using Compute Engine default settings, and adjust as you go based on Rightsizing Recommendations.

<i>A is correct - https://cloud.google.com/compute/docs/machine-types</i>


90. You want to configure a solution for archiving data in a Cloud Storage bucket. The solution must be cost-effective. Data with multiple versions should be archived after 30 days. Previous versions are accessed once a month for reporting. This archive data is also occasionally updated at month-end. What should you do?

    A. Add a bucket lifecycle rule that archives data with newer versions after 30 days to Coldline Storage.

    <b>B. Add a bucket lifecycle rule that archives data with newer versions after 30 days to Nearline Storage.</b>

    C. Add a bucket lifecycle rule that archives data from regional storage after 30 days to Coldline Storage.

    D. Add a bucket lifecycle rule that archives data from regional storage after 30 days to Nearline Storage.

<i>Correct Answer (B):

NumberOfNewerVersions
The NumberOfNewerVersions condition is typically only used in conjunction with Object Versioning. If the value of this condition is set to N, an object version satisfies the condition when there are at least N versions (including the live version) newer than it. For a live object version, the number of newer versions is considered to be 0. For the most recent noncurrent version, the number of newer versions is 1 (or 0 if there is no live object version), and so on.

Important: When specifying this condition in a .json configuration file, you must use numNewerVersions instead of NumberOfNewerVersions.

https://cloud.google.com/storage/docs/lifecycle#numberofnewerversions</i>


91. Your company's infrastructure is on-premises, but all machines are running at maximum capacity. You want to burst to Google Cloud. The workloads on Google Cloud must be able to directly communicate to the workloads on-premises using a private IP range. What should you do?

    A. In Google Cloud, configure the VPC as a host for Shared VPC.

    B. In Google Cloud, configure the VPC for VPC Network Peering.

    C. Create bastion hosts both in your on-premises environment and on Google Cloud. Configure both as proxy servers using their public IP addresses.

    <b>D. Set up Cloud VPN between the infrastructure on-premises and Google Cloud.</b>

<i>Correct Answer is (D):

Access internal IPs directly
Your VPC network's internal (RFC 1918) IP addresses are directly accessible from your on-premises network with peering, no NAT device or VPN tunnel required.

Hybrid made easy
Today’s business climate demands flexibility. Connecting your on-premises resources to your cloud resources seamlessly, with minimum latency or interruption, is a business-critical requirement. The speed and reliability of Cloud Interconnect lets you extend your organization’s data center network into Google Cloud, simply and easily, while options such as Cloud VPN provide flexibility for all your workloads. This unlocks the potential of hybrid app development and all the benefits the cloud has to offer.

In the graphic below: What GCP Connection is right for you? shows clearly what is the method for extend your on premise network (IP Private communication).
What GCP Connection is right for you?
https://cloud.google.com/hybrid-connectivity</i>


92. You want to select and configure a solution for storing and archiving data on Google Cloud Platform. You need to support compliance objectives for data from one geographic location. This data is archived after 30 days and needs to be accessed annually. What should you do?

    A. Select Multi-Regional Storage. Add a bucket lifecycle rule that archives data after 30 days to Coldline Storage.

    B. Select Multi-Regional Storage. Add a bucket lifecycle rule that archives data after 30 days to Nearline Storage.

    C. Select Regional Storage. Add a bucket lifecycle rule that archives data after 30 days to Nearline Storage.

    <b>D. Select Regional Storage. Add a bucket lifecycle rule that archives data after 30 days to Coldline Storage.</b>

<i>D
Google Cloud Coldline is a new cold-tier storage for archival data with access frequency of less than once per year. Unlike other cold storage options, Nearline has no delays prior to data access, so now it is the leading solution among competitors.</i>

93. Your company uses BigQuery for data warehousing. Over time, many different business units in your company have created 1000+ datasets across hundreds of projects. Your CIO wants you to examine all datasets to find tables that contain an employee_ssn column. You want to minimize effort in performing this task. What should you do?

    <b>A. Go to Data Catalog and search for employee_ssn in the search box.</b>

    B. Write a shell script that uses the bq command line tool to loop through all the projects in your organization.

    C. Write a script that loops through all the projects in your organization and runs a query on INFORMATION_SCHEMA.COLUMNS view to find the employee_ssn column.

    D. Write a Cloud Dataflow job that loops through all the projects in your organization and runs a query on INFORMATION_SCHEMA.COLUMNS view to find employee_ssn column

<i>A is the correct answer, Data Catalog can be used to search the column with keyword:value pair,

Filter your search by adding a keyword:value to your search terms in the search box:

Keyword Description
name: Match data asset name
***column: Match column name or nested column name
description: Match table description</i>


94. You create a Deployment with 2 replicas in a Google Kubernetes Engine cluster that has a single preemptible node pool. After a few minutes, you use kubectl to examine the status of your Pod and observe that one of them is still in Pending status: What is the most likely cause?

    A. The pending Pod's resource requests are too large to fit on a single node of the cluster.

    B. Too many Pods are already running in the cluster, and there are not enough resources left to schedule the pending Pod.

    C. The node pool is configured with a service account that does not have permission to pull the container image used by the pending Pod.

    D. The pending Pod was originally scheduled on a node that has been preempted between the creation of the Deployment and your verification of the Pods' status. It is currently being rescheduled on a new node.

<i>Correct Answer is (B):

Reasons for a Pod Status Pending:
Troubleshooting Reason #1: Not enough CPU
Troubleshooting Reason #2: Not enough memory
Troubleshooting Reason #3: Not enough CPU and memory
https://managedkube.com/kubernetes/k8sbot/troubleshooting/pending/pod/2019/02/22/pending-pod.html

Pre-emptible would have been an issue if the cluster had more than one node. The question clearly states that it is a single node cluster. That means if that single VM was pre-empted, neither of the pods should have been running. Since one pod is running, that means that (the only) VM is running. So, the reason the second pod is still pending because the VM is not having enough resources to run both the pods. Hence B.
</i>

95. You want to find out when users were added to Cloud Spanner Identity Access Management (IAM) roles on your Google Cloud Platform (GCP) project. What should you do in the GCP Console?

    A. Open the Cloud Spanner console to review configurations.

    B. Open the IAM & admin console to review IAM policies for Cloud Spanner roles.

    C. Go to the Stackdriver Monitoring console and review information for Cloud Spanner.

    D. Go to the Stackdriver Logging console, review admin activity logs, and filter them for Cloud Spanner IAM roles.

<i>Answer = D, I have simple rule; if metrics then Monitoring, if Auditing then Logging. Stackdriver Logging console > admin activity logs > Cloud Spanner IAM role
</i>

96. Your company implemented BigQuery as an enterprise data warehouse. Users from multiple business units run queries on this data warehouse. However, you notice that query costs for BigQuery are very high, and you need to control costs. Which two methods should you use? (Choose two.)

    A. Split the users from business units to multiple projects.

    B. Apply a user- or project-level custom query quota for BigQuery data warehouse.

    C. Create separate copies of your BigQuery data warehouse for each business unit.

    D. Split your BigQuery data warehouse into multiple data warehouses for each business unit.

    E. Change your BigQuery query model from on-demand to flat rate. Apply the appropriate number of slots to each Project.

<i>B & E
Refer below link - first of all you can define quotas on project or user level and 2nd one is you can change from on demand to flat rate model
and define the parameters based on your requirement ---

https://cloud.google.com/bigquery/docs/custom-quotas
https://cloud.google.com/bigquery/pricing#flat_rate_pricing</i>

97. You are building a product on top of Google Kubernetes Engine (GKE). You have a single GKE cluster. For each of your customers, a Pod is running in that cluster, and your customers can run arbitrary code inside their Pod. You want to maximize the isolation between your customers' Pods. What should you do?

    A. Use Binary Authorization and whitelist only the container images used by your customers' Pods.

    B. Use the Container Analysis API to detect vulnerabilities in the containers used by your customers' Pods.

    <b>C. Create a GKE node pool with a sandbox type configured to gvisor. Add the parameter runtimeClassName: gvisor to the specification of your customers' Pods.</b>

    D. Use the cos_containerd image for your GKE nodes. Add a nodeSelector with the value cloud.google.com/gke-os-distribution: cos_containerd to the specification of your customers' Pods.

<i>As it has been mentioned already: https://cloud.google.com/kubernetes-engine/docs/how-to/sandbox-pods?hl=en

https://cloud.google.com/kubernetes-engine/docs/how-to/sandbox-pods?hl=en#working_with</i>

98. our customer has implemented a solution that uses Cloud Spanner and notices some read latency-related performance issues on one table. This table is accessed only by their users using a primary key. The table schema is shown below. You want to resolve the issue. What should you do?

    A. Remove the profile_picture field from the table.

    B. Add a secondary index on the person_id column.

    <b>C. Change the primary key to not have monotonically increasing values.</b>

    D. Create a secondary index using the following Data Definition Language (DDL): 

<i>C is the right answer. Why? "This table is accessed only by their users using a primary key." So adding additional indexes on firstname and lastname won't help.</i>

99. Your finance team wants to view the billing report for your projects. You want to make sure that the finance team does not get additional permissions to the project. What should you do?

    A. Add the group for the finance team to roles/billing user role.

    B. Add the group for the finance team to roles/billing admin role.

    <b>C. Add the group for the finance team to roles/billing viewer role.</b>

    D. Add the group for the finance team to roles/billing project/Manager role.

100. Your organization has strict requirements to control access to Google Cloud projects. You need to enable your Site Reliability Engineers (SREs) to approve requests from the Google Cloud support team when an SRE opens a support case. You want to follow Google-recommended practices. What should you do?

    A. Add your SREs to roles/iam.roleAdmin role.

    B. Add your SREs to roles/accessapproval.approver role.

    C. Add your SREs to a group and then add this group to roles/iam.roleAdmin.role.

    D. Add your SREs to a group and then add this group to roles/accessapproval.approver role. 

<i>D. Add your SREs to a group and then add this group to roles/accessapproval approver role.
-Google recommendation.</i>

