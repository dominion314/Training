101. You need to host an application on a Compute Engine instance in a project shared with other teams. You want to prevent the other teams from accidentally causing downtime on that application. Which feature should you use?

    A. Use a Shielded VM.

    B. Use a Preemptible VM.

    C. Use a sole-tenant node.

    D. Enable deletion protection on the instance.

Correct Answer is (D):

Preventing Accidental VM Deletion
This document describes how to protect specific VM instances from deletion by setting the deletionProtection property on an Instance resource. To learn more about VM instances, read the Instances documentation.

As part of your workload, there might be certain VM instances that are critical to running your application or services, such as an instance running a SQL server, a server used as a license manager, and so on. These VM instances might need to stay running indefinitely so you need a way to protect these VMs from being deleted.

By setting the deletionProtection flag, a VM instance can be protected from accidental deletion. If a user attempts to delete a VM instance for which you have set the deletionProtection flag, the request fails. Only a user that has been granted a role with compute.instances.create permission can reset the flag to allow the resource to be deleted.

https://cloud.google.com/compute/docs/instances/preventing-accidental-vm-deletion

102. Your organization needs to grant users access to query datasets in BigQuery but prevent them from accidentally deleting the datasets. You want a solution that follows Google-recommended practices. What should you do?

    A. Add users to roles/bigquery user role only, instead of roles/bigquery dataOwner.

    B. Add users to roles/bigquery dataEditor role only, instead of roles/bigquery dataOwner.

    C. Create a custom role by removing delete permissions, and add users to that role only.

    D. Create a custom role by removing delete permissions. Add users to the group, and then add the group to the custom role.

Correct answer is D, I believe the key part is the "following Google Best Practices" phrase.

A - Works, but doesn't follow GCP best practices

B - Doesn't work as the role grants permission to delete datasets

C - Works, but is more complicated than A and doesn't follow Google best practices

D - Correct, more complicated than A, but it follows Google Best Practices.

103. You have a developer laptop with the Cloud SDK installed on Ubuntu. The Cloud SDK was installed from the Google Cloud Ubuntu package repository. You want to test your application locally on your laptop with Cloud Datastore. What should you do?

    A. Export Cloud Datastore data using gcloud datastore export.

    B. Create a Cloud Datastore index using gcloud datastore indexes create.

    C. Install the google-cloud-sdk-datastore-emulator component using the apt get install command.

    D. Install the cloud-datastore-emulator component using the gcloud components install command. 

I believe answer is C
https://cloud.google.com/sdk/docs/downloads-apt-get

The question is not about the datastore command itself but from where we should run the update command on the Ubuntu to install the component.

104. Your company set up a complex organizational structure on Google Cloud. The structure includes hundreds of folders and projects. Only a few team members should be able to view the hierarchical structure. You need to assign minimum permissions to these team members, and you want to follow Google-recommended practices. What should you do?

    A. Add the users to roles/browser role.

    B. Add the users to roles/iam.roleViewer role.

    C. Add the users to a group, and add this group to roles/browser.

    D. Add the users to a group, and add this group to roles/iam.roleViewer role.

Correct Answer is (C):

We need to apply the GCP Best practices.
roles/browser Browser Read access to browse the hierarchy for a project, including the folder, organization, and IAM policy. This role doesn't include permission to view resources in the project.

https://cloud.google.com/iam/docs/understanding-roles


105. Your company has a single sign-on (SSO) identity provider that supports Security Assertion Markup Language (SAML) integration with service providers. Your company has users in Cloud Identity. You would like users to authenticate using your company's SSO provider. What should you do?

    A. In Cloud Identity, set up SSO with Google as an identity provider to access custom SAML apps.

    B. In Cloud Identity, set up SSO with a third-party identity provider with Google as a service provider.

    C. Obtain OAuth 2.0 credentials, configure the user consent screen, and set up OAuth 2.0 for Mobile & Desktop Apps.

    D. Obtain OAuth 2.0 credentials, configure the user consent screen, and set up OAuth 2.0 for Web Server Applications.

B - This is the only possible option. You configure applications (service providers) to accept SAML assertions from the company’s existing identity provider and users in Cloud Identity can sign in to various applications through the third-party single sign-on (SSO) identity provider. It is important to note that user authentication occurs in the third-party IdP so the absence of a Gmail login is not an issue for signing in.
Ref: https://cloud.google.com/identity/solutions/enable-sso


106. Your organization has a dedicated person who creates and manages all service accounts for Google Cloud projects. You need to assign this person the minimum role for projects. What should you do?

    A. Add the user to roles/iam.roleAdmin role.

    B. Add the user to roles/iam.securityAdmin role.

    C. Add the user to roles/iam.serviceAccountUser role.

    D. Add the user to roles/iam.serviceAccountAdmin role. 

Answer is D, Service Account User (roles/iam.serviceAccountUser): Includes permissions to list service accounts, get details about a service account, and impersonate a service account.
Service Account Admin (roles/iam.serviceAccountAdmin): Includes permissions to list service accounts and get details about a service account. Also includes permissions to create, update, and delete service accounts, and to view or change the IAM policy on a service account.
Now look in which role mentioned "CREATE"?
Obviously - roles/iam.serviceAccountAdmin.

107. You are building an archival solution for your data warehouse and have selected Cloud Storage to archive your data. Your users need to be able to access this archived data once a quarter for some regulatory requirements. You want to select a cost-efficient option. Which storage option should you use?

    A. Cold Storage

    B. Nearline Storage

    C. Regional Storage

    D. Multi-Regional Storage

Correct Answer: A
Nearline, Coldline, and Archive offer ultra low-cost, highly-durable, highly available archival storage. For data accessed less than once a year, Archive is a cost- effective storage option for long-term preservation of data. Coldline is also ideal for cold storageג€"data your business expects to touch less than once a quarter.
For warmer storage, choose Nearline: data you expect to access less than once a month, but possibly multiple times throughout the year. All storage classes are available across all GCP regions and provide unparalleled sub-second access speeds with a consistent API.
Reference:
https://cloud.google.com/storage/archival


108. A team of data scientists infrequently needs to use a Google Kubernetes Engine (GKE) cluster that you manage. They require GPUs for some long-running, non- restartable jobs. You want to minimize cost. What should you do?

    A. Enable node auto-provisioning on the GKE cluster.

    B. Create a VerticalPodAutscaler for those workloads.

    C. Create a node pool with preemptible VMs and GPUs attached to those VMs.

    D. Create a node pool of instances with GPUs, and enable autoscaling on this node pool with a minimum size of 1. 

The correct option is "A"
auto-provisioning = Attaches and deletes node pools to cluster based on the requirements.
Hence creating a GPU node pool, and auto-scaling would be better

https://cloud.google.com/kubernetes-engine/docs/how-to/node-auto-provisioning

Incorrect options are
B. VerticalPodAutscaler scales PODS based on the app you deploy.

For handle infrequently GPU access, you need infrequently GPU nodes
VerticalAutscaler Pod deployed on a non GPU node it useless,
[We cant have the node always have GPU for infrequent requests]

C. Preemptible VMs cant last long

D. For infrequent access, you don't want to have a permanent homogenous cluster.

109. Your organization has user identities in Active Directory. Your organization wants to use Active Directory as their source of truth for identities. Your organization wants to have full control over the Google accounts used by employees for all Google services, including your Google Cloud Platform (GCP) organization. What should you do?

    A. Use Google Cloud Directory Sync (GCDS) to synchronize users into Cloud Identity.

    B. Use the cloud Identity APIs and write a script to synchronize users to Cloud Identity.

    C. Export users from Active Directory as a CSV and import them to Cloud Identity via the Admin Console.

    D. Ask each employee to create a Google account using self signup. Require that each employee use their company email address and password.

Correct Answer (A):

Directory Sync
Google Cloud Directory Sync enables administrators to synchronize users, groups and other data from an Active Directory/LDAP service to their Google Cloud domain directory

https://tools.google.com/dlpage/dirsync/

https://cloud.google.com/solutions/federating-gcp-with-active-directory-introduction


110. You have successfully created a development environment in a project for an application. This application uses Compute Engine and Cloud SQL. Now you need to create a production environment for this application. The security team has forbidden the existence of network routes between these 2 environments and has asked you to follow Google-recommended practices. What should you do?

    A. Create a new project, enable the Compute Engine and Cloud SQL APIs in that project, and replicate the setup you have created in the development environment.

    B. Create a new production subnet in the existing VPC and a new production Cloud SQL instance in your existing project, and deploy your application using those resources.

    C. Create a new project, modify your existing VPC to be a Shared VPC, share that VPC with your new project, and replicate the setup you have in the development environment in that new project in the Shared VPC.

    D. Ask the security team to grant you the Project Editor role in an existing production project used by another division of your company. Once they grant you that role, replicate the setup you have in the development environment in that project.

A - correct. Best practice is to create a new project for each environment, such as production and testing. There are no routes between VPCs in these projects by default, so that satisfies the requirement by the security team.

B. Nope. not best practice and allows communication.

C. While this is best practice to create a new project for a different environment, it explicitly breaks the security team's rule of having no path between environments by nature of the shared VPC. The shared VPC allows entities in both VPCs to communicate as if they were in the same VPC. That's definitely wrong.

D. One - not best practice to replicate in the setup in that project. Two - why do they suddenly need the project editor rule? Just a bad answer. Wrong.




111. Your management has asked an external auditor to review all the resources in a specific project. The security team has enabled the Organization Policy called Domain Restricted Sharing on the organization node by specifying only your Cloud Identity domain. You want the auditor to only be able to view, but not modify, the resources in that project. What should you do?

    A. Ask the auditor for their Google account, and give them the Viewer role on the project.

    B. Ask the auditor for their Google account, and give them the Security Reviewer role on the project.

    C. Create a temporary account for the auditor in Cloud Identity, and give that account the Viewer role on the project.

    D. Create a temporary account for the auditor in Cloud Identity, and give that account the Security Reviewer role on the project.

C - https://cloud.google.com/iam/docs/roles-audit-logging#scenario_external_auditors

roles/viewer Read access to all resources. Get and list access for all resources.

Using primitive roles
The following table lists the primitive roles that you can grant to access a project, the description of what the role does, and the permissions bundled within that role. Avoid using primitive roles except when absolutely necessary. These roles are very powerful, and include a large number of permissions across all Google Cloud services. For more details on when you should use primitive roles, see the Identity and Access Management FAQ.

IAM predefined roles are much more granular, and allow you to carefully manage the set of permissions that your users have access to. See Understanding Roles for a list of roles that can be granted at the project level. Creating custom roles can further increase the control you have over user permissions.

https://cloud.google.com/resource-manager/docs/access-control-proj#using_primitive_roles


112. You have a workload running on Compute Engine that is critical to your business. You want to ensure that the data on the boot disk of this workload is backed up regularly. You need to be able to restore a backup as quickly as possible in case of disaster. You also want older backups to be cleaned automatically to save on cost. You want to follow Google-recommended practices. What should you do?

    A. Create a Cloud Function to create an instance template.

    B. Create a snapshot schedule for the disk using the desired interval.

    C. Create a cron job to create a new disk from the disk using gcloud.

    D. Create a Cloud Task to create an image and export it to Cloud Storage.

Correct Answer (B):
Best practices for persistent disk snapshots
You can create persistent disk snapshots at any time, but you can create snapshots more quickly and with greater reliability if you use the following best practices.

Creating frequent snapshots efficiently
Use snapshots to manage your data efficiently.

Create a snapshot of your data on a regular schedule to minimize data loss due to unexpected failure.

Improve performance by eliminating excessive snapshot downloads and by creating an image and reusing it.

Set your snapshot schedule to off-peak hours to reduce snapshot time.

Snapshot frequency limits
Creating snapshots from persistent disks
You can snapshot your disks at most once every 10 minutes. If you want to issue a burst of requests to snapshot your disks, you can issue at most 6 requests in 60 minutes.

If the limit is exceeded, the operation fails and returns the following error:

https://cloud.google.com/compute/docs/disks/snapshot-best-practices

 
 
113. You need to assign a Cloud Identity and Access Management (Cloud IAM) role to an external auditor. The auditor needs to have permissions to review your Google Cloud Platform (GCP) Audit Logs and also to review your Data Access logs. What should you do?

    A. Assign the auditor the IAM role roles/logging.privateLogViewer. Perform the export of logs to Cloud Storage.

    B. Assign the auditor the IAM role roles/logging.privateLogViewer. Direct the auditor to also review the logs for changes to Cloud IAM policy.

    C. Assign the auditor's IAM user to a custom role that has logging.privateLogEntries.list permission. Perform the export of logs to Cloud Storage.

    D. Assign the auditor's IAM user to a custom role that has logging.privateLogEntries.list permission. Direct the auditor to also review the logs for changes to Cloud IAM policy.

Correct Answer is (B):

Background
Google Cloud provides Cloud Audit Logs, which is an integral part of Cloud Logging. It consists of two log streams for each project: Admin Activity and Data Access.
Admin Activity logs contain log entries for API calls or other administrative actions that modify the configuration or metadata of resources. Admin Activity logs are always enabled. There is no charge for your Admin Activity audit logs.
Data Access logs record API calls that create, modify, or read user-provided data. Data Access audit logs are disabled by default because they can be large.


logging.viewer: The logging.viewer role gives the security admin team the ability to view the Admin Activity logs.
logging.privateLogViewer : The logging.privateLogViewer role gives the ability to view the Data Access logs.

 
 
114. You are managing several Google Cloud Platform (GCP) projects and need access to all logs for the past 60 days. You want to be able to explore and quickly analyze the log contents. You want to follow Google-recommended practices to obtain the combined logs for all projects. What should you do?

    A. Navigate to Stackdriver Logging and select resource.labels.project_id="*"

    B. Create a Stackdriver Logging Export with a Sink destination to a BigQuery dataset. Configure the table expiration to 60 days.

    C. Create a Stackdriver Logging Export with a Sink destination to Cloud Storage. Create a lifecycle rule to delete objects after 60 days.

    D. Configure a Cloud Scheduler job to read from Stackdriver and store the logs in BigQuery. Configure the table expiration to 60 days.

I believe B is the answer.

All that matters in this scenario is the logs for the past 60 days.
We can use BigQuery to analyze contents so C is incorrect. We need to configure a BQ as the sink for the logs export so we can query and analyze log data in the future. Therefore D is incorrect.
https://cloud.google.com/logging/docs/audit/best-practices#export-best-practices

 
 
115. You need to reduce GCP service costs for a division of your company using the fewest possible steps. You need to turn off all configured services in an existing GCP project. What should you do?

    A. 1. Verify that you are assigned the Project Owners IAM role for this project. 2. Locate the project in the GCP console, click Shut down and then enter the project ID.

    B. 1. Verify that you are assigned the Project Owners IAM role for this project. 2. Switch to the project in the GCP console, locate the resources and delete them.

    C. 1. Verify that you are assigned the Organizational Administrator IAM role for this project. 2. Locate the project in the GCP console, enter the project ID and then click Shut down.

    D. 1. Verify that you are assigned the Organizational Administrators IAM role for this project. 2. Switch to the project in the GCP console, locate the resources and delete them.
 
A is right
Hint : You need to turn off all configured services in an ***existing GCP project***.
So C and D out from selection
 
116. You are configuring service accounts for an application that spans multiple projects. Virtual machines (VMs) running in the web-applications project need access to BigQuery datasets in crm-databases-proj. You want to follow Google-recommended practices to give access to the service account in the web-applications project. What should you do?

    A. Give project owner for web-applications appropriate roles to crm-databases-proj.

    B. Give project owner role to crm-databases-proj and the web-applications project.

    C. Give project owner role to crm-databases-proj and bigquery.dataViewer role to web-applications.

    D. Give bigquery.dataViewer role to crm-databases-proj and appropriate roles to web-applications. 

It is D because you're right, the question doesn't specify any specific kind of access, however, we need to follow the principle of least-privilege. Hence, we can only assume that read-only access is needed.

bigquery.dataViewer should be assigned to the group of analysts in the crm-databases-proj project.
https://cloud.google.com/bigquery/docs/access-control-examples#read_access_to_data_in_a_different_project

 
117. An employee was terminated, but their access to Google Cloud was not removed until 2 weeks later. You need to find out if this employee accessed any sensitive customer information after their termination. What should you do?

    A. View System Event Logs in Cloud Logging. Search for the user's email as the principal.

    B. View System Event Logs in Cloud Logging. Search for the service account associated with the user.

    C. View Data Access audit logs in Cloud Logging. Search for the user's email as the principal.

    D. View the Admin Activity log in Cloud Logging. Search for the service account associated with the user.

C is the correct answer. We are trying to find out if any sensitive data was accessed. Data access logs are the only logs that show this. C is the only option that mentions data access logs. 

https://cloud.google.com/logging/docs/audit#data-access
 
118. You need to create a custom IAM role for use with a GCP service. All permissions in the role must be suitable for production use. You also want to clearly share with your organization the status of the custom role. This will be the first version of the custom role. What should you do?

    A. Use permissions in your role that use the 'supported' support level for role permissions. Set the role stage to ALPHA while testing the role permissions.

    B. Use permissions in your role that use the 'supported' support level for role permissions. Set the role stage to BETA while testing the role permissions.

    C. Use permissions in your role that use the 'testing' support level for role permissions. Set the role stage to ALPHA while testing the role permissions.

    D. Use permissions in your role that use the 'testing' support level for role permissions. Set the role stage to BETA while testing the role permissions.

Answer should be A. You need a custom role with permissions supported in prod and you want to publish the status of the role.
https://cloud.google.com/iam/docs/custom-roles-permissions-support
SUPPORTED The permission is fully supported in custom roles.
TESTING The permission is being tested to check its compatibility with custom roles. You can include the permission in custom roles, but you might see unexpected behavior. Not recommended for production use.
NOT_SUPPORTED The permission is not supported in custom roles.
You can't use TESTING as it is not good for prod. And you need first version which should be ALPHA. 
    
 
119. Your company has a large quantity of unstructured data in different file formats. You want to perform ETL transformations on the data. You need to make the data accessible on Google Cloud so it can be processed by a Dataflow job. What should you do?

    A. Upload the data to BigQuery using the bq command line tool.

    B. Upload the data to Cloud Storage using the gsutil command line tool.

    C. Upload the data into Cloud SQL using the import function in the console.

    D. Upload the data into Cloud Spanner using the import function in the console.

B looks correct. Key work unstructured data. Cloud Storage as a datalake. "large quantity" : Cloud Storage or BigQuery
"files" a file is nothing but an Object. So Cloud Storage is the better option. For unstructured data use cloud storage. Use Big Query for analytics, data warehouse with structured data
 
 
120. You need to manage multiple Google Cloud projects in the fewest steps possible. You want to configure the Google Cloud SDK command line interface (CLI) so that you can easily manage multiple projects. What should you do?

    A. 1. Create a configuration for each project you need to manage. 2. Activate the appropriate configuration when you work with each of your assigned Google Cloud projects.

    B. 1. Create a configuration for each project you need to manage. 2. Use gcloud init to update the configuration values when you need to work with a non-default project

    C. 1. Use the default configuration for one project you need to manage. 2. Activate the appropriate configuration when you work with each of your assigned Google Cloud projects.

    D. 1. Use the default configuration for one project you need to manage. 2. Use gcloud init to update the configuration values when you need to work with a non-default project.

A
Cloud SDK comes with a default configuration. To create multiple configurations, use gcloud config configurations create, and gcloud config configurations activate to switch between them.

https://cloud.google.com/sdk/gcloud/reference/config/set

Generate your configurations with "gcloud config configurations create <config_id> ..." then activate the one you need according to the project you are working on with "gcloud config activate <config_id>"

