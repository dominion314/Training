21. You have one GCP account running in your default region and zone and another account running in a non-default region and zone. You want to start a new Compute Engine instance in these two Google Cloud Platform accounts using the command line interface. What should you do?

    <b>A. Create two configurations using gcloud config configurations create [NAME]. Run gcloud config configurations activate [NAME] to switch between accounts when running the commands to start the Compute Engine instances.</b>

    B. Create two configurations using gcloud config configurations create [NAME]. Run gcloud configurations list to start the Compute Engine instances.

    C. Activate two configurations using gcloud configurations activate [NAME]. Run gcloud config list to start the Compute Engine instances.

    D. Activate two configurations using gcloud configurations activate [NAME]. Run gcloud configurations list to start the Compute Engine instances.

<i>A is correct. All the other options don't make any sense when day say "Run gcloud configurations list to start the Compute Engine instances". How the heck are you expecting to "start" GCE instances doing "configuration list". Obviously B,C,D don't make any sense. How the heck are you expecting to "start" GCE instances doing "configuration list". Obviously B,C,D don't make any sense.</i>



22. You significantly changed a complex Deployment Manager template and want to confirm that the dependencies of all defined resources are properly met before committing it to the project. You want the most rapid feedback on your changes. What should you do?

    A. Use granular logging statements within a Deployment Manager template authored in Python.

    B. Monitor activity of the Deployment Manager execution on the Stackdriver Logging page of the GCP Console.

    C. Execute the Deployment Manager template against a separate project with the same configuration, and monitor for failures.

    <b> D. Execute the Deployment Manager template using the --preview option in the same project, and observe the state of interdependent resources.</b>

<i>From: https://cloud.google.com/deployment-manager/docs/deployments/  updating-deployments#optional_preview_an_updated_configuration
Preview an updated configuration
You can preview the update you want to make before committing any changes, with the Google Cloud CLI or the API. The Deployment Manager service previews the configuration by expanding the full configuration and creating "shell" resources.</i>


23. You are building a pipeline to process time-series data. Which Google Cloud Platform services should you put in boxes 1,2,3, and 4?

    A. Cloud Pub/Sub, Cloud Dataflow, Cloud Datastore, BigQuery

    B. Firebase Messages, Cloud Pub/Sub, Cloud Spanner, BigQuery

    C. Cloud Pub/Sub, Cloud Storage, BigQuery, Cloud Bigtable

    <b>D. Cloud Pub/Sub, Cloud Dataflow, Cloud Bigtable, BigQuery</b>

<i>
See this reference : https://cloud.google.com/blog/products/databases/getting-started-with-time-series-trend-predictions-using-gcp
Whenever we want to process timeseries data look for BigTable.
Also you want to perform analystics in Box 4 ..look for BigQuery  

    IoT = Unstructured data -> eliminated Datastore + Timeseries requirement = BigTable
    Ingestion point = Pub/Sub (Firebase messaging is a push notification service to client)
    Process data + realtime or batch = Data flow
    Analytics = BigQuery  
</i>

24. You have a project for your App Engine application that serves a development environment. The required testing has succeeded and you want to create a new project to serve as your production environment. What should you do?

    <b>A. Use gcloud to create the new project, and then deploy your application to the new project.</b>

    B. Use gcloud to create the new project and to copy the deployed application to the new project.

    C. Create a Deployment Manager configuration file that copies the current App Engine deployment into a new project.

    D. Deploy your application again using gcloud and specify the project parameter with the new project name to create the new project.

<i>Correct is A.
Option B is wrong as the option to use gcloud app cp does not exist.
Option C is wrong as Deployment Manager does not copy the application, but allows you to specify all the resources needed for your application in a declarative format using yaml
Option D is wrong as gcloud app deploy would not create a new project. The project should be created before usage</i>

25. You need to configure IAM access audit logging in BigQuery for external auditors. You want to follow Google-recommended practices. What should you do?

    <b>A. Add the auditors group to the 'logging.viewer' and 'bigQuery.dataViewer' predefined IAM roles.</b>

    B. Add the auditors group to two new custom IAM roles.

    C. Add the auditor user accounts to the 'logging.viewer' and 'bigQuery.dataViewer' predefined IAM roles.

    D. Add the auditor user accounts to two new custom IAM roles.

<i>Correct is A.
As per google best practices it is recommended to use predefined roles and create groups to control access to multiple users with same responsibility

https://cloud.google.com/iam/docs/job-functions/auditing#scenario_external_auditors
</i>


26. You need to set up permissions for a set of Compute Engine instances to enable them to write data into a particular Cloud Storage bucket. You want to follow Google-recommended practices. What should you do?

    A. Create a service account with an access scope. Use the access scope 'https://www.googleapis.com/auth/devstorage.write_only'.

    B. Create a service account with an access scope. Use the access scope 'https://www.googleapis.com/auth/cloud-platform'.

    <b>C. Create a service account and add it to the IAM role 'storage.objectCreator' for that bucket.</b>

    D. Create a service account and add it to the IAM role 'storage.objectAdmin' for that bucket.

<i>
No, it's not D. The question ask us to grant write object permission. You should not give a more broader permission as asked because it's against the principle of least privilege.

objectCreator has this description:
Allows users to create objects. Does not give permission to view, delete, or replace objects.

objectAdmin has this description:
Grants full control over objects, including listing, creating, viewing, and deleting objects.

The objectAdmin has unnecessary permissions that doesn't needed by the question context.

https://cloud.google.com/storage/docs/access-control/iam-roles

(https://cloud.google.com/compute/docs/access/create-enable-service-accounts-for-instances#best_practices) you grant the instance the scope and the permissions are determined by the IAM roles of the service account. In this case, you would grant the instance the scope and the role (storage.objectCreator) to the service account.

Ans B and C

Role from GCP Console:
ID = roles/storage.objectCreator
Role launch stage = General Availability
Description = Access to create objects in GCS.

    3 assigned permissions
    resourcemanager.projects.get
    resourcemanager.projects.list
    storage.objects.create
</i>


27. You have sensitive data stored in three Cloud Storage buckets and have enabled data access logging. You want to verify activities for a particular user for these buckets, using the fewest possible steps. You need to verify the addition of metadata labels and which files have been viewed from those buckets. What should you do?

    <b>A. Using the GCP Console, filter the Activity log to view the information</b>

    B. Using the GCP Console, filter the Stackdriver log to view the information.

    C. View the bucket in the Storage section of the GCP Console.

    D. Create a trace in Stackdriver to view the information.

<i>
A is correct. As mentioned in the question, data access logging is enabled. I tried to download a file from a bucket and was able to view this information in Activity tab in console
</i>


28. You are the project owner of a GCP project and want to delegate control to colleagues to manage buckets and files in Cloud Storage. You want to follow Google- recommended practices. Which IAM roles should you grant your colleagues?

    A. Project Editor

    <b>B. Storage Admin</b>

    C. Storage Object Admin

    D. Storage Object Creator

<i>
Correct Answer is (B):

Storage Admin (roles/storage.admin) Grants full control of buckets and objects.
When applied to an individual bucket, control applies only to the specified bucket and objects within the bucket.

    firebase.projects.get
    resourcemanager.projects.get
    resourcemanager.projects.list
    storage.buckets.*
    storage.objects.*
</i>


29. You have an object in a Cloud Storage bucket that you want to share with an external company. The object contains sensitive data. You want access to the content to be removed after four hours. The external company does not have a Google account to which you can grant specific user-based access privileges. You want to use the most secure method that requires the fewest steps. What should you do?

    <b>A. Create a signed URL with a four-hour expiration and share the URL with the company.</b>

    B. Set object access to 'public' and use object lifecycle management to remove the object after four hours.

    C. Configure the storage bucket as a static website and furnish the object's URL to the company. Delete the object from the storage bucket after four hours.

    D. Create a new Cloud Storage bucket specifically for the external company to access. Copy the object to that bucket. Delete the bucket after four hours have passed.

<i>
A.
Signed URLs are used to give time-limited resource access to anyone in possession of the URL, regardless of whether they have a Google account.
https://cloud.google.com/storage/docs/access-control/signed-urls
</i>


30. You are creating a Google Kubernetes Engine (GKE) cluster with a cluster autoscaler feature enabled. You need to make sure that each node of the cluster will run a monitoring pod that sends container metrics to a third-party monitoring solution. What should you do?
   
    A. Deploy the monitoring pod in a StatefulSet object.

    <b>B. Deploy the monitoring pod in a DaemonSet object.</b>

    C. Reference the monitoring pod in a Deployment object.

    D. Reference the monitoring pod in a cluster initializer at the GKE cluster creation time.

<i>
B is right: https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/
Some typical uses of a DaemonSet are:

running a cluster storage daemon on every node
running a logs collection daemon on every node
running a node monitoring daemon on every node

Every node is the keyword.

DaemonSets attempt to adhere to a one-Pod-per-node model, either across the entire cluster or a subset of nodes. As you add nodes to a node pool, DaemonSets automatically add Pods to the new nodes as needed.
</i>


31. You want to send and consume Cloud Pub/Sub messages from your App Engine application. The Cloud Pub/Sub API is currently disabled. You will use a service account to authenticate your application to the API. You want to make sure your application can use Cloud Pub/Sub. What should you do?

    <b>A. Enable the Cloud Pub/Sub API in the API Library on the GCP Console.</b>

    B. Rely on the automatic enablement of the Cloud Pub/Sub API when the Service Account accesses it.

    C. Use Deployment Manager to deploy your application. Rely on the automatic enablement of all APIs used by the application being deployed.

    D. Grant the App Engine Default service account the role of Cloud Pub/Sub Admin. Have your application enable the API on the first connection to Cloud Pub/ Sub.

<i>
We need to enable the pub/sub API, if we are going to use it in your project... then APP engine can able to access it with required ServiceAccount
</i>


32. You need to monitor resources that are distributed over different projects in Google Cloud Platform. You want to consolidate reporting under the same Stackdriver Monitoring dashboard. What should you do?

    A. Use Shared VPC to connect all projects, and link Stackdriver to one of the projects.

    B. For each project, create a Stackdriver account. In each project, create a service account for that project and grant it the role of Stackdriver Account Editor in all other projects.

    <b>C. Configure a single Stackdriver account, and link all projects to the same account.</b>

    D. Configure a single Stackdriver account for one of the projects. In Stackdriver, create a Group and add the other project names as criteria for that Group.

<i>
First of all D is incorrect, Groups are used to define alerts on set of resources(such as VM instances, databases, and load balancers). FYI tried adding Two projects into a group it did not allowed me as the "AND"/"OR" criteria for the group failed with this combination of resources.

C is correct because,
When you intially click on Monitoring(Stackdriver Monitoring) it creates a workspac(a stackdriver account) linked to the ACTIVE(CURRENT) Project from which it was clicked.

Now if you change the project and again click onto Monitoring it would create an another workspace(a stackdriver account) linked to the changed ACTIVE(CURRENT) Project, we don't want this as this would not consolidate our result into a single dashboard(workspace/stackdriver account).

    If you have accidently created two diff workspaces merge them under Monitoring > Settings > Merge Workspaces > MERGE.

    If we have only one workspace and two projects we can simply add other GCP Project under
    Monitoring > Settings > GCP Projects > Add GCP Projects.

In both of these cases we did not create a GROUP, we just linked GCP Project to the workspace(stackdriver account).
</i>


33. You are deploying an application to a Compute Engine VM in a managed instance group. The application must be running at all times, but only a single instance of the VM should run per GCP project. How should you configure the instance group?

    <b>A. Set autoscaling to On, set the minimum number of instances to 1, and then set the maximum number of instances to 1.</b>

    B. Set autoscaling to Off, set the minimum number of instances to 1, and then set the maximum number of instances to 1.

    C. Set autoscaling to On, set the minimum number of instances to 1, and then set the maximum number of instances to 2.

    D. Set autoscaling to Off, set the minimum number of instances to 1, and then set the maximum number of instances to 2.

<i>
In my GCP console, I created a managed instance group for each answer. For each answer I deleted the instance that was created as a simple test to prove or disprove each answer.

In answer A, another instance was created after I deleted the instance
In answer B, no other instance was created after I deleted the instance
In answer C, another instance was created after I deleted the instance
In answer D, no other instance was created after I deleted the instance

My observation is A is the correct Answer.

A - Correct - It correctly solves the problem with only a single instance at one time
B - Incorrect - Does not fit the requirement because AFTER the deletion of the instance, no other instance was created
C - Incorrect - It creates another instance after the delete HOWEVER it 2 VM's could be created even if the target is exceeded
D - Incorrect - Does not fit the requirement because AFTER the deletion of the instance, no other instance was created
</i>


34. You want to verify the IAM users and roles assigned within a GCP project named my-project. What should you do?

    A. Run gcloud iam roles list. Review the output section.

    B. Run gcloud iam service-accounts list. Review the output section.

    <b>C. Navigate to the project and then to the IAM section in the GCP Console. Review the members and roles.</b>

    D. Navigate to the project and then to the Roles section in the GCP Console. Review the roles and status.

<i>
Correct answer is C as IAM section provides the list of both Members and Roles.Option A is wrong as it would provide information about the roles only.Option B is wrong as it would provide only the service accounts.Option D is wrong as it would provide information about the roles only.
</i>


35. You need to create a new billing account and then link it with an existing Google Cloud Platform project. What should you do?

    A. Verify that you are Project Billing Manager for the GCP project. Update the existing project to link it to the existing billing account.

    <b>B. Verify that you are Project Billing Manager for the GCP project. Create a new billing account and link the new billing account to the existing project.</b>

    C. Verify that you are Billing Administrator for the billing account. Create a new project and link the new project to the existing billing account.

    D. Verify that you are Billing Administrator for the billing account. Update the existing project to link it to the existing billing account.

<i>
Billing Account Administrator
(roles/billing.admin) Manage billing accounts (but not create them).
Billing Account User
(roles/billing.user)

When granted in conjunction with the Project Owner role or Project Billing Manager role, provides access to associate projects with billing accounts.
</i>


36. You have one project called proj-sa where you manage all your service accounts. You want to be able to use a service account from this project to take snapshots of VMs running in another project called proj-vm. What should you do?

    A. Download the private key from the service account, and add it to each VMs custom metadata.

    B. Download the private key from the service account, and add the private key to each VM's SSH keys.

    <b>C. Grant the service account the IAM Role of Compute Storage Admin in the project called proj-vm.</b>

    D. When creating the VMs, set the service account's API scope for Compute Engine to read/write.

<i>
Any answer saying to transfer private keys is not safe. 

C is the correct answer.
It took me a while to figure it out because I didn't understand how service accounts work across project. This article made it clear for me. https://gtseres.medium.com/using-service-accounts-across-projects-in-gcp-cf9473fef8f0

You create the service account in proj-sa and take note of the service account email, then you go to proj-vm in IAM > ADD and add the service account's email as new member and give it the Compute Storage Admin role
</i>


37. You created a Google Cloud Platform project with an App Engine application inside the project. You initially configured the application to be served from the us-central region. Now you want the application to be served from the asia-northeast1 region. What should you do?

    A. Change the default region property setting in the existing GCP project to asia-northeast1.

    B. Change the region property setting in the existing App Engine application from us-central to asia-northeast1.

    C. Create a second App Engine application in the existing GCP project and specify asia-northeast1 as the region to serve your application.

    <b>D. Create a new GCP project and create an App Engine application inside this new project. Specify asia-northeast1 as the region to serve your application.</b>

<i>
Option D is correct, as there can be only one App Engine application inside a project . C is incorrect, as GCP can't have two app engine applications. Also, you can change the region of a deployed app engine application.
</i>


38. You need to grant access for three users so that they can view and edit table data on a Cloud Spanner instance. What should you do?

    A. Run gcloud iam roles describe roles/spanner.databaseUser. Add the users to the role.

    <b>B. Run gcloud iam roles describe roles/spanner.databaseUser. Add the users to a new group. Add the group to the role.</b>

    C. Run gcloud iam roles describe roles/spanner.viewer - -project my-project. Add the users to the role.

    D. Run gcloud iam roles describe roles/spanner.viewer - -project my-project. Add the users to a new group. Add the group to the role.

<i>
B is the most correct based on this
https://cloud.google.com/spanner/docs/iam#spanner.databaseUser
</b>


39. You create a new Google Kubernetes Engine (GKE) cluster and want to make sure that it always runs a supported and stable version of Kubernetes. What should you do?

    A. Enable the Node Auto-Repair feature for your GKE cluster.

    <b>B. Enable the Node Auto-Upgrades feature for your GKE cluster.</b>

    C. Select the latest available cluster version for your GKE cluster.

    D. Select ג€Container-Optimized OS (cos)ג€ as a node image for your GKE cluster.

<i>

"Creating or upgrading a cluster by specifying the version as <latest> does not provide automatic upgrades. Enable automatic node upgrades to ensure that the nodes in your cluster up to date with the latest stable version." --source: https://cloud.google.com/kubernetes-engine/versioning-and-upgrades

-Correct answer: B

</i>


40. You have an instance group that you want to load balance. You want the load balancer to terminate the client SSL session. The instance group is used to serve a public web application over HTTPS. You want to follow Google-recommended practices. What should you do?

    <b>A. Configure an HTTP(S) load balancer.</b>

    B. Configure an internal TCP load balancer.

    C. Configure an external SSL proxy load balancer.

    D. Configure an external TCP proxy load balancer.

<i>According to the documentation of SSL Proxy Load Balacing on Google, "SSL Proxy Load Balancing is intended for non-HTTP(S) traffic. For HTTP(S) traffic, we recommend that you use HTTP(S) Load Balancing." in my opinion A should be the most suitable choice.</i>