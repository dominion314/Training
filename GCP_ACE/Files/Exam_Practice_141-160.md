141. You created several resources in multiple Google Cloud projects. All projects are linked to different billing accounts. To better estimate future charges, you want to have a single visual representation of all costs incurred. You want to include new cost data as soon as possible. What should you do?

    A. Configure Billing Data Export to BigQuery and visualize the data in Data Studio.

    B. Visit the Cost Table page to get a CSV export and visualize it using Data Studio.

    C. Fill all resources in the Pricing Calculator to get an estimate of the monthly cost.

    D. Use the Reports view in the Cloud Billing Console to view the desired cost information.

A is more correct as you can show data from multiple billing accounts as well as different projects.

 
142. Your company has workloads running on Compute Engine and on-premises. The Google Cloud Virtual Private Cloud (VPC) is connected to your WAN over a Virtual Private Network (VPN). You need to deploy a new Compute Engine instance and ensure that no public Internet traffic can be routed to it. What should you do?

    A. Create the instance without a public IP address.

    B. Create the instance with Private Google Access enabled.

    C. Create a deny-all egress firewall rule on the VPC network.

    D. Create a route on the VPC to route all traffic to the instance over the VPN tunnel.

A for sure

B - this allows internal communicaiton, but does nothing to limit public traffic

C - deny all is nice, but it's for egress -- we're looking for ingress

D - this is way to invasive and doesn't explicitly address the issue of preventing public internet traffic from reaching your instance -- if it does, someone let me know how.

 
 
143. Your team maintains the infrastructure for your organization. The current infrastructure requires changes. You need to share your proposed changes with the rest of the team. You want to follow Google's recommended best practices. What should you do?

    A. Use Deployment Manager templates to describe the proposed changes and store them in a Cloud Storage bucket.

    B. Use Deployment Manager templates to describe the proposed changes and store them in Cloud Source Repositories.

    C. Apply the changes in a development environment, run gcloud compute instances list, and then save the output in a shared Storage bucket.

    D. Apply the changes in a development environment, run gcloud compute instances list, and then save the output in Cloud Source Repositories.

B is correct. https://cloud.google.com/source-repositories/docs/features


 
144. You have a Compute Engine instance hosting an application used between 9 AM and 6 PM on weekdays. You want to back up this instance daily for disaster recovery purposes. You want to keep the backups for 30 days. You want the Google-recommended solution with the least management overhead and the least number of services. What should you do?

    A. 1. Update your instances' metadata to add the following value: snapshot"schedule: 0 1 * * * 2. Update your instances' metadata to add the following value: snapshot"retention: 30

    B. 1. In the Cloud Console, go to the Compute Engine Disks page and select your instance's disk. 2. In the Snapshot Schedule section, select Create Schedule and configure the following parameters: - Schedule frequency: Daily - Start time: 1:00 AM " 2:00 AM - Autodelete snapshots after: 30 days

    C. 1. Create a Cloud Function that creates a snapshot of your instance's disk. 2. Create a Cloud Function that deletes snapshots that are older than 30 days. 3. Use Cloud Scheduler to trigger both Cloud Functions daily at 1:00 AM.

    D. 1. Create a bash script in the instance that copies the content of the disk to Cloud Storage. 2. Create a bash script in the instance that deletes data older than 30 days in the backup Cloud Storage bucket. 3. Configure the instance's crontab to execute these scripts daily at 1:00 AM.

Correct Answer is (B):

Creating scheduled snapshots for persistent disk
This document describes how to create a snapshot schedule to regularly and automatically back up your zonal and regional persistent disks. Use snapshot schedules as a best practice to back up your Compute Engine workloads. After creating a snapshot schedule, you can apply it to one or more persistent disks.

https://cloud.google.com/compute/docs/disks/scheduled-snapshots

 
145. Your existing application running in Google Kubernetes Engine (GKE) consists of multiple pods running on four GKE n1`"standard`"2 nodes. You need to deploy additional pods requiring n2`"highmem`"16 nodes without any downtime. What should you do?

    A. Use gcloud container clusters upgrade. Deploy the new services.

    B. Create a new Node Pool and specify machine type "highmem"16. Deploy the new pods.

    C. Create a new cluster with "highmem"16 nodes. Redeploy the pods and delete the old cluster.

    D. Create a new cluster with both "standard"2 and "highmem"16 nodes. Redeploy the pods and delete the old cluster.

B is correct answer, read below form google docs;

This tutorial demonstrates how to migrate workloads running on a Google Kubernetes Engine (GKE) cluster to a new set of nodes within the same cluster without incurring downtime for your application. Such a migration can be useful if you want to migrate your workloads to nodes with a different machine type.

Background
A node pool is a subset of machines that all have the same configuration, including machine type (CPU and memory) authorization scopes. Node pools represent a subset of nodes within a cluster; a container cluster can contain one or more node pools.

When you need to change the machine profile of your Compute Engine cluster, you can create a new node pool and then migrate your workloads over to the new node pool.

To migrate your workloads without incurring downtime, you need to:

Mark the existing node pool as unschedulable.
Drain the workloads running on the existing node pool.
Delete the existing node pool.

https://cloud.google.com/kubernetes-engine/docs/tutorials/migrating-node-pool#creating_a_node_pool_with_large_machine_type
 
146. You have an application that uses Cloud Spanner as a database backend to keep current state information about users. Cloud Bigtable logs all events triggered by users. You export Cloud Spanner data to Cloud Storage during daily backups. One of your analysts asks you to join data from Cloud Spanner and Cloud Bigtable for specific users. You want to complete this ad hoc request as efficiently as possible. What should you do?

    A. Create a dataflow job that copies data from Cloud Bigtable and Cloud Storage for specific users.

    B. Create a dataflow job that copies data from Cloud Bigtable and Cloud Spanner for specific users.

    C. Create a Cloud Dataproc cluster that runs a Spark job to extract data from Cloud Bigtable and Cloud Storage for specific users.

    D. Create two separate BigQuery external tables on Cloud Storage and Cloud Bigtable. Use the BigQuery console to join these tables through user fields, and apply appropriate filters.

Correct Answer is (D):

Introduction to external data sources
This page provides an overview of querying data stored outside of BigQuery.

https://cloud.google.com/bigquery/external-data-sources

147. You are hosting an application from Compute Engine virtual machines (VMs) in us`"central1`"a. You want to adjust your design to support the failure of a single Compute Engine zone, eliminate downtime, and minimize cost. What should you do?

    A. " Create Compute Engine resources in us"central1"b. " Balance the load across both us"central1"a and us"central1"b.

    B. " Create a Managed Instance Group and specify us"central1"a as the zone. " Configure the Health Check with a short Health Interval.

    C. " Create an HTTP(S) Load Balancer. " Create one or more global forwarding rules to direct traffic to your VMs.

    D. " Perform regular backups of your application. " Create a Cloud Monitoring Alert and be notified if your application becomes unavailable. " Restore from backups when notified.

A. Create Compute Engine resources in us "central1 "b. " Balance the load across both us "central1"a and us "central1"b.

 
148. A colleague handed over a Google Cloud Platform project for you to maintain. As part of a security checkup, you want to review who has been granted the ProjectOwner role. What should you do?

    A. In the console, validate which SSH keys have been stored as project-wide keys.

    B. Navigate to Identity-Aware Proxy and check the permissions for these resources.

    C. Enable Audit Logs on the IAM & admin page for all resources, and validate the results.

    D. Use the command gcloud projects get"iam"policy to view the current role assignments. 

Correct Answer is (D):

A simple approach would be to use the command flags available when listing all the IAM policy for a given project. For instance, the following command:

    `gcloud projects get-iam-policy $PROJECT_ID --flatten="bindings[].members" --format="table(bindings.members)" --filter="bindings.role:roles/owner"`

outputs all the users and service accounts associated with the role ‘roles/owner’ in the project in question.

https://groups.google.com/g/google-cloud-dev/c/Z6sZs7TvygQ?pli=1

149. You are running multiple VPC-native Google Kubernetes Engine clusters in the same subnet. The IPs available for the nodes are exhausted, and you want to ensure that the clusters can grow in nodes when needed. What should you do?

    A. Create a new subnet in the same region as the subnet being used.

    B. Add an alias IP range to the subnet used by the GKE clusters.

    C. Create a new VPC, and set up VPC peering with the existing VPC.

    D. Expand the CIDR range of the relevant subnet for the cluster.

Correct Answer is (D):

gcloud compute networks subnets expand-ip-range
NAME
gcloud compute networks subnets expand-ip-range - expand the IP range of a Compute Engine subnetwork

https://cloud.google.com/sdk/gcloud/reference/compute/networks/subnets/expand-ip-range
 
150. You have a batch workload that runs every night and uses a large number of virtual machines (VMs). It is fault-tolerant and can tolerate some of the VMs being terminated. The current cost of VMs is too high. What should you do?

    A. Run a test using simulated maintenance events. If the test is successful, use preemptible N1 Standard VMs when running future jobs.

    B. Run a test using simulated maintenance events. If the test is successful, use N1 Standard VMs when running future jobs.

    C. Run a test using a managed instance group. If the test is successful, use N1 Standard VMs in the managed instance group when running future jobs.

    D. Run a test using N1 standard VMs instead of N2. If the test is successful, use N1 Standard VMs when running future jobs.

Correct Answer is (A):

Creating and starting a preemptible VM instance
This page explains how to create and use a preemptible virtual machine (VM) instance. A preemptible instance is an instance you can create and run at a much lower price than normal instances. However, Compute Engine might terminate (preempt) these instances if it requires access to those resources for other tasks. Preemptible instances will always terminate after 24 hours. To learn more about preemptible instances, read the preemptible instances documentation.

Preemptible instances are recommended only for fault-tolerant applications that can withstand instance preemptions. Make sure your application can handle preemptions before you decide to create a preemptible instance. To understand the risks and value of preemptible instances, read the preemptible instances documentation.

https://cloud.google.com/compute/docs/instances/create-start-preemptible-instance

 
151. You are working with a user to set up an application in a new VPC behind a firewall. The user is concerned about data egress. You want to configure the fewest open egress ports. What should you do?

    A. Set up a low-priority (65534) rule that blocks all egress and a high-priority rule (1000) that allows only the appropriate ports.

    B. Set up a high-priority (1000) rule that pairs both ingress and egress ports.

    C. Set up a high-priority (1000) rule that blocks all egress and a low-priority (65534) rule that allows only the appropriate ports.

    D. Set up a high-priority (1000) rule to allow the appropriate ports.

Correct Answer is (A):

Implied rules
Every VPC network has two implied firewall rules. These rules exist, but are not shown in the Cloud Console:

Implied allow egress rule. An egress rule whose action is allow, destination is 0.0.0.0/0, and priority is the lowest possible (65535) lets any instance send traffic to any destination, except for traffic blocked by Google Cloud. A higher priority firewall rule may restrict outbound access. Internet access is allowed if no other firewall rules deny outbound traffic and if the instance has an external IP address or uses a Cloud NAT instance. For more information, see Internet access requirements.

Implied deny ingress rule. An ingress rule whose action is deny, source is 0.0.0.0/0, and priority is the lowest possible (65535) protects all instances by blocking incoming connections to them. A higher priority rule might allow incoming access. The default network includes some additional rules that override this one, allowing certain types of incoming connections.

https://cloud.google.com/vpc/docs/firewalls#default_firewall_rules

 
152. Your company runs its Linux workloads on Compute Engine instances. Your company will be working with a new operations partner that does not use Google Accounts. You need to grant access to the instances to your operations partner so they can maintain the installed tooling. What should you do?

    A. Enable Cloud IAP for the Compute Engine instances, and add the operations partner as a Cloud IAP Tunnel User.

    B. Tag all the instances with the same network tag. Create a firewall rule in the VPC to grant TCP access on port 22 for traffic from the operations partner to instances with the network tag.

    C. Set up Cloud VPN between your Google Cloud VPC and the internal network of the operations partner.

    D. Ask the operations partner to generate SSH key pairs, and add the public keys to the VM instances.

A is right. IAP will allow you to connect compute engine without GCP account from the public internet.

To control which users and groups are allowed to use IAP TCP forwarding and which VM instances they're allowed to connect to, configure Identity and Access Management (IAM) permissions.

How to:
Open the IAP admin page and select the SSH and TCP Resources tab.
Open the IAP admin page

Select the VM instances that you want to configure.
Click Show info panel if the info panel is not visible.
Click Add member and configure the following:

New members: Specify the user or group you want to grant access.
Select a role Select Cloud IAP > IAP-Secured Tunnel User.

https://cloud.google.com/iap/docs/using-tcp-forwarding#grant-permission
 
 IAP controls access to your App Engine apps and Compute Engine VMs running on Google Cloud. It leverages user identity and the context of a request to determine if a user should be allowed access. IAP is a building block toward BeyondCorp, an enterprise security model that enables employees to work from untrusted networks without using a VPN.

By default, IAP uses Google identities and IAM. By leveraging Identity Platform instead, you can authenticate users with a wide range of external identity providers, such as:

Email/password
OAuth (Google, Facebook, Twitter, GitHub, Microsoft, etc.)
SAML
OIDC
Phone number
Custom
Anonymous
This is useful if your application is already using an external authentication system, and migrating your users to Google accounts is impractica

153. You have created a code snippet that should be triggered whenever a new file is uploaded to a Cloud Storage bucket. You want to deploy this code snippet. What should you do?

    A. Use App Engine and configure Cloud Scheduler to trigger the application using Pub/Sub.

    B. Use Cloud Functions and configure the bucket as a trigger resource.

    C. Use Google Kubernetes Engine and configure a CronJob to trigger the application using Pub/Sub.

    D. Use Dataflow as a batch job, and configure the bucket as a data source.

Correct Answer is (B):

Google Cloud Storage Triggers
Cloud Functions can respond to change notifications emerging from Google Cloud Storage. These notifications can be configured to trigger in response to various events inside a bucket—object creation, deletion, archiving and metadata updates.

Note: Cloud Functions can only be triggered by Cloud Storage buckets in the same Google Cloud Platform project.
Event types
Cloud Storage events used by Cloud Functions are based on Cloud Pub/Sub Notifications for Google Cloud Storage and can be configured in a similar way.

Supported trigger type values are:

google.storage.object.finalize

google.storage.object.delete

google.storage.object.archive

google.storage.object.metadataUpdate

Object Finalize
Trigger type value: google.storage.object.finalize

This event is sent when a new object is created (or an existing object is overwritten, and a new generation of that object is created) in the bucket.

https://cloud.google.com/functions/docs/calling/storage#event_types


 
154. You have been asked to set up Object Lifecycle Management for objects stored in storage buckets. The objects are written once and accessed frequently for 30 days. After 30 days, the objects are not read again unless there is a special need. The objects should be kept for three years, and you need to minimize cost. What should you do?

    A. Set up a policy that uses Nearline storage for 30 days and then moves to Archive storage for three years.

    B. Set up a policy that uses Standard storage for 30 days and then moves to Archive storage for three years.

    C. Set up a policy that uses Nearline storage for 30 days, then moves the Coldline for one year, and then moves to Archive storage for two years.

    D. Set up a policy that uses Standard storage for 30 days, then moves to Coldline for one year, and then moves to Archive storage for two years.

Correct Answer is (B):

The key to understand the requirement is : "The objects are written once and accessed frequently for 30 days"
Standard Storage
Standard Storage is best for data that is frequently accessed ("hot" data) and/or stored for only brief periods of time.

Archive Storage
Archive Storage is the lowest-cost, highly durable storage service for data archiving, online backup, and disaster recovery. Unlike the "coldest" storage services offered by other Cloud providers, your data is available within milliseconds, not hours or days. Archive Storage is the best choice for data that you plan to access less than once a year.

https://cloud.google.com/storage/docs/storage-classes#standard


 
155. You are storing sensitive information in a Cloud Storage bucket. For legal reasons, you need to be able to record all requests that read any of the stored data. You want to make sure you comply with these requirements. What should you do?

    A. Enable the Identity Aware Proxy API on the project.

    B. Scan the bucket using the Data Loss Prevention API.

    C. Allow only a single Service Account access to read the data.

    D. Enable Data Access audit logs for the Cloud Storage API.

Correct Answer is (D):

Logged information
Within Cloud Audit Logs, there are two types of logs:

Admin Activity logs: Entries for operations that modify the configuration or metadata of a project, bucket, or object.

Data Access logs: Entries for operations that modify objects or read a project, bucket, or object. There are several sub-types of data access logs:

ADMIN_READ: Entries for operations that read the configuration or metadata of a project, bucket, or object.

DATA_READ: Entries for operations that read an object.

DATA_WRITE: Entries for operations that create or modify an object.

https://cloud.google.com/storage/docs/audit-logs#types

 
156. You are the team lead of a group of 10 developers. You provided each developer with an individual Google Cloud Project that they can use as their personal sandbox to experiment with different Google Cloud solutions. You want to be notified if any of the developers are spending above $500 per month on their sandbox environment. What should you do?

    A. Create a single budget for all projects and configure budget alerts on this budget.

    B. Create a separate billing account per sandbox project and enable BigQuery billing exports. Create a Data Studio dashboard to plot the spending per billing account.

    C. Create a budget per project and configure budget alerts on all of these budgets.

    D. Create a single billing account for all sandbox projects and enable BigQuery billing exports. Create a Data Studio dashboard to plot the spending per project.

Correct Answer is (C):

Set budgets and budget alerts
Overview
Avoid surprises on your bill by creating Cloud Billing budgets to monitor all of your Google Cloud charges in one place. A budget enables you to track your actual Google Cloud spend against your planned spend. After you've set a budget amount, you set budget alert threshold rules that are used to trigger email notifications. Budget alert emails help you stay informed about how your spend is tracking against your budget. Set budget scope
Set the budget Scope and then click Next.

In the Projects field, select one or more projects that you want to apply the budget alert to. To apply the budget alert to all the projects in the Cloud Billing account, choose Select all.

https://cloud.google.com/billing/docs/how-to/budgets#budget-scop

 
157. You are deploying a production application on Compute Engine. You want to prevent anyone from accidentally destroying the instance by clicking the wrong button. What should you do?

    A. Disable the flag Delete boot disk when instance is deleted.

    B. Enable delete protection on the instance.
    
    C. Disable Automatic restart on the instance.

    D. Enable Preemptibility on the instance.

Correct Answer is (B):

Preventing Accidental VM Deletion
This document describes how to protect specific VM instances from deletion by setting the deletionProtection property on an Instance resource. To learn more about VM instances, read the Instances documentation.

As part of your workload, there might be certain VM instances that are critical to running your application or services, such as an instance running a SQL server, a server used as a license manager, and so on. These VM instances might need to stay running indefinitely so you need a way to protect these VMs from being deleted.

By setting the deletionProtection flag, a VM instance can be protected from accidental deletion. If a user attempts to delete a VM instance for which you have set the deletionProtection flag, the request fails. Only a user that has been granted a role with compute.instances.create permission can reset the flag to allow the resource to be deleted.

https://cloud.google.com/compute/docs/instances/preventing-accidental-vm-deletion

 
158. Your company uses a large number of Google Cloud services centralized in a single project. All teams have specific projects for testing and development. The DevOps team needs access to all of the production services in order to perform their job. You want to prevent Google Cloud product changes from broadening their permissions in the future. You want to follow Google-recommended practices. What should you do?

    A. Grant all members of the DevOps team the role of Project Editor on the organization level.

    B. Grant all members of the DevOps team the role of Project Editor on the production project.

    C. Create a custom role that combines the required permissions. Grant the DevOps team the custom role on the production project.

    D. Create a custom role that combines the required permissions. Grant the DevOps team the custom role on the organization level.

C is the correct answer, give the devops team the least privileged role, only the required permissions to access the production services, as the question states 'to prevent product changes' for which editor role is not recommended either at Project or organizational level, organizational level access gives broad scope to all the projects in the organization, this role cannot be given to the devops team.

A. Editor has privilege to change the products, and the scope is broad

B. Editor has privilege to change the products

C. Recommended, as this will give only required permission at project level to devops team.

D. They require only project level access. This gives access to all project in the organization.

https://cloud.google.com/iam/docs/understanding-custom-roles#basic_concepts

159. You are building an application that processes data files uploaded from thousands of suppliers. Your primary goals for the application are data security and the expiration of aged data. You need to design the application to:
* Restrict access so that suppliers can access only their own data.
* Give suppliers write access to data only for 30 minutes.
* Delete data that is over 45 days old. You have a very short development cycle, and you need to make sure that the application requires minimal maintenance. Which two strategies should you use? (Choose two.)

    A. Build a lifecycle policy to delete Cloud Storage objects after 45 days.

    B. Use signed URLs to allow suppliers limited time access to store their objects.

    C. Set up an SFTP server for your application, and create a separate user for each supplier.

    D. Build a Cloud function that triggers a timer of 45 days to delete objects that have expired.

    E. Develop a script that loops through all Cloud Storage buckets and deletes any buckets that are older than 45 days.

100% A and B.

A. lifecycle policies are designed for situations just like these. It can cleanly schedule deletion of objects that are past 45 days old.
B. signed URls also can allow the user to 1) access exclusively their own data and 2) access it for a set period of time, in this case 30 minutes
C. an SFTP server? Really? This is GCP, not circa 2000. Plus it's high maintenance.
D. Ok, though it's better than C, it's still clunky. Why not use a pre-built lifecycle policy? Plus this doesn't tell us how objects are marked as expired anyways. Sounds like extra work.
E. Now this one is as bad as C. All that looping isn't sustainable as the dataset grows. Plus it's overly complex, and doesn't even mention how the script gets invoked in the first place. Another can of worms.

https://cloud.google.com/storage/docs/lifecycle#delete
https://cloud.google.com/storage/docs/access-control/signed-urls


 
160. Your company wants to standardize the creation and management of multiple Google Cloud resources using Infrastructure as Code. You want to minimize the amount of repetitive code needed to manage the environment. What should you do?

    A. Develop templates for the environment using Cloud Deployment Manager.

    B. Use curl in a terminal to send a REST request to the relevant Google API for each individual resource.

    C. Use the Cloud Console interface to provision and manage all related resources.

    D. Create a bash script that contains all requirement steps as gcloud commands.

A
You can use Google Cloud Deployment Manager to create a set of Google Cloud resources and manage them as a unit, called a deployment. For example, if your team's development environment needs two virtual machines (VMs) and a BigQuery database, you can define these resources in a configuration file, and use Deployment Manager to create, change, or delete these resources. You can make the configuration file part of your team's code repository, so that anyone can create the same environment with consistent results.
https://cloud.google.com/deployment-manager/docs/quickstart

