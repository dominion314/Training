1. Every employee of your company has a Google account. Your operational team needs to manage a large number of instances on Compute Engine. Each member of this team needs only administrative access to the servers. Your security team wants to ensure that the deployment of credentials is operationally efficient and must be able to determine who accessed a given instance. What should you do?

    A. Generate a new SSH key pair. Give the private key to each member of your team. Configure the public key in the metadata of each instance.

    B. Ask each member of the team to generate a new SSH key pair and to send you their public key. Use a configuration management tool to deploy those keys on each instance.

    <b>C. Ask each member of the team to generate a new SSH key pair and to add the public key to their Google account. Grant the compute.osAdminLogin role to the Google group corresponding to this team.</b>

    D. Generate a new SSH key pair. Give the private key to each member of your team. Configure the public key as a project-wide public SSH key in your Cloud Platform project and allow project-wide public SSH keys on each instance.

<i>We recommend collecting users with the same responsibilities into groups and assigning IAM roles to the groups rather than to individual users. For example, you can create a "data scientist" group and assign appropriate roles to enable interaction with BigQuery and Cloud Storage. When a new data scientist joins your team, you can simply add them to the group and they will inherit the defined permissions. You can create and manage groups through the Admin Console.</i>

2. You need to create a custom VPC with a single subnet. The subnet's range must be as large as possible. Which range should you use?

    A. 0.0.0.0/0

    <b>B. 10.0.0.0/8</b>

    C. 172.16.0.0/12

    D. 192.168.0.0/16

<i>10.0.0.0/8 gives you the most extensive range - 16777216 IP Addresses.</i>

3. You want to select and configure a cost-effective solution for relational data on Google Cloud Platform. You are working with a small set of operational data in one geographic location. You need to support point-in-time recovery. What should you do?

    <b>A. Select Cloud SQL (MySQL). Verify that the enable binary logging option is selected.</b>

    B. Select Cloud SQL (MySQL). Select the create failover replicas option.

    C. Select Cloud Spanner. Set up your instance with 2 nodes.

    D. Select Cloud Spanner. Set up your instance as multi-regional.

<i>A is Correct. You must enable binary logging to use point-in-time recovery. Enabling binary logging causes a slight reduction in write performance. Point-in-time recovery is enabled by default when you create a new Cloud SQL instance. https://cloud.google.com/sql/docs/mysql/backup-recovery/backups</i>

4. You want to configure autohealing for <u>network load balancing</u> for a group of Compute Engine instances that run in multiple zones, using the fewest possible steps. You need to configure re-creation of VMs if they are unresponsive after 3 attempts of 10 seconds each. What should you do?

    A. Create an HTTP load balancer with a backend configuration that references an existing instance group. Set the health check to healthy (HTTP)

    B. Create an HTTP load balancer with a backend configuration that references an existing instance group. Define a balancing mode and set the maximum RPS to 10.

    <b>C. Create a managed instance group. Set the Autohealing health check to healthy (HTTP)</b>

    D. Create a managed instance group. Verify that the autoscaling setting is on.

<i>reference : https://cloud.google.com/compute/docs/tutorials/high-availability-autohealing

go to gcp console create a httpa load balancer and in the health check settings take your mouse to question mark it says """Ensures that requests are sent only to instances that are up and running"""
so its not recreating, if the vm not working it redirect to one which work.

go to gpc console create MIG and check the questions mark of Autohealing health check settings it says
"""Autohealing allows recreating VM instances when needed. You can use a health check to recreate a VM instance if the health check finds it unresponsive. If you don't select a health check, Compute Engine will recreate VM instances only when they're not running.

    step-1: go to the instance group
    step-2: click edit
    step-3: scroll down you will see auto-healing off by default change to ON
    step-4: create a health check saying 10 seconds as CHECK INTERVAL and UNHEALTHY THRESHOLD=3
</i>

5.  You are using multiple configurations for gcloud. You want to review the configured Kubernetes Engine cluster of an inactive configuration using the fewest possible steps. What should you do?

    A. Use gcloud config configurations describe to review the output.

    B. Use gcloud config configurations activate and gcloud config list to review the output.

    C. Use kubectl config get-contexts to review the output.

    <b>D. Use kubectl config use-context and kubectl config view to review the output.</b>

<i>The question is want to review a inactive configure. So, to me, C is viewing info about all configure while D is apply a specific config and viewing it. So I eliminate C to go with D.</i>


6. Your company uses Cloud Storage to store application backup files for disaster recovery purposes. You want to follow Google's recommended practices. Which storage option should you use?

    A. Multi-Regional Storage

    B. Regional Storage

    C. Nearline Storage

    <b>D. Coldline Storage</b>

<i>Best Answer is " Archive Storage "
https://cloud.google.com/storage/docs/storage-classes

But as per the given option next best solution is " Coldline Storage"
Coldline Storage COLDLINE 90 days
99.95% in multi-regions and dual-regions
99.9% in regions
</i> 


7. Several employees at your company have been creating projects with Cloud Platform and paying for it with their personal credit cards, which the company reimburses. The company wants to centralize all these projects under a single, new billing account. What should you do?

    A. Contact cloud-billing@google.com with your bank account details and request a corporate billing account for your company.

    B. Create a ticket with Google Support and wait for their call to share your credit card details over the phone.

    C. In the Google Platform Console, go to the Resource Manage and move all projects to the root Organizarion.

    <b>D. In the Google Cloud Platform Console, create a new billing account and set up a payment method.</b>

<i>We need to add a new payment method and need to set that as Primary, post that we need to remove the previous one
"If you want to remove a payment method, you should add a new payment method first."

C is incomplete. Moving projects under an organisation doesn't change their linked billing project.
https://cloud.google.com/resource-manager/docs/migrating-projects-billing

Note: The link between projects and billing accounts is preserved, irrespective of the hierarchy. When you move your existing projects into the organization they will continue to work and be billed as they used to before the migration, even if the corresponding billing account has not been migrated yet.

D is incomplete as well, after setting the billing account in the organisation you need to link the projects to the new billing account.
</i>


8. You have an application that looks for its licensing server on the IP 10.0.3.21. You need to deploy the licensing server on Compute Engine. You do not want to change the configuration of the application and want the application to be able to reach the licensing server. What should you do?

    <b>A. Reserve the IP 10.0.3.21 as a static internal IP address using gcloud and assign it to the licensing server.</b>

    B. Reserve the IP 10.0.3.21 as a static public IP address using gcloud and assign it to the licensing server.

    C. Use the IP 10.0.3.21 as a custom ephemeral IP address and assign it to the licensing server.

    D. Start the licensing server with an automatic ephemeral IP address, and then promote it to a static internal IP address.

<i>IP 10.0.3.21 is internal by default, and to ensure that it will be static non-changing it should be selected as static internal ip address.</i>


9. You are deploying an application to App Engine. You want the number of instances to scale based on request rate. You need at least 3 unoccupied instances at all times. Which scaling type should you use?

    A. Manual Scaling with 3 instances.

    B. Basic Scaling with min_instances set to 3.

    C. Basic Scaling with max_instances set to 3.

    <b>D. Automatic Scaling with min_idle_instances set to 3.</b>

<i>App Engine supports the following scaling types, which controls how and when instances are created:

Automatic
Basic
Manual
You specify the scaling type in your app's app.yaml.

Automatic scaling
Automatic scaling creates instances based on request rate, response latencies, and other application metrics. You can specify thresholds for each of these metrics, as well as a minimum number instances to keep running at all times.</i>


10. You have a development project with appropriate IAM roles defined. You are creating a production project and want to have the same IAM roles on the new project, using the fewest possible steps. What should you do?

    <b>A. Use gcloud iam roles copy and specify the production project as the destination project.</b>

    B. Use gcloud iam roles copy and specify your organization as the destination organization.

    C. In the Google Cloud Platform Console, use the 'create role from role' functionality.

    D. In the Google Cloud Platform Console, use the 'create role' functionality and select all applicable permissions.

<i>Could also be B, but I think because its one project that we're copying to that we dont need to apply it to the whole org. Its the difference in the following code applying to all projects if you go with the org option for B

    gcloud iam roles copy --source="roles/spanner.databaseAdmin" --destination=CustomViewer --dest-organization=1234567

    vs

    gcloud iam roles copy --source="roles/spanner.databaseAdmin" --destination=CustomSpannerDbAdmin --dest-project=PROJECT_ID

    </i>

11. You need a dynamic way of provisioning VMs on Compute Engine. The exact specifications will be in a dedicated configuration file. You want to follow Google's recommended practices. Which method should you use? 

    <b>A. Deployment Manager</b>

    B. Cloud Composer

    C. Managed Instance Group

    D. Unmanaged Instance Group

<i> Its not C because managed instance groups dont have a dedicated configuration file Explained here: https://cloud.google.com/deployment-manager/docs/configuration/create-basic-configuration</i>


12. You have a Dockerfile that you need to deploy on Kubernetes Engine. What should you do?

    A. Use kubectl app deploy <dockerfilename>.

    B. Use gcloud app deploy <dockerfilename>.

    <b>C. Create a docker image from the Dockerfile and upload it to Container Registry. Create a Deployment YAML file to point to that image. Use kubectl to create the deployment with that file.</b>

    D. Create a docker image from the Dockerfile and upload it to Cloud Storage. Create a Deployment YAML file to point to that image. Use kubectl to create the deployment with that file


13. Your development team needs a new Jenkins server for their project. You need to deploy the server using the fewest steps possible. What should you do?

    A. Download and deploy the Jenkins Java WAR to App Engine Standard.

    B. Create a new Compute Engine instance and install Jenkins through the command line interface.

    C. Create a Kubernetes cluster on Compute Engine and create a deployment with the Jenkins Docker image.

    <b>D. Use GCP Marketplace to launch the Jenkins solution.</b>


14. You need to update a deployment in Deployment Manager without any resource downtime in the deployment. Which command should you use?

    A. gcloud deployment-manager deployments create --config <deployment-config-path>

    <b>B. gcloud deployment-manager deployments update --config <deployment-config-path></b>

    C. gcloud deployment-manager resources create --config <deployment-config-path>

    D. gcloud deployment-manager resources update --config <deployment-config-path>
 
<i>update and create resource is not even a command under deployment management service.</i>


15. You need to run an important query in BigQuery but expect it to return a lot of records. You want to find out how much it will cost to run the query. You are using on-demand pricing. What should you do?

    A. Arrange to switch to Flat-Rate pricing for this query, then move back to on-demand.

    <b>B. Use the command line to run a dry run query to estimate the number of bytes read. Then convert that bytes estimate to dollars using the Pricing Calculator.</b>

    C. Use the command line to run a dry run query to estimate the number of bytes returned. Then convert that bytes estimate to dollars using the Pricing Calculator.

    D. Run a select count (*) to get an idea of how many records your query will look through. Then convert that number of rows to dollars using the Pricing Calculator.

<i>Youre charged for the number of bytes READ not RETURNED

On-demand pricing
Under on-demand pricing, BigQuery charges for queries by using one metric: the number of bytes processed (also referred to as bytes read). You are charged for the number of bytes processed whether the data is stored in BigQuery or in an external data source such as Cloud Storage, Drive, or Cloud Bigtable. On-demand pricing is based solely on usage.

https://cloud.google.com/bigquery/pricing#on_demand_pricing</i>


16. You have a single binary application that you want to run on Google Cloud Platform. You decided to automatically scale the application based on underlying infrastructure CPU usage. Your organizational policies require you to use virtual machines directly. You need to ensure that the application scaling is operationally efficient and completed as quickly as possible. What should you do?

    A. Create a Google Kubernetes Engine cluster, and use horizontal pod autoscaling to scale the application.

    <b>B. Create an instance template, and use the template in a managed instance group with autoscaling configured.</b>

    C. Create an instance template, and use the template in a managed instance group that scales up and down based on the time of day.

    D. Use a set of third-party tools to build automation around scaling the application up and down, based on Stackdriver CPU usage monitoring.

<i>Let's first break down the question into key requirements -

1. automatically scale the application based on underlying infrastructure CPU usage.

2. use virtual machines directly.

A. Not feasible because VMs are not used directly here.

B. This is the correct answer.

C. Time of Day... Easy elimination because this does not scale on CPU usage and time of day is mentioned NOWHERE.

D. Third Party Tools.... Nobody would use GCP if they needed third party tools to do something as simple as scaling based on CPU usage. all popular cloud providers have native solutions for this including GCP.
</i>

17. You are analyzing Google Cloud Platform service costs from three separate projects. You want to use this information to create service cost estimates by service type, daily and monthly, for the next six months using standard query syntax. What should you do?

    A. Export your bill to a Cloud Storage bucket, and then import into Cloud Bigtable for analysis.

    B. Export your bill to a Cloud Storage bucket, and then import into Google Sheets for analysis.
    
    C. Export your transactions to a local file, and perform analysis with a desktop tool.

    <b>D. Export your bill to a BigQuery dataset, and then write time window-based SQL queries for analysis.</b>
    
<i>A. 'Cloud Storage bucket'........'Cloud Bigtable'. Not feasible, mainly because cloud BigTable is not good for Structured Data (or Relational Data on which we can run SQL queries as per the question's requirements). BigTable is better suited for Semi Structured data and NoSQL data.

B. 'Cloud Storage bucket'.....'Google Sheets'. Not Feasible because there is no use of SQL in this option, which is one of the requirements.

C. Local file, external tools... this is automatically eliminated because the operation we need is simple, and there has to be a GCP native solution for this. We shouldn't need to rely on going out of the cloud for such a simple thing.

D. 'BigQuery'.....'SQL queries' -> This is the right answer</i>


18. You need to set up a policy so that videos stored in a specific Cloud Storage Regional bucket are moved to Coldline after 90 days, and then deleted after one year from their creation. How should you set up the policy?

    A. Use Cloud Storage Object Lifecycle Management using Age conditions with SetStorageClass and Delete actions. Set the SetStorageClass action to 90 days and the Delete action to 275 days (365-90)

    <b>B. Use Cloud Storage Object Lifecycle Management using Age conditions with SetStorageClass and Delete actions. Set the SetStorageClass action to 90 days and the Delete action to 365 days.</b>

    C. Use gsutil rewrite and set the Delete action to 275 days (365-90).

    D. Use gsutil rewrite and set the Delete action to 365 days.

<i>You only re-calculate expiry date when objects are re-written using re-write option to another storage class in which case creation date is rest.
But in this case objects is moveed to Coldline class after 90 days and then we want to delete the object after 365 days.

The Age condition is satisfied when an object reaches the specified age (in days). Age is measured from the object's creation time. For example, if an object's creation time is 2019/01/10 10:00 UTC and the Age condition is 10 days, then the condition is satisfied for the object on and after 2019/01/20 10:00 UTC. This is true even if the object becomes noncurrent through Object Versioning sometime after its creation.</i>



19. You have a Linux VM that must connect to Cloud SQL. You created a service account with the appropriate access rights. You want to make sure that the VM uses this service account instead of the default Compute Engine service account. What should you do?

    <b>A. When creating the VM via the web console, specify the service account under the 'Identity and API Access' section.</b>

    B. Download a JSON Private Key for the service account. On the Project Metadata, add that JSON as the value for the key compute-engine-service- account.

    C. Download a JSON Private Key for the service account. On the Custom Metadata of the VM, add that JSON as the value for the key compute-engine- service-account.

    D. Download a JSON Private Key for the service account. After creating the VM, ssh into the VM and save the JSON under ~/.gcloud/compute-engine-service- account.json.

<i>
https://cloud.google.com/compute/docs/access/create-enable-service-accounts-for-instances#changeserviceaccountandscopes
---
"To change an instance's service account and access scopes, the instance must be temporarily stopped ... After changing the service account or access scopes, remember to restart the instance." So we can stop the instance, change the service account, then start it up again.

A: It's wrong, but it is the most applicable here. Here is why:

B: Talks about project wide, it's to broad and should be ruled out.

C: I did voted for this one at first, but after some research, i've found this:

The json file downloaded is a private key, and as per documentation, the service-account metadata of a VM should be used for tokens, unique to the VMs, not the service account:
https://cloud.google.com/compute/docs/metadata/default-metadata-values

Now, even considering this to be a key scenario, wouldn't the SQL need to be configured with the private key of the service account for this to work? And adding this private key alone to the VMs does not guarantee that the default service account wont be used (which is stated by the question)

D: this is for gcloud tool/SDK, not VMs.
</i>


20. You created an instance of SQL Server 2017 on Compute Engine to test features in the new version. You want to connect to this instance using the fewest number of steps. What should you do?

    A. Install a RDP client on your desktop. Verify that a firewall rule for port 3389 exists.

    <b>B. Install a RDP client in your desktop. Set a Windows username and password in the GCP Console. Use the credentials to log in to the instance.</b>

    C. Set a Windows password in the GCP Console. Verify that a firewall rule for port 22 exists. Click the RDP button in the GCP Console and supply the credentials to log in.

    D. Set a Windows username and password in the GCP Console. Verify that a firewall rule for port 3389 exists. Click the RDP button in the GCP Console, and supply the credentials to log in.


<i>
RDP is enabled by default when you crate a Windows instance (no need to chek on it). Just make sure you install an RDP client ( chrome ext or RDP) and set windows password. The firewall rule for port 3389 is created by default if you create windows server on Compute Engine. So, no need to verify it.

https://cloud.google.com/compute/docs/instances/connecting-to-windows#before-you-begin

D is not correct. When you click the RDP button, you are asked to install a client or use the Windows RDP client if you are running Windows. There is no option to enter credentials or get an RDP session through the web interface.
</i>



 
