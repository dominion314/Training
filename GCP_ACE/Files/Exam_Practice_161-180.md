161. You are performing a monthly security check of your Google Cloud environment and want to know who has access to view data stored in your Google Cloud Project. What should you do?

    A. Enable Audit Logs for all APIs that are related to data storage.

    B. Review the IAM permissions for any role that allows for data access.

    C. Review the Identity-Aware Proxy settings for each resource.

    D. Create a Data Loss Prevention job.

B is the one:

A. Enable Audit Logs for all APIs that are related to data storage. --> That is not the correct answer, if someone with permissions has not accessed or does not access, it will not be listed.
B. Review the IAM permissions for any role that allows for data access. --> That's correct
C. Review the Identity-Aware Proxy settings for each resource. --> Nothing relevant, Proxy? Is configured? The question don't ask or tell something about if it is configured.
D. Create a Data Loss Prevention job. --> Data Loss Prevention nothing to see here.
 
162. Your company has embraced a hybrid cloud strategy where some of the applications are deployed on Google Cloud. A Virtual Private Network (VPN) tunnel connects your Virtual Private Cloud (VPC) in Google Cloud with your company's on-premises network. Multiple applications in Google Cloud need to connect to an on-premises database server, and you want to avoid having to change the IP configuration in all of your applications when the IP of the database changes. What should you do?

    A. Configure Cloud NAT for all subnets of your VPC to be used when egressing from the VM instances.

    B. Create a private zone on Cloud DNS, and configure the applications with the DNS name.

    C. Configure the IP of the database as custom metadata for each instance, and query the metadata server.

    D. Query the Compute Engine internal DNS from the applications to retrieve the IP of the database.

B,
Forwarding zones
Cloud DNS forwarding zones let you configure target name servers for specific private zones. Using a forwarding zone is one way to implement outbound DNS forwarding from your VPC network.

A Cloud DNS forwarding zone is a special type of Cloud DNS private zone. Instead of creating records within the zone, you specify a set of forwarding targets. Each forwarding target is an IP address of a DNS server, located in your VPC network, or in an on-premises network connected to your VPC network by Cloud VPN or Cloud Interconnect.

A does not apply, that is to provide internet access to resources
C, does not apply
D, I don't get it


 
163. You have developed a containerized web application that will serve internal colleagues during business hours. You want to ensure that no costs are incurred outside of the hours the application is used. You have just created a new Google Cloud project and want to deploy the application. What should you do?

    A. Deploy the container on Cloud Run for Anthos, and set the minimum number of instances to zero.

    B. Deploy the container on Cloud Run (fully managed), and set the minimum number of instances to zero.

    C. Deploy the container on App Engine flexible environment with autoscaling, and set the value min_instances to zero in the app.yaml.

    D. Deploy the container on App Engine flexible environment with manual scaling, and set the value instances to zero in the app.yaml.

B:
not A because Anthos is an add-on to GKE clusters, 'new project' means we dont have a GKE cluster to work with
https://cloud.google.com/kuberun/docs/architecture-overview#components_in_the_default_installation

 
164. You have experimented with Google Cloud using your own credit card and expensed the costs to your company. Your company wants to streamline the billing process and charge the costs of your projects to their monthly invoice. What should you do?

    A. Grant the financial team the IAM role of ג€Billing Account Userג€ on the billing account linked to your credit card.

    B. Set up BigQuery billing export and grant your financial department IAM access to query the data.

    C. Create a ticket with Google Billing Support to ask them to send the invoice to your company.

    D. Change the billing account of your projects to the billing account of your company.

D. Change the billing account of your projects to the billing account of your company.


 
165. You are running a data warehouse on BigQuery. A partner company is offering a recommendation engine based on the data in your data warehouse. The partner company is also running their application on Google Cloud. They manage the resources in their own project, but they need access to the BigQuery dataset in your project. You want to provide the partner company with access to the dataset. What should you do?

    A. Create a Service Account in your own project, and grant this Service Account access to BigQuery in your project.

    B. Create a Service Account in your own project, and ask the partner to grant this Service Account access to BigQuery in their project.

    C. Ask the partner to create a Service Account in their project, and have them give the Service Account access to BigQuery in their project.

    D. Ask the partner to create a Service Account in their project, and grant their Service Account access to the BigQuery dataset in your project.

D is correct, https://gtseres.medium.com/using-service-accounts-across-projects-in-gcp-cf9473fef8f0#:~:text=Go%20to%20the%20destination%20project,Voila!

A is to broad. The question states: "provide the partner company with access to the dataset"
A states: "grant this Service Account access to BigQuery in your project"
 
166. Your web application has been running successfully on Cloud Run for Anthos. You want to evaluate an updated version of the application with a specific percentage of your production users (canary deployment). What should you do?

    A. Create a new service with the new version of the application. Split traffic between this version and the version that is currently running.

    B. Create a new revision with the new version of the application. Split traffic between this version and the version that is currently running.

    C. Create a new service with the new version of the application. Add an HTTP Load Balancer in front of both services.

    D. Create a new revision with the new version of the application. Add an HTTP Load Balancer in front of both revisions.

B
https://cloud.google.com/kuberun/docs/rollouts-rollbacks-traffic-migration
Cloud Run for Anthos allows you to specify which revisions should receive traffic and to specify traffic percentages that are received by a revision

Anyway principles for CloudRun and CloundRun for Anthos are the same. Traffic can be split between multiple revisions.

 
167. Your company developed a mobile game that is deployed on Google Cloud. Gamers are connecting to the game with their personal phones over the Internet. The game sends UDP packets to update the servers about the gamers' actions while they are playing in multiplayer mode. Your game backend can scale over multiple virtual machines (VMs), and you want to expose the VMs over a single IP address. What should you do?

    A. Configure an SSL Proxy load balancer in front of the application servers.

    B. Configure an Internal UDP load balancer in front of the application servers.

    C. Configure an External HTTP(s) load balancer in front of the application servers.

    D. Configure an External Network load balancer in front of the application servers. 

Answer is D. theres so much confusion here, from B,C or D. For myself im eliminating all options except B,D due to the traffic type. which leaves me with B or D. Then next the traffic source either external or internal which in this case is an external traffic from the internet, therefore my final answer is D.

https://cloud.google.com/load-balancing/docs/choosing-load-balancer


168. You are working for a hospital that stores its medical images in an on-premises data room. The hospital wants to use Cloud Storage for archival storage of these images. The hospital wants an automated process to upload any new medical images to Cloud Storage. You need to design and implement a solution. What should you do?

    A. Create a Pub/Sub topic, and enable a Cloud Storage trigger for the Pub/Sub topic. Create an application that sends all medical images to the Pub/Sub topic.

    B. Deploy a Dataflow job from the batch template, ג€Datastore to Cloud Storage.ג€ Schedule the batch job on the desired interval.

    C. Create a script that uses the gsutil command line interface to synchronize the on-premises storage with Cloud Storage. Schedule the script as a cron job.

    D. In the Cloud Console, go to Cloud Storage. Upload the relevant images to the appropriate bucket.

I would go with C

Not A, don’t think you can send image files to Pub/Sub. Technically you can do so by converting image to some binary text, but then we don’t know the size of the image and there is a limitation on message size. Not recommended.

Not B – there is only this template “Datastore to Cloud Storage Text”, as the name implies it is for text, https://cloud.google.com/dataflow/docs/guides/templates/provided-batch#datastore-to-cloud-storage-text, and it reads from datastore which is definitely not where the medical images are stored, from the question “… stores its medical images in an on-premises data room”.

Not D – it’s not automated

 
169. Your auditor wants to view your organization's use of data in Google Cloud. The auditor is most interested in auditing who accessed data in Cloud Storage buckets. You need to help the auditor access the data they need. What should you do?

    A. Turn on Data Access Logs for the buckets they want to audit, and then build a query in the log viewer that filters on Cloud Storage.

    B. Assign the appropriate permissions, and then create a Data Studio report on Admin Activity Audit Logs.

    C. Assign the appropriate permissions, and then use Cloud Monitoring to review metrics.

    D. Use the export logs API to provide the Admin Activity Audit Logs in the format they want.

A is the correct answer,
Since the auditor wants to know who accessed the cloud storage data, we need data acces logs for cloud storage.

Types of audit logs
Cloud Audit Logs provides the following audit logs for each Cloud project, folder, and organization:

Admin Activity audit logs
Data Access audit logs
System Event audit logs
Policy Denied audit logs

***Data Access audit logs contain API calls that read the configuration or metadata of resources, as well as user-driven API calls that create, modify, or read user-provided resource data.

https://cloud.google.com/logging/docs/audit#types

    
170. You received a JSON file that contained a private key of a Service Account in order to get access to several resources in a Google Cloud project. You downloaded and installed the Cloud SDK and want to use this private key for authentication and authorization when performing gcloud commands. What should you do?

    A. Use the command gcloud auth login and point it to the private key.

    B. Use the command gcloud auth activate-service-account and point it to the private key.

    C. Place the private key file in the installation directory of the Cloud SDK and rename it to ג€credentials.jsonג€.

    D. Place the private key file in your home directory and rename it to ג€GOOGLE_APPLICATION_CREDENTIALSג€.

B. Use the command gcloud auth activate-service-account and point it to the private key.

Authorizing with a service account
gcloud auth activate-service-account authorizes access using a service account. As with gcloud init and gcloud auth login, this command saves the service account credentials to the local system on successful completion and sets the specified account as the active account in your Cloud SDK configuration.

https://cloud.google.com/sdk/docs/authorizing#authorizing_with_a_service_account

 
171. You are working with a Cloud SQL MySQL database at your company. You need to retain a month-end copy of the database for three years for audit purposes. What should you do?

    A. Set up an export job for the first of the month. Write the export file to an Archive class Cloud Storage bucket.

    B. Save the automatic first-of-the-month backup for three years. Store the backup file in an Archive class Cloud Storage bucket.

    C. Set up an on-demand backup for the first of the month. Write the backup to an Archive class Cloud Storage bucket.

    D. Convert the automatic first-of-the-month backup to an export file. Write the export file to a Coldline class Cloud Storage bucket.

A - https://cloud.google.com/sql/docs/mysql/backup-recovery/backups

not B: Automatic backups are made EVERY SINGLE DAY. You can set only the number of backups up to 365. Also you cannot choose your Archival storage as destination

not C: You cannot setup "on-demand" backup. User would have to make backups manually every month. Also you cannot choose your Archival storage as destination

not D: You cannot conver backup to export file. Also Coldline class is less cost-effective than Archival class.

The only option left is "A"
You can set up your job with any date/time schedule. You can export file to any storage with any storage class.
 
172. You are monitoring an application and receive user feedback that a specific error is spiking. You notice that the error is caused by a Service Account having insufficient permissions. You are able to solve the problem but want to be notified if the problem recurs. What should you do?

    A. In the Log Viewer, filter the logs on severity 'Error' and the name of the Service Account.

    B. Create a sink to BigQuery to export all the logs. Create a Data Studio dashboard on the exported logs.

    C. Create a custom log-based metric for the specific error to be used in an Alerting Policy.

    D. Grant Project Owner access to the Service Account.

C is the correct answer,
Since the problem is resolved, We need to monitor if the error recurs, hence we create a custom log based metrics to monitor only the particular service account.

 
173. You are developing a financial trading application that will be used globally. Data is stored and queried using a relational structure, and clients from all over the world should get the exact identical state of the data. The application will be deployed in multiple regions to provide the lowest latency to end users. You need to select a storage option for the application data while minimizing latency. What should you do?

    A. Use Cloud Bigtable for data storage.

    B. Use Cloud SQL for data storage.

    C. Use Cloud Spanner for data storage.

    D. Use Firestore for data storage.

C, Cloud Spanner, keywords are globally, relational structure and lastly "clients from all over the world should get the exact identical state of the data" which implies strong consistency is needed.
 
174. You are about to deploy a new Enterprise Resource Planning (ERP) system on Google Cloud. The application holds the full database in-memory for fast data access, and you need to configure the most appropriate resources on Google Cloud for this application. What should you do?

    A. Provision preemptible Compute Engine instances.

    B. Provision Compute Engine instances with GPUs attached.

    C. Provision Compute Engine instances with local SSDs attached.

    D. Provision Compute Engine instances with M1 machine type. 

D is the correct answer,
M1 machine series
Medium in-memory databases such as SAP HANA
Tasks that require intensive use of memory with higher memory-to-vCPU ratios than the general-purpose high-memory machine types.
In-memory databases and in-memory analytics, business warehousing (BW) workloads, genomics analysis, SQL analysis services.
Microsoft SQL Server and similar databases.

https://cloud.google.com/compute/docs/machine-types


175. You have developed an application that consists of multiple microservices, with each microservice packaged in its own Docker container image. You want to deploy the entire application on Google Kubernetes Engine so that each microservice can be scaled individually. What should you do?

    A. Create and deploy a Custom Resource Definition per microservice.

    B. Create and deploy a Docker Compose File.

    C. Create and deploy a Job per microservice.

    D. Create and deploy a Deployment per microservice.

I was a little unsure about this question, here's how I understand why D is the best answer

A. Custom Resource Definition... we have docker containers already, which is an established kind of resource for Kubernetes. We don't need to create a whole new type of resource, so this is wrong.
B. Docker Compose is a wholly different tool from Kubernetes.
C. A Kubernetes job describes a specific "task" which involves a bunch of pods and things. It makes no sense to have one job per microservice, a "Job" would be a bunch of different microservices executing together.
D. is the leftover, correct answer. You can add scaling to each Deployment, an important aspect of the question.


 
176. You will have several applications running on different Compute Engine instances in the same project. You want to specify at a more granular level the service account each instance uses when calling Google Cloud APIs. What should you do?

    A. When creating the instances, specify a Service Account for each instance.

    B. When creating the instances, assign the name of each Service Account as instance metadata.

    C. After starting the instances, use gcloud compute instances update to specify a Service Account for each instance.

    D. After starting the instances, use gcloud compute instances update to assign the name of the relevant Service Account as instance metadata.

Vote for A, because there is no instance running yet. "You will have several applications running..."

https://cloud.google.com/compute/docs/access/service-accounts#associating_a_service_account_to_an_instance

177. You are creating an application that will run on Google Kubernetes Engine. You have identified MongoDB as the most suitable database system for your application and want to deploy a managed MongoDB environment that provides a support SLA. What should you do?

    A. Create a Cloud Bigtable cluster, and use the HBase API.

    B. Deploy MongoDB Atlas from the Google Cloud Marketplace.

    C. Download a MongoDB installation package, and run it on Compute Engine instances.

    D. Download a MongoDB installation package, and run it on a Managed Instance Group.

B
MongoDB Atlas is actually managed and supported by third-party service providers.

https://console.cloud.google.com/marketplace/details/gc-launcher-for-mongodb-atlas/mongodb-atlas
 
178. You are managing a project for the Business Intelligence (BI) department in your company. A data pipeline ingests data into BigQuery via streaming. You want the users in the BI department to be able to run the custom SQL queries against the latest data in BigQuery. What should you do?

    A. Create a Data Studio dashboard that uses the related BigQuery tables as a source and give the BI team view access to the Data Studio dashboard.

    B. Create a Service Account for the BI team and distribute a new private key to each member of the BI team.

    C. Use Cloud Scheduler to schedule a batch Dataflow job to copy the data from BigQuery to the BI team's internal data warehouse.

    D. Assign the IAM role of BigQuery User to a Google Group that contains the members of the BI team. 

D is correct
roles/bigquery.user
When applied to a dataset, this role provides the ability to read the dataset's metadata and list tables in the dataset.
When applied to a project, this role also provides the ability to run jobs, including queries, within the project. A member with this role can enumerate their own jobs, cancel their own jobs, and enumerate datasets within a project. Additionally, allows the creation of new datasets within the project; the creator is granted the BigQuery Data Owner role (roles/bigquery.dataOwner) on these new datasets.

https://cloud.google.com/dataflow/docs/guides/data-pipelines#create_a_batch_data_pipeline

To create this sample batch data pipeline, you must have access to the following resources in your project:

A Cloud Storage bucket to store input and output files
A BigQuery dataset where you will create a table.


179. Your company is moving its entire workload to Compute Engine. Some servers should be accessible through the Internet, and other servers should only be accessible over the internal network. All servers need to be able to talk to each other over specific ports and protocols. The current on-premises network relies on a demilitarized zone (DMZ) for the public servers and a Local Area Network (LAN) for the private servers. You need to design the networking infrastructure on Google Cloud to match these requirements. What should you do?

    A. 1. Create a single VPC with a subnet for the DMZ and a subnet for the LAN. 2. Set up firewall rules to open up relevant traffic between the DMZ and the LAN subnets, and another firewall rule to allow public ingress traffic for the DMZ.

    B. 1. Create a single VPC with a subnet for the DMZ and a subnet for the LAN. 2. Set up firewall rules to open up relevant traffic between the DMZ and the LAN subnets, and another firewall rule to allow public egress traffic for the DMZ.

    C. 1. Create a VPC with a subnet for the DMZ and another VPC with a subnet for the LAN. 2. Set up firewall rules to open up relevant traffic between the DMZ and the LAN subnets, and another firewall rule to allow public ingress traffic for the DMZ.

    D. 1. Create a VPC with a subnet for the DMZ and another VPC with a subnet for the LAN. 2. Set up firewall rules to open up relevant traffic between the DMZ and the LAN subnets, and another firewall rule to allow public egress traffic for the DMZ.

A is the Right answer. You can discard B and C because they lack the need of creating Network Peering to communicate the DMZ VPC with the LAN VPC (LAN VPC is not exposed to public so they need to communicate via private addresses which cannot be achieved with 2 VPCs without Network Peering). Plus, you can discard B, as you don't need to enable the egress traffic, you always need to enable the ingress traffic as this is never enabled by default.

 
180. You have just created a new project which will be used to deploy a globally distributed application. You will use Cloud Spanner for data storage. You want to create a Cloud Spanner instance. You want to perform the first step in preparation of creating the instance. What should you do?

    A. Enable the Cloud Spanner API.

    B. Configure your Cloud Spanner instance to be multi-regional.

    C. Create a new VPC network with subnetworks in all desired regions.
    
    D. Grant yourself the IAM role of Cloud Spanner Admin.

 It's A
Since it does not specify if we are using command line tool or UI, if you are using command line tool then you will have to enable this.