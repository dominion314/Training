181. You have created a new project in Google Cloud through the gcloud command line interface (CLI) and linked a billing account. You need to create a new ComputeEngine instance using the CLI. You need to perform the prerequisite steps. What should you do?

    A. Create a Cloud Monitoring Workspace.

    B. Create a VPC network in the project.

    C. Enable the compute googleapis.com API.

    D. Grant yourself the IAM role of Computer Admin.

C
nothing can be done before activating the API

 
182. Your company has developed a new application that consists of multiple microservices. You want to deploy the application to Google Kubernetes Engine (GKE), and you want to ensure that the cluster can scale as more applications are deployed in the future. You want to avoid manual intervention when each new application is deployed. What should you do?

    A. Deploy the application on GKE, and add a HorizontalPodAutoscaler to the deployment.

    B. Deploy the application on GKE, and add a VerticalPodAutoscaler to the deployment.

    C. Create a GKE cluster with autoscaling enabled on the node pool. Set a minimum and maximum for the size of the node pool.

    D. Create a separate node pool for each application, and deploy each application to its dedicated node pool.

C is the right choice... See this for reference https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-autoscaler

 
183. You need to manage a third-party application that will run on a Compute Engine instance. Other Compute Engine instances are already running with default configuration. Application installation files are hosted on Cloud Storage. You need to access these files from the new instance without allowing other virtual machines (VMs) to access these files. What should you do?

    A. Create the instance with the default Compute Engine service account. Grant the service account permissions on Cloud Storage.

    B. Create the instance with the default Compute Engine service account. Add metadata to the objects on Cloud Storage that matches the metadata on the new instance.

    C. Create a new service account and assign this service account to the new instance. Grant the service account permissions on Cloud Storage.

    D. Create a new service account and assign this service account to the new instance. Add metadata to the objects on Cloud Storage that matches the metadata on the new instance.

C, using Default account makes the storage visible to other machines
 
184. You need to configure optimal data storage for files stored in Cloud Storage for minimal cost. The files are used in a mission-critical analytics pipeline that is used continually. The users are in Boston, MA (United States). What should you do?

    A. Configure regional storage for the region closest to the users. Configure a Nearline storage class.

    B. Configure regional storage for the region closest to the users. Configure a Standard storage class.

    C. Configure dual-regional storage for the dual region closest to the users. Configure a Nearline storage class.

    D. Configure dual-regional storage for the dual region closest to the users. Configure a Standard storage class. 

D
Mission critical is the keyword here which specifies that we need to have a multi-regional backup of the data to survive any regional failures.

Could also be B since it mentions one specific city/region. 

185. You are developing a new web application that will be deployed on Google Cloud Platform. As part of your release cycle, you want to test updates to your application on a small portion of real user traffic. The majority of the users should still be directed towards a stable version of your application. What should you do?

    A. Deploy the application on App Engine. For each update, create a new version of the same service. Configure traffic splitting to send a small percentage of traffic to the new version.

    B. Deploy the application on App Engine. For each update, create a new service. Configure traffic splitting to send a small percentage of traffic to the new service.

    C. Deploy the application on Kubernetes Engine. For a new release, update the deployment to use the new version.

    D. Deploy the application on Kubernetes Engine. For a new release, create a new deployment for the new version. Update the service to use the new deployment.

A is correct answer,
Keyword, Version, traffic splitting, App Engine supports traffic splitting for versions before releasing.

 
186. You need to add a group of new users to Cloud Identity. Some of the users already have existing Google accounts. You want to follow one of Google's recommended practices and avoid conflicting accounts. What should you do?

    A. Invite the user to transfer their existing account.

    B. Invite the user to use an email alias to resolve the conflict.

    C. Tell the user that they must delete their existing account.

    D. Tell the user to remove all personal email from the existing account.

https://cloud.google.com/architecture/identity/assessing-existing-user-accounts

If you want to maintain the access rights and some of the data associated with the Gmail account, you can ask the owner to remove Gmail from the user account so that you can then migrate them to Cloud Identity or Google Workspace.
 
187. You need to manage a Cloud Spanner instance for best query performance. Your instance in production runs in a single Google Cloud region. You need to improve performance in the shortest amount of time. You want to follow Google best practices for service configuration. What should you do?

    A. Create an alert in Cloud Monitoring to alert when the percentage of high priority CPU utilization reaches 45%. If you exceed this threshold, add nodes to your instance.

    B. Create an alert in Cloud Monitoring to alert when the percentage of high priority CPU utilization reaches 45%. Use database query statistics to identify queries that result in high CPU usage, and then rewrite those queries to optimize their resource usage.

    C. Create an alert in Cloud Monitoring to alert when the percentage of high priority CPU utilization reaches 65%. If you exceed this threshold, add nodes to your instance.

    D. Create an alert in Cloud Monitoring to alert when the percentage of high priority CPU utilization reaches 65%. Use database query statistics to identify queries that result in high CPU usage, and then rewrite those queries to optimize their resource usage.

C looks correct, increase instances on single region if CPU above 65%
https://cloud.google.com/spanner/docs/cpu-utilization#recommended-max
 
188. Your company has an internal application for managing transactional orders. The application is used exclusively by employees in a single physical location. The application requires strong consistency, fast queries, and ACID guarantees for multi-table transactional updates. The first version of the application is implemented in PostgreSQL, and you want to deploy it to the cloud with minimal code changes. Which database is most appropriate for this application?

    A. BigQuery

    B. Cloud SQL

    C. Cloud Spanner

    D. Cloud Datastore

B, The application is used exclusively by employees in a single physical location. Spanner is used for global scaling, not internally in a single location. B is the correct answer here.
 
189. You are assigned to maintain a Google Kubernetes Engine (GKE) cluster named 'dev' that was deployed on Google Cloud. You want to manage the GKE configuration using the command line interface (CLI). You have just downloaded and installed the Cloud SDK. You want to ensure that future CLI commands by default address this specific cluster What should you do?

    A. Use the command gcloud config set container/cluster dev.

    B. Use the command gcloud container clusters update dev.

    C. Create a file called gke.default in the ~/.gcloud folder that contains the cluster name.

    D. Create a file called defaults.json in the ~/.gcloud folder that contains the cluster name.

A
To set a default cluster for gcloud commands, run the following command:
https://cloud.google.com/kubernetes-engine/docs/how-to/managing-clusters
 
190. The sales team has a project named Sales Data Digest that has the ID acme-data-digest. You need to set up similar Google Cloud resources for the marketing team but their resources must be organized independently of the sales team. What should you do?

    A. Grant the Project Editor role to the Marketing team for acme-data-digest.

    B. Create a Project Lien on acme-data-digest and then grant the Project Editor role to the Marketing team.

    C. Create another project with the ID acme-marketing-data-digest for the Marketing team and deploy the resources there.

    D. Create a new project named Marketing Data Digest and use the ID acme-data-digest. Grant the Project Editor role to the Marketing team.

C because the resources for the marketing team should be independent from the Sales team. Resources are tied and separated by projects.

 
191. You have deployed multiple Linux instances on Compute Engine. You plan on adding more instances in the coming weeks. You want to be able to access all of these instances through your SSH client over the internet without having to configure specific access on the existing and new instances. You do not want the Compute Engine instances to have a public IP. What should you do?

    A. Configure Cloud Identity-Aware Proxy for HTTPS resources.

    B. Configure Cloud Identity-Aware Proxy for SSH and TCP resources

    C. Create an SSH keypair and store the public key as a project-wide SSH Key.

    D. Create an SSH keypair and store the private key as a project-wide SSH Key.

B is correct as question say no public IP on the instance.

Use IAP TCP to enable access to VM instances that do not have external IP addresses or do not permit direct access over the internet.
https://cloud.google.com/iap/docs/using-tcp-forwarding

 
192. You have created an application that is packaged into a Docker image. You want to deploy the Docker image as a workload on Google Kubernetes Engine. What should you do?

    A. Upload the image to Cloud Storage and create a Kubernetes Service referencing the image.

    B. Upload the image to Cloud Storage and create a Kubernetes Deployment referencing the image.

    C. Upload the image to Container Registry and create a Kubernetes Service referencing the image.

    D. Upload the image to Container Registry and create a Kubernetes Deployment referencing the image.

D, A deployment is responsible for keeping a set of pods running. A service is responsible for enabling network access to a set of pods. Also you can only pull images from the registry

193. You are using Data Studio to visualize a table from your data warehouse that is built on top of BigQuery. Data is appended to the data warehouse during the day. At night, the daily summary is recalculated by overwriting the table. You just noticed that the charts in Data Studio are broken, and you want to analyze the problem. What should you do?

    A. Review the Error Reporting page in the Cloud Console to find any errors.

    B. Use the BigQuery interface to review the nightly job and look for any errors.

    C. Use Cloud Debugger to find out why the data was not refreshed correctly.

    D. In Cloud Logging, create a filter for your Data Studio report. 

D because cloud logging is enabled by default. Cloud Debugger is something we need to configure manually in our code. Its more direct than A


194. You have been asked to set up the billing configuration for a new Google Cloud customer. Your customer wants to group resources that share common IAM policies. What should you do?

    A. Use labels to group resources that share common IAM policies.

    B. Use folders to group resources that share common IAM policies.

    C. Set up a proper billing account structure to group IAM policies.

    D. Set up a proper project naming structure to group IAM policies.

B is correct Answer,

Folders are nodes in the Cloud Platform Resource Hierarchy. A folder can contain projects, other folders, or a combination of both. Organizations can use folders to group projects under the organization node in a hierarchy. For example, your organization might contain multiple departments, each with its own set of Google Cloud resources. Folders allow you to group these resources on a per-department basis. Folders are used to group resources that share common IAM policies. While a folder can contain multiple folders or resources, a given folder or resource can have exactly one parent.

https://cloud.google.com/resource-manager/docs/creating-managing-folders
 
195. You have been asked to create robust Virtual Private Network (VPN) connectivity between a new Virtual Private Cloud (VPC) and a remote site. Key requirements include dynamic routing, a shared address space of 10.19.0.1/22, and no overprovisioning of tunnels during a failover event. You want to follow Google- recommended practices to set up a high availability Cloud VPN. What should you do?

    A. Use a custom mode VPC network, configure static routes, and use active/passive routing.

    B. Use an automatic mode VPC network, configure static routes, and use active/active routing.

    C. Use a custom mode VPC network, use Cloud Router border gateway protocol (BGP) routes, and use active/passive routing.

    D. Use an automatic mode VPC network, use Cloud Router border gateway protocol (BGP) routes, and configure policy-based routing.

C . Choose a Cloud VPN gateway that uses dynamic routing and the Border Gateway Protocol (BGP). Google recommends using HA VPN and deploying on-premises devices that support BGP.

Choose the appropriate tunnel configuration
Choose the appropriate tunnel configuration based on the number of HA VPN gateways:

If you have a single HA VPN gateway, use an active/passive tunnel configuration.

If you have more than one HA VPN gateway, use an active/active tunnel configuration.

https://cloud.google.com/network-connectivity/docs/vpn/concepts/best-practices

we need custom mode vpc so subnets are not created automatically (the ip range is mentioned in the question) also we will need active/passive HA VPN (as it is not mentioned we will have to use more than one HA VPN gateway).

Links : https://cloud.google.com/network-connectivity/docs/vpn/concepts/best-practices
https://cloud.google.com/network-connectivity/docs/vpn/concepts/overview#active
https://cloud.google.com/vpc/docs/vpc#subnet-ranges
 
196. You are running multiple microservices in a Kubernetes Engine cluster. One microservice is rendering images. The microservice responsible for the image rendering requires a large amount of CPU time compared to the memory it requires. The other microservices are workloads that are optimized for n1-standard machine types. You need to optimize your cluster so that all workloads are using resources as efficiently as possible. What should you do?

    A. Assign the pods of the image rendering microservice a higher pod priority than the other microservices.

    B. Create a node pool with compute-optimized machine type nodes for the image rendering microservice. Use the node pool with general-purpose machine type nodes for the other microservices.

    C. Use the node pool with general-purpose machine type nodes for the image rendering microservice. Create a node pool with compute-optimized machine type nodes for the other microservices.

    D. Configure the required amount of CPU and memory in the resource requests specification of the image rendering microservice deployment. Keep the resource requests for the other microservices at the default.

B is the most suitable answer.
C is not correct coz general purpose machine types will not suffice for image rendering.

 
197. Your organization has three existing Google Cloud projects. You need to bill the Marketing department for only their Google Cloud services for a new initiative within their group. What should you do?

    A. 1. Verify that you are assigned the Billing Administrator IAM role for your organization's Google Cloud Project for the Marketing department. 2. Link the new project to a Marketing Billing Account.

    B. 1. Verify that you are assigned the Billing Administrator IAM role for your organization's Google Cloud account. 2. Create a new Google Cloud Project for the Marketing department. 3. Set the default key-value project labels to department:marketing for all services in this project.

    C. 1. Verify that you are assigned the Organization Administrator IAM role for your organization's Google Cloud account. 2. Create a new Google Cloud Project for the Marketing department. 3. Link the new project to a Marketing Billing Account.
    
    D. 1. Verify that you are assigned the Organization Administrator IAM role for your organization's Google Cloud account. 2. Create a new Google Cloud Project for the Marketing department. 3. Set the default key-value project labels to department:marketing for all services in this project.

Option A seems the only valid answer as Billing Admin can link a new project to a billing acc

B & D assumes you have the resourcemanager.projects.create permission to create a new project. According to https://cloud.google.com/resource-manager/docs/creating-managing-labels#permissions resourcemanager.projects.update permission is needed to edit the labels
neither Billing Adminstrator nor Organization Adminstrator has resourcemanager.projects.update permission to edit the labels they only can see the labels of a project as both have only resourcemanager.projects.get permission so Options B & D are both wrong

C is wrong because neither does Organizational admin have resourcemanager.projects.create permission to create a project nor does it have permission to link a project to a billing account - > https://cloud.google.com/iam/docs/understanding-roles#resourcemanager.organizationAdmin