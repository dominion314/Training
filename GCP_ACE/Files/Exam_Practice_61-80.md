61. Your organization is a financial company that needs to store audit log files for 3 years. Your organization has hundreds of Google Cloud projects. You need to implement a cost-effective approach for log file retention. What should you do?

    A. Create an export to the sink that saves logs from Cloud Audit to BigQuery.

    <b>B. Create an export to the sink that saves logs from Cloud Audit to a Coldline Storage bucket.</b>

    C. Write a custom script that uses logging API to copy the logs from Stackdriver logs to BigQuery.

    D. Export these logs to Cloud Pub/Sub and write a Cloud Dataflow pipeline to store logs to Cloud SQL.

<i>The question is clearly saying cost effect. BQ is one of the most expensive services in GCP.</i>


62. You want to run a single caching HTTP reverse proxy on GCP for a latency-sensitive website. This specific reverse proxy consumes almost no CPU. You want to have a 30-GB in-memory cache, and need an additional 2 GB of memory for the rest of the processes. You want to minimize cost. How should you run this reverse proxy?

    A. Create a Cloud Memorystore for Redis instance with 32-GB capacity.

    <b>B. Run it on Compute Engine, and choose a custom instance type with 6 vCPUs and 32 GB of memory.</b>

    C. Package it in a container image, and run it on Kubernetes Engine, using n1-standard-32 instances as nodes.

    D. Run it on Compute Engine, choose the instance type n1-standard-1, and add an SSD persistent disk of 32 GB.

<i>MemoryStore Pricing is $0.023/GB/hr, for 32GB means $0.736/hr compares to $0.239/hr. The question states that it needs additional 2GB for it's process, which mean if you choose A, you will need another vm with 2GB ram either.</i>


63. You are hosting an application on bare-metal servers in your own data center. The application needs access to Cloud Storage. However, security policies prevent the servers hosting the application from having public IP addresses or access to the internet. You want to follow Google-recommended practices to provide the application with access to Cloud Storage. What should you do?

    A. 1. Use nslookup to get the IP address for storage.googleapis.com. 2. Negotiate with the security team to be able to give a public IP address to the servers. 3. Only allow egress traffic from those servers to the IP addresses for storage.googleapis.com.

    B. 1. Using Cloud VPN, create a VPN tunnel to a Virtual Private Cloud (VPC) in Google Cloud. 2. In this VPC, create a Compute Engine instance and install the Squid proxy server on this instance. 3. Configure your servers to use that instance as a proxy to access Cloud Storage.

    C. 1. Use Migrate for Compute Engine (formerly known as Velostrata) to migrate those servers to Compute Engine. 2. Create an internal load balancer (ILB) that uses storage.googleapis.com as backend. 3. Configure your new instances to use this ILB as proxy.

    <b>D. 1. Using Cloud VPN or Interconnect, create a tunnel to a VPC in Google Cloud. 2. Use Cloud Router to create a custom route advertisement for 199.36.153.4/30. Announce that network to your on-premises network through the VPN tunnel. 3. In your on-premises network, configure your DNS server to resolve *.googleapis.com as a CNAME to restricted.googleapis.com.</b>

<i>D is the correct one as per Ref: https://cloud.google.com/vpc/docs/configure-private-google-access-hybrid</i>


64. You want to deploy an application on Cloud Run that processes messages from a Cloud Pub/Sub topic. You want to follow Google-recommended practices. What should you do?

    A. 1. Create a Cloud Function that uses a Cloud Pub/Sub trigger on that topic. 2. Call your application on Cloud Run from the Cloud Function for every message.

    B. 1. Grant the Pub/Sub Subscriber role to the service account used by Cloud Run. 2. Create a Cloud Pub/Sub subscription for that topic. 3. Make your application pull messages from that subscription.

    <b>C. 1. Create a service account. 2. Give the Cloud Run Invoker role to that service account for your Cloud Run application. 3. Create a Cloud Pub/Sub subscription that uses that service account and uses your Cloud Run application as the push endpoint.</b>

    D. 1. Deploy your application on Cloud Run on GKE with the connectivity set to Internal. 2. Create a Cloud Pub/Sub subscription for that topic. 3. In the same Google Kubernetes Engine cluster as your application, deploy a container that takes the messages and sends them to your application.
    
<i>You can use Pub/Sub to push messages to the endpoint of your Cloud Run service, where the messages are subsequently delivered to containers as HTTP requests. You cannot use Pub/Sub pull subscriptions because Cloud Run only allocates CPU during the processing of a request.</i>


65. You need to deploy an application, which is packaged in a container image, in a new project. The application exposes an HTTP endpoint and receives very few requests per day. You want to minimize costs. What should you do?

    <b>A. Deploy the container on Cloud Run.</b>

    B. Deploy the container on Cloud Run on GKE.

    C. Deploy the container on App Engine Flexible.

    D. Deploy the container on GKE with cluster autoscaling and horizontal pod autoscaling enabled.

<i>Cloud Run takes any container images and pairs great with the container ecosystem: Cloud Build, Artifact Registry, Docker. ... No infrastructure to manage: once deployed, Cloud Run manages your services so you can sleep well. Fast autoscaling. Cloud Run automatically scales up or down from zero to N depending on traffic.

https://cloud.google.com/run</i>


66. Your company has an existing GCP organization with hundreds of projects and a billing account. Your company recently acquired another company that also has hundreds of projects and its own billing account. You would like to consolidate all GCP costs of both GCP organizations onto a single invoice. You would like to consolidate all costs as of tomorrow. What should you do?

    <b>A. Link the acquired company's projects to your company's billing account.</b>

    B. Configure the acquired company's billing account and your company's billing account to export the billing data into the same BigQuery dataset.

    C. Migrate the acquired company's projects into your company's GCP organization. Link the migrated projects to your company's billing account.

    D. Create a new GCP organization and a new billing account. Migrate the acquired company's projects and your company's projects into the new GCP organization and link the projects to the new billing account.

<i>A is correct because linking all projects of the acquired organization to the main organization’s billing account will generate a single bill for all projects.
D is incorrect because there is no need to create a new organization for this.  The keywords are "as of tomorrow" and "single invoice". The quickest(the only?) way of achieving this is by Linking. The proper way would be under C, but that's not possible in 24h or less</i>

67. You built an application on Google Cloud that uses Cloud Spanner. Your support team needs to monitor the environment but should not have access to table data. You need a streamlined solution to grant the correct permissions to your support team, and you want to follow Google-recommended practices. What should you do?

    <b>A. Add the support team group to the roles/monitoring.viewer role</b>

    B. Add the support team group to the roles/spanner.databaseUser role.

    C. Add the support team group to the roles/spanner.databaseReader role.

    D. Add the support team group to the roles/stackdriver.accounts.viewer role.

<i>A, right, correct answer. You only need monitor

B and C are incorrect because allow to read data.

D also incorrect: Not for monitoring. roles/stackdriver.accounts.viewer Stackdriver Accounts Viewer:
Read-only access to get and list information about Stackdriver account structure (resourcemanager.projects.get, resourcemanager.projects.list and stackdriver.projects.get)

https://cloud.google.com/iam/docs/understanding-roles
</i>

68. For analysis purposes, you need to send all the logs from all of your Compute Engine instances to a BigQuery dataset called platform-logs. You have already installed the Cloud Logging agent on all the instances. You want to minimize cost. What should you do?

    A. 1. Give the BigQuery Data Editor role on the platform-logs dataset to the service accounts used by your instances. 2. Update your instances' metadata to add the following value: logs-destination: bq://platform-logs.

    B. 1. In Cloud Logging, create a logs export with a Cloud Pub/Sub topic called logs as a sink. 2. Create a Cloud Function that is triggered by messages in the logs topic. 3. Configure that Cloud Function to drop logs that are not from Compute Engine and to insert Compute Engine logs in the platform-logs dataset.

    <b>C. 1. In Cloud Logging, create a filter to view only Compute Engine logs. 2. Click Create Export. 3. Choose BigQuery as Sink Service, and the platform-logs dataset as Sink Destination.</b>

    D. 1. Create a Cloud Function that has the BigQuery User role on the platform-logs dataset. 2. Configure this Cloud Function to create a BigQuery Job that executes this query: INSERT INTO dataset.platform-logs (timestamp, log) SELECT timestamp, log FROM compute.logs WHERE timestamp > DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY) 3. Use Cloud Scheduler to trigger this Cloud Function once a day.

<i>https://cloud.google.com/logging/docs/export/configure_export_v2</i>

69. You are using Deployment Manager to create a Google Kubernetes Engine cluster. Using the same Deployment Manager deployment, you also want to create a DaemonSet in the kube-system namespace of the cluster. You want a solution that uses the fewest possible services. What should you do?

    <b>A. Add the cluster's API as a new Type Provider in Deployment Manager, and use the new type to create the DaemonSet.</b>

    B. Use the Deployment Manager Runtime Configurator to create a new Config resource that contains the DaemonSet definition.

    C. With Deployment Manager, create a Compute Engine instance with a startup script that uses kubectl to create the DaemonSet.

    D. In the cluster's definition in Deployment Manager, add a metadata that has kube-system as key and the DaemonSet manifest as value.

<i>Correct Answer is (A):

Adding an API as a type provider
This page describes how to add an API to Google Cloud Deployment Manager as a type provider. To learn more about types and type providers, read the Types overview documentation.

A type provider exposes all of the resources of a third-party API to Deployment Manager as base types that you can use in your configurations. These types must be directly served by a RESTful API that supports Create, Read, Update, and Delete (CRUD).

If you want to use an API that is not automatically provided by Google with Deployment Manager, you must add the API as a type provider.

https://cloud.google.com/deployment-manager/docs/configuration/type-providers/creating-type-provider</i>


70. You are building an application that will run in your data center. The application will use Google Cloud Platform (GCP) services like AutoML. You created a service account that has appropriate access to AutoML. You need to enable authentication to the APIs from your on-premises environment. What should you do?

    A. Use service account credentials in your on-premises application.

    <b>B. Use gcloud to create a key file for the service account that has appropriate permissions.</b>

    C. Set up direct interconnect between your data center and Google Cloud Platform to enable authentication for your on-premises applications.

    D. Go to the IAM & admin console, grant a user account permissions similar to the service account permissions, and use this user account for authentication from your data center.

<i>B is right
To use a service account from outside of Google Cloud, such as on other platforms or on-premises, you must first establish the identity of the service account. Public/private key pairs provide a secure way of accomplishing this goal. When you create a service account key, the public portion is stored on Google Cloud, while the private portion is available only to you. For more information about public/private key pairs, see Service account keys. https://cloud.google.com/iam/docs/creating-managing-service-account-keys
</i>


71. You are using Container Registry to centrally store your company's container images in a separate project. In another project, you want to create a GoogleKubernetes Engine (GKE) cluster. You want to ensure that Kubernetes can download images from Container Registry. What should you do?

    <b>A. In the project where the images are stored, grant the Storage Object Viewer IAM role to the service account used by the Kubernetes nodes.</b>

    B. When you create the GKE cluster, choose the Allow full access to all Cloud APIs option under 'Access scopes'.

    C. Create a service account, and give it access to Cloud Storage. Create a P12 key for this service account and use it as an imagePullSecrets in Kubernetes.

    D. Configure the ACLs on each image in Cloud Storage to give read-only access to the default Compute Engine service account.

<i>Correct Answer (A):
IAM permissions
IAM permissions determine who can access resources. All users, service accounts, and other identities that interact with Container Registry must have the appropriate Cloud Storage permissions.

By default, Google Cloud use default service accounts to interact with resources within the same project. For example, the Cloud Build service account can both push and pull images when Container Registry is in the same project.

You must configure or modify permissions yourself if:

You are using a service account in one project to access Container Registry in a different project
You are using a default service account with read-only access to storage, but you want to both pull and push images
You are using a custom service account to interact with Container Registry

https://cloud.google.com/container-registry/docs/access-control</i>


72. You deployed a new application inside your Google Kubernetes Engine cluster using the YAML file specified below. You check the status of the deployed pods and notice that one of them is still in PENDING status. You want to find out why the pod is stuck in pending status. What should you do?

    A. Review details of the myapp-service Service object and check for error messages.

    B. Review details of the myapp-deployment Deployment object and check for error messages.

    <b>C. Review details of myapp-deployment-58ddbbb995-lp86m Pod and check for warning messages.</b>

    D. View logs of the container in myapp-deployment-58ddbbb995-lp86m pod and check for warning messages.

<i>C is correct,
Debugging Pods
The first step in debugging a Pod is taking a look at it. Check the current state of the Pod and recent events with the following command:

    kubectl describe pods ${POD_NAME}</i>


73. You are setting up a Windows VM on Compute Engine and want to make sure you can log in to the VM via RDP. What should you do?

    A. After the VM has been created, use your Google Account credentials to log in into the VM.

    <b>B. After the VM has been created, use gcloud compute reset-windows-password to retrieve the login credentials for the VM.</b>

    C. When creating the VM, add metadata to the instance using 'windows-password' as the key and a password as the value.

    D. After the VM has been created, download the JSON private key for the default Compute Engine service account. Use the credentials in the JSON file to log in to the VM.

<i>Correct Answer is B.
B. After the VM has been created, use gcloud compute reset-windows-password to retrieve the login credentials for the VM.

https://cloud.google.com/sdk/gcloud/reference/beta/compute/reset-windows-password</i>


74. You want to configure an SSH connection to a single Compute Engine instance for users in the dev1 group. This instance is the only resource in this particular Google Cloud Platform project that the dev1 users should be able to connect to. What should you do?

   <b>A. Set metadata to enable-oslogin=true for the instance. Grant the dev1 group the compute.osLogin role. Direct them to use the Cloud Shell to ssh to that instance.</b>

    B. Set metadata to enable-oslogin=true for the instance. Set the service account to no service account for that instance. Direct them to use the Cloud Shell to ssh to that instance.

    C. Enable block project wide keys for the instance. Generate an SSH key for each user in the dev1 group. Distribute the keys to dev1 users and direct them to use their third-party tools to connect.

    D. Enable block project wide keys for the instance. Generate an SSH key and associate the key with that instance. Distribute the key to dev1 users and direct them to use their third-party tools to connect.

<i>A is correct and recommended option.
D is incorrect because block project-wide restrict access to this instance, evidence: https://cloud.google.com/compute/docs/connect/restrict-ssh-keyshttps://cloud.google.com/compute/docs/connect/restrict-ssh-keys</i>

75. You need to produce a list of the enabled Google Cloud Platform APIs for a GCP project using the gcloud command line in the Cloud Shell. The project name is my-project. What should you do?

    <b>A. Run gcloud projects list to get the project ID, and then run gcloud services list --project <project ID>.</b>

    B. Run gcloud init to set the current project to my-project, and then run gcloud services list --available.

    C. Run gcloud info to view the account value, and then run gcloud services list --account <Account>.

    D. Run gcloud projects describe <project ID> to verify the project value, and then run gcloud services list --available.

<i>A is the correct answer, log to gcloud and run the commands, doesnt make sense to run cloud init and gcloud services list --available gives you the full services that are available.</i>


76. You are building a new version of an application hosted in an App Engine environment. You want to test the new version with 1% of users before you completely switch your application over to the new version. What should you do?

    A. Deploy a new version of your application in Google Kubernetes Engine instead of App Engine and then use GCP Console to split traffic.

    B. Deploy a new version of your application in a Compute Engine instance instead of App Engine and then use GCP Console to split traffic.

    C. Deploy a new version as a separate app in App Engine. Then configure App Engine using GCP Console to split traffic between the two apps.

    <b>D. Deploy a new version of your application in App Engine. Then go to App Engine settings in GCP Console and split traffic between the current version and newly deployed versions accordingly.</b>

<i>Splitting the question to the key requirements

1. new version of an application hosted in an App Engine environment.
2. test the new version with 1% of users

App engine supports versioning and traffic splitting so no need to involve anything else
(source - https://cloud.google.com/appengine#all-features)

A. ....'Google Kubernetes Engine'.... - No need to involve GKE. Not the right option
B. ....'Compute Engine instance'.... - No need to involve Compute Engine.
C. ....'Separate app in App Engine'....- No need to deploy as a separate app. versioning is supported already. Not the right option.
D. This is the right answer.</i>

77. You need to provide a cost estimate for a Kubernetes cluster using the GCP pricing calculator for Kubernetes. Your workload requires high IOPs, and you will also be using disk snapshots. You start by entering the number of nodes, average hours, and average days. What should you do next?

    <b>A. Fill in local SSD. Fill in persistent disk storage and snapshot storage.</b>

    B. Fill in local SSD. Add estimated cost for cluster management.

    C. Select Add GPUs. Fill in persistent disk storage and snapshot storage.

    D. Select Add GPUs. Add estimated cost for cluster management.

<i>This one is Tricky, local SSD is require for High IOPS - https://cloud.google.com/compute/docs/disks/local-ssd , but it say using disk snapshots. A is correct.</i>


78. You are using Google Kubernetes Engine with autoscaling enabled to host a new application. You want to expose this new application to the public, using HTTPS on a public IP address. What should you do?

    A. Create a Kubernetes Service of type NodePort for your application, and a Kubernetes Ingress to expose this Service via a Cloud Load Balancer.

    B. Create a Kubernetes Service of type ClusterIP for your application. Configure the public DNS name of your application using the IP of this Service.

    C. Create a Kubernetes Service of type NodePort to expose the application on port 443 of each node of the Kubernetes cluster. Configure the public DNS name of your application with the IP of every node of the cluster to achieve load-balancing.

    D. Create a HAProxy pod in the cluster to load-balance the traffic to all the pods of the application. Forward the public traffic to HAProxy with an iptable rule. Configure the DNS name of your application using the public IP of the node HAProxy is running on.

<i>HAProxy is HTTP only, doesnt support HTTPS, so you can reject option D
https://www.haproxy.org/#desc


Cluster IP - is an internal IP, you cannot expose public externally. reject option B


out of option A and C

C, port 443 is https but public DNS is not going to give you a load balancing
A is the right choice,
kubernets ingress exposes HTTPS
https://kubernetes.io/docs/concepts/services-networking/ingress/

and cloud load balancer is the right choice which will help to expose the app to public</i>


79. You need to enable traffic between multiple groups of Compute Engine instances that are currently running two different GCP projects. Each group of ComputeEngine instances is running in its own VPC. What should you do?

    A. Verify that both projects are in a GCP Organization. Create a new VPC and add all instances.

    <b>B. Verify that both projects are in a GCP Organization. Share the VPC from one project and request that the Compute Engine instances in the other project use this shared VPC.</b>

    C. Verify that you are the Project Administrator of both projects. Create two new VPCs and add all instances.

    D. Verify that you are the Project Administrator of both projects. Create a new VPC and add all instances.

<i>B - https://cloud.google.com/vpc/docs/shared-vpc</i>


80. You want to add a new auditor to a Google Cloud Platform project. The auditor should be allowed to read, but not modify, all project items. How should you configure the auditor's permissions?

    A. Create a custom role with view-only project permissions. Add the user's account to the custom role.

    B. Create a custom role with view-only service permissions. Add the user's account to the custom role.

    C. Select the built-in IAM project Viewer role. Add the user's account to this role.

    D. Select the built-in IAM service Viewer role. Add the user's account to this role.

<i>C is correct
roles/Viewer role provides access to all resources under the projects but do not alter the state of these resources</i>