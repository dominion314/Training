121. Your managed instance group raised an alert stating that new instance creation has failed to create new instances. You need to maintain the number of running instances specified by the template to be able to process expected application traffic. What should you do?

    A. Create an instance template that contains valid syntax which will be used by the instance group. Delete any persistent disks with the same name as instance names.

    B. Create an instance template that contains valid syntax that will be used by the instance group. Verify that the instance name and persistent disk name values are not the same in the template.

    C. Verify that the instance template being used by the instance group contains valid syntax. Delete any persistent disks with the same name as instance names. Set the disks.autoDelete property to true in the instance template.
    
    D. Delete the current instance template and replace it with a new instance template. Verify that the instance name and persistent disk name values are not the same in the template. Set the disks.autoDelete property to true in the instance template.

A - This is the only option (I know that it can be temporary) that will work without Rolling update according to

B - will not solve our problem without Rolling update

C - not correct, we cannot update existing template

D - not correct, we cannot delete existing template when it is in use (just checked in GCP) (We need rolling update)

https://cloud.google.com/compute/docs/troubleshooting/troubleshooting-migs

 
122. Your company is moving from an on-premises environment to Google Cloud. You have multiple development teams that use Cassandra environments as backend databases. They all need a development environment that is isolated from other Cassandra instances. You want to move to Google Cloud quickly and with minimal support effort. What should you do?

    A. 1. Build an instruction guide to install Cassandra on Google Cloud. 2. Make the instruction guide accessible to your developers.

    B. 1. Advise your developers to go to Cloud Marketplace. 2. Ask the developers to launch a Cassandra image for their development work.

    C. 1. Build a Cassandra Compute Engine instance and take a snapshot of it. 2. Use the snapshot to create instances for your developers.

    D. 1. Build a Cassandra Compute Engine instance and take a snapshot of it. 2. Upload the snapshot to Cloud Storage and make it accessible to your developers. 3. Build instructions to create a Compute Engine instance from the snapshot so that developers can do it themselves.

Correct Answer is (B):

https://medium.com/google-cloud/how-to-deploy-cassandra-and-connect-on-google-cloud-platform-with-a-few-clicks-11ee3d7001d1

 
123. You have a Compute Engine instance hosting a production application. You want to receive an email if the instance consumes more than 90% of its CPU resources for more than 15 minutes. You want to use Google services. What should you do?

    A. 1. Create a consumer Gmail account. 2. Write a script that monitors the CPU usage. 3. When the CPU usage exceeds the threshold, have that script send an email using the Gmail account and smtp.gmail.com on port 25 as SMTP server.

    B. 1. Create a Cloud Monitoring Workspace and associate your Google Cloud Platform (GCP) project with it. 2. Create a Cloud Monitoring Alerting Policy that uses the threshold as a trigger condition. 3. Configure your email address in the notification channel.

    C. 1. Create a Cloud Monitoring Workspace and associate your GCP project with it. 2. Write a script that monitors the CPU usage and sends it as a custom metric to Cloud Monitoring. 3. Create an uptime check for the instance in Cloud Monitoring.

    D. 1. In Cloud Logging, create a logs-based metric to extract the CPU usage by using this regular expression: CPU Usage: ([0-9] {1,3})% 2. In Cloud Monitoring, create an Alerting Policy based on this metric. 3. Configure your email address in the notification channel.

answer is B, but I would write it this way as stackdriver is deprecated and Operation Suite uses scopes now.
Create a Cloud Monitoring metric scope and associate your Google Cloud Platform (GCP) project with it.

Create a Cloud Monitoring Alerting Policy that uses the threshold as a trigger condition.

Configure your email address in the notification channel.

 
124. You have an application that uses Cloud Spanner as a backend database. The application has a very predictable traffic pattern. You want to automatically scale up or down the number of Spanner nodes depending on traffic. What should you do?

    A. Create a cron job that runs on a scheduled basis to review Cloud Monitoring metrics, and then resize the Spanner instance accordingly.

    B. Create a Cloud Monitoring alerting policy to send an alert to oncall SRE emails when Cloud Spanner CPU exceeds the threshold. SREs would scale resources up or down accordingly.

    C. Create a Cloud Monitoring alerting policy to send an alert to Google Cloud Support email when Cloud Spanner CPU exceeds your threshold. Google support would scale resources up or down accordingly.

    D. Create a Cloud Monitoring alerting policy to send an alert to webhook when Cloud Spanner CPU is over or under your threshold. Create a Cloud Function that listens to HTTP and resizes Spanner resources accordingly.

D
Excerpt from - https://cloud.google.com/spanner/docs/instances
Note: You can scale the number of nodes in your instance based on the Cloud Monitoring metrics on CPU or storage utilization in conjunction with Cloud Functions.

 
125. Your company publishes large files on an Apache web server that runs on a Compute Engine instance. The Apache web server is not the only application running in the project. You want to receive an email when the egress network costs for the server exceed 100 dollars for the current month as measured by Google Cloud. What should you do?

    A. Set up a budget alert on the project with an amount of 100 dollars, a threshold of 100%, and notification type of email.

    B. Set up a budget alert on the billing account with an amount of 100 dollars, a threshold of 100%, and notification type of email.

    C. Export the billing data to BigQuery. Create a Cloud Function that uses BigQuery to sum the egress network costs of the exported billing data for the Apache web server for the current month and sends an email if it is over 100 dollars. Schedule the Cloud Function using Cloud Scheduler to run hourly.

    D. Use the Cloud Logging Agent to export the Apache web server logs to Cloud Logging. Create a Cloud Function that uses BigQuery to parse the HTTP response log data in Cloud Logging for the current month and sends an email if the size of all HTTP responses, multiplied by current Google Cloud egress prices, totals over 100 dollars. Schedule the Cloud Function using Cloud Scheduler to run hourly.

C is the correct answer,
You export the bill to BigQuery and filter for the Egress cost for the particular application, and send an email if the cost is over 100 dollars, to send an email you need to use cloud function to monitor and trigger based on the conditions. it can only be the C because "it's not the only app running". 

A & B discarded as they are not resourced oriented. We need to charge for the apache server (so focusing on the VM where it is hosted) in order to charge this server.

D can't be as you are not charged in this case for that Response payload received.
 
126. You have designed a solution on Google Cloud that uses multiple Google Cloud products. Your company has asked you to estimate the costs of the solution. You need to provide estimates for the monthly total cost. What should you do?

    A. For each Google Cloud product in the solution, review the pricing details on the products pricing page. Use the pricing calculator to total the monthly costs for each Google Cloud product.

    B. For each Google Cloud product in the solution, review the pricing details on the products pricing page. Create a Google Sheet that summarizes the expected monthly costs for each product.

    C. Provision the solution on Google Cloud. Leave the solution provisioned for 1 week. Navigate to the Billing Report page in the Cloud Console. Multiply the 1 week cost to determine the monthly costs.

    D. Provision the solution on Google Cloud. Leave the solution provisioned for 1 week. Use Cloud Monitoring to determine the provisioned and used resource amounts. Multiply the 1 week cost to determine the monthly costs.

A is the correct answer, use the pricing calculator to estimate the pricing for a month and download the estimate to csv file, or you can share the URL of the pricing calculator or email the estimate to the respective people in the company. 

https://cloud.google.com/free/docs/estimate-costs-google-cloud-platform


127. You have an application that receives SSL-encrypted TCP traffic on port 443. Clients for this application are located all over the world. You want to minimize latency for the clients. Which load balancing option should you use?

    A. HTTPS Load Balancer

    B. Network Load Balancer

    C. SSL Proxy Load Balancer

    D. Internal TCP/UDP Load Balancer. Add a firewall rule allowing ingress traffic from 0.0.0.0/0 on the target instances.
C
SSL Proxy Load Balancing can be configured as a global load balancing service.
https://cloud.google.com/load-balancing/docs/ssl

Answer should be SSL proxy LB because it handles tcp traffic on port 443. where https LB not handles tcp traffic.

 
128. You have an application on a general-purpose Compute Engine instance that is experiencing excessive disk read throttling on its Zonal SSD Persistent Disk. The application primarily reads large files from disk. The disk size is currently 350 GB. You want to provide the maximum amount of throughput while minimizing costs. What should you do?

    A. Increase the size of the disk to 1 TB.

    B. Increase the allocated CPU to the instance.

    C. Migrate to use a Local SSD on the instance.

    D. Migrate to use a Regional SSD on the instance.

C is correct, local SSD has more IOPS

Here are calculations (taken from GCP when creating instance)
350 Gb SSD Persistent disk: 59.50$/month, read IOPS: 10 500 with n1-standard-1
1000 Gb SSD Persistent disk: 170.00$/month, read IOPS: 15 000 with n1-standard-1
375 Gb Local SSD (NVMe): 30.00$/month, read IOPS: 170 000 with n1-standard-1

The question does not stipulate that the local files must be persistent, and this is the only reason why you would choose "A" over "C".

Also, the question has an important key word: Minimising costs.

1TB of zonal persistent disk costs a huge amount more than 350GB of local disk.

 
129. Your Dataproc cluster runs in a single Virtual Private Cloud (VPC) network in a single subnet with range 172.16.20.128/25. There are no private IP addresses available in the VPC network. You want to add new VMs to communicate with your cluster using the minimum number of steps. What should you do?

    A. Modify the existing subnet range to 172.16.20.0/24.

    B. Create a new Secondary IP Range in the VPC and configure the VMs to use that range.

    C. Create a new VPC network for the VMs. Enable VPC Peering between the VMs' VPC network and the Dataproc cluster VPC network.

    D. Create a new VPC network for the VMs with a subnet of 172.32.0.0/16. Enable VPC network Peering between the Dataproc VPC network and the VMs VPC network. Configure a custom Route exchange.

Correct Answers is (A):

gcloud compute networks subnets expand-ip-range
NAME
gcloud compute networks subnets expand-ip-range - expand the IP range of a Compute Engine subnetwork

Could also be C, since you're using a completely different subnet range. 
 
130. You manage an App Engine Service that aggregates and visualizes data from BigQuery. The application is deployed with the default App Engine Service account. The data that needs to be visualized resides in a different project managed by another team. You do not have access to this project, but you want your application to be able to read data from the BigQuery dataset. What should you do?

    A. Ask the other team to grant your default App Engine Service account the role of BigQuery Job User.

    B. Ask the other team to grant your default App Engine Service account the role of BigQuery Data Viewer.

    C. In Cloud IAM of your project, ensure that the default App Engine service account has the role of BigQuery Data Viewer.

    D. In Cloud IAM of your project, grant a newly created service account from the other team the role of BigQuery Job User in your project.

B is the answer
according to Google's least privilege model and question states that service account needs to have permission to access another project's BigQuery dataset.

(roles/bigquery.dataViewer) - Lowest-level resources where you can grant this role to Table to view data only for specific dataset

(roles/bigquery.jobUser) - Provides permissions to run jobs, including queries, within the project.

if so it has been assigned with jobUser it has privilege to access all datasets in that project and it is access violation


    
131. You need to create a copy of a custom Compute Engine virtual machine (VM) to facilitate an expected increase in application traffic due to a business acquisition. What should you do?

    A. Create a Compute Engine snapshot of your base VM. Create your images from that snapshot.

    B. Create a Compute Engine snapshot of your base VM. Create your instances from that snapshot.

    C. Create a custom Compute Engine image from a snapshot. Create your images from that image.

    D. Create a custom Compute Engine image from a snapshot. Create your instances from that image.

Correct Answer is (D):

Preparing your instance for an image
You can create an image from a disk even while it is attached to a running VM instance. However, your image will be more reliable if you put the instance in a state that is easier for the image to capture. Use one of the following processes to prepare your boot disk for the image:

Stop the instance so that it can shut down and stop writing any data to the persistent disk.

If you can't stop your instance before you create the image, minimize the amount of writes to the disk and sync your file system.

Pause apps or operating system processes that write data to that persistent disk.
Run an app flush to disk if necessary. For example, MySQL has a FLUSH statement. Other apps might have similar processes.
Stop your apps from writing to your persistent disk.
Run sudo sync.
After you prepare the instance, create the image.

https://cloud.google.com/compute/docs/images/create-delete-deprecate-private-images#prepare_instance_for_image

It is not B because if you make a snapshot and replicate it ,the new machines will have the same SSID, imagine if machine are in a domain, all the clones machines are viewing from domain controlles are the same. It doesn't work.
 
132. You have deployed an application on a single Compute Engine instance. The application writes logs to disk. Users start reporting errors with the application. You want to diagnose the problem. What should you do?

    A. Navigate to Cloud Logging and view the application logs.

    B. Connect to the instance's serial console and read the application logs.

    C. Configure a Health Check on the instance and set a Low Healthy Threshold value.

    D. Install and configure the Cloud Logging Agent and view the logs from Cloud Logging. 

Answer: D

App logs can't be visible to Cloud Logging until we install Cloud Logging Agent on GCE

In its default configuration, the Logging agent streams logs from common third-party applications and system software to Logging; review the list of default logs. You can configure the agent to stream additional logs; go to Configuring the Logging agent for details on agent configuration and operation.

It is a best practice to run the Logging agent on all your VM instances. The agent runs under both Linux and Windows. To install the Logging agent, go to Installing the agent.

https://cloud.google.com/logging/docs/agent



133. An application generates daily reports in a Compute Engine virtual machine (VM). The VM is in the project corp-iot-insights. Your team operates only in the project corp-aggregate-reports and needs a copy of the daily exports in the bucket corp-aggregate-reports-storage. You want to configure access so that the daily reports from the VM are available in the bucket corp-aggregate-reports-storage and use as few steps as possible while following Google-recommended practices. What should you do?
    A. Move both projects under the same folder.

    B. Grant the VM Service Account the role Storage Object Creator on corp-aggregate-reports-storage.

    C. Create a Shared VPC network between both projects. Grant the VM Service Account the role Storage Object Creator on corp-iot-insights.

    D. Make corp-aggregate-reports-storage public and create a folder with a pseudo-randomized suffix name. Share the folder with the IoT team.

Correct Answer is (B):

Predefined roles
The following table describes Identity and Access Management (IAM) roles that are associated with Cloud Storage and lists the permissions that are contained in each role. Unless otherwise noted, these roles can be applied either to entire projects or specific buckets.

Storage Object Creator (roles/storage.objectCreator) Allows users to create objects. Does not give permission to view, delete, or overwrite objects.

https://cloud.google.com/storage/docs/access-control/iam-roles#standard-roles
 
134. You built an application on your development laptop that uses Google Cloud services. Your application uses Application Default Credentials for authentication and works fine on your development laptop. You want to migrate this application to a Compute Engine virtual machine (VM) and set up authentication using Google- recommended practices and minimal changes. What should you do?

    A. Assign appropriate access for Google services to the service account used by the Compute Engine VM.

    B. Create a service account with appropriate access for Google services, and configure the application to use this account.

    C. Store credentials for service accounts with appropriate access for Google services in a config file, and deploy this config file with your application.

    D. Store credentials for your user account with appropriate access for Google services in a config file, and deploy this config file with your application.

Correct Answer is (B):

Best practices
In general, Google recommends that each instance that needs to call a Google API should run as a service account with the minimum permissions necessary for that instance to do its job. In practice, this means you should configure service accounts for your instances with the following process:

Create a new service account rather than using the Compute Engine default service account.
Grant IAM roles to that service account for only the resources that it needs.
Configure the instance to run as that service account.
Grant the instance the https://www.googleapis.com/auth/cloud-platform scope to allow full access to all Google Cloud APIs, so that the IAM permissions of the instance are completely determined by the IAM roles of the service account.
Avoid granting more access than necessary and regularly check your service account permissions to make sure they are up-to-date.

https://cloud.google.com/compute/docs/access/create-enable-service-accounts-for-instances#best_practices

 
135. You need to create a Compute Engine instance in a new project that doesn't exist yet. What should you do?

    A. Using the Cloud SDK, create a new project, enable the Compute Engine API in that project, and then create the instance specifying your new project.

    B. Enable the Compute Engine API in the Cloud Console, use the Cloud SDK to create the instance, and then use the --project flag to specify a new project.

    C. Using the Cloud SDK, create the new instance, and use the --project flag to specify the new project. Answer yes when prompted by Cloud SDK to enable the Compute Engine API.

    D. Enable the Compute Engine API in the Cloud Console. Go to the Compute Engine section of the Console to create a new instance, and look for the Create In A New Project option in the creation form.

Correct Answer is (A):
Quickstart: Creating a New Instance Using the Command Line
Before you begin
    1. In the Cloud Console, on the project selector page, select or create a Cloud project.
    2. Make sure that billing is enabled for your Google Cloud project. Learn how to confirm billing is enabled for your project.

To use the gcloud command-line tool for this quickstart, you must first install and initialize the Cloud SDK:
    1. Download and install the Cloud SDK using the instructions given on Installing Google Cloud SDK.
    2. Initialize the SDK using the instructions given on Initializing Cloud SDK.
To use gcloud in Cloud Shell for this quickstart, first activate Cloud Shell using the instructions given on Starting Cloud Shell.

https://cloud.google.com/ai-platform/deep-learning-vm/docs/quickstart-cli#before-you-begin


136. Your company runs one batch process in an on-premises server that takes around 30 hours to complete. The task runs monthly, can be performed offline, and must be restarted if interrupted. You want to migrate this workload to the cloud while minimizing cost. What should you do?

    A. Migrate the workload to a Compute Engine Preemptible VM.

    B. Migrate the workload to a Google Kubernetes Engine cluster with Preemptible nodes.

    C. Migrate the workload to a Compute Engine VM. Start and stop the instance as needed.

    D. Create an Instance Template with Preemptible VMs On. Create a Managed Instance Group from the template and adjust Target CPU Utilization. Migrate the workload.

C is the correct answer,
Install the workload in a compute engine VM, start and stop the instance as needed, because as per the question the VM runs for 30 hours, process can be performed offline and should not be interrupted, if interrupted we need to restart the batch process again. Preemptible VMs are cheaper, but they will not be available beyond 24hrs, and if the process gets interrupted the preemptible VM will restart.
 
137. You are developing a new application and are looking for a Jenkins installation to build and deploy your source code. You want to automate the installation as quickly and easily as possible. What should you do?

    A. Deploy Jenkins through the Google Cloud Marketplace.

    B. Create a new Compute Engine instance. Run the Jenkins executable.

    C. Create a new Kubernetes Engine cluster. Create a deployment for the Jenkins image.

    D. Create an instance template with the Jenkins executable. Create a managed instance group with this template.

Correct Answer is (A):

Installing Jenkins
In this section, you use Cloud Marketplace to provision a Jenkins instance. You customize this instance to use the agent image you created in the previous section.

Go to the Cloud Marketplace solution for Jenkins.

Click Launch on Compute Engine.

Change the Machine Type field to 4 vCPUs 15 GB Memory, n1-standard-4.

Machine type selection for Jenkins deployment.

Click Deploy and wait for your Jenkins instance to finish being provisioned. When it is finished, you will see:

Jenkins has been deployed.

https://cloud.google.com/solutions/using-jenkins-for-distributed-builds-on-compute-engine#installing_jenkins

 
138. You have downloaded and installed the gcloud command line interface (CLI) and have authenticated with your Google Account. Most of your Compute Engine instances in your project run in the europe-west1-d zone. You want to avoid having to specify this zone with each CLI command when managing these instances. What should you do?

    A. Set the europe-west1-d zone as the default zone using the gcloud config subcommand.

    B. In the Settings page for Compute Engine under Default location, set the zone to europewest1-d.

    C. In the CLI installation directory, create a file called default.conf containing zone=europewest1d.

    D. Create a Metadata entry on the Compute Engine page with key compute/zone and value europewest1d.

Correct Answer is (A):

Change your default zone and region in the metadata server
Note: This only applies to the default configuration.
You can change the default zone and region in your metadata server by making a request to the metadata server. For example:

gcloud compute project-info add-metadata \
--metadata google-compute-default-region=europe-west1,google-compute-default-zone=europe-west1-b

The gcloud command-line tool only picks up on new default zone and region changes after you rerun the gcloud init command. After updating your default metadata, run gcloud init to reinitialize your default configuration.

https://cloud.google.com/compute/docs/gcloud-compute#change_your_default_zone_and_region_in_the_metadata_server

 
139. The core business of your company is to rent out construction equipment at large scale. All the equipment that is being rented out has been equipped with multiple sensors that send event information every few seconds. These signals can vary from engine status, distance traveled, fuel level, and more. Customers are billed based on the consumption monitored by these sensors. You expect high throughput `" up to thousands of events per hour per device `" and need to retrieve consistent data based on the time of the event. Storing and retrieving individual signals should be atomic. What should you do?

    A. Create a file in Cloud Storage per device and append new data to that file.

    B. Create a file in Cloud Filestore per device and append new data to that file.

    C. Ingest the data into Datastore. Store data in an entity group based on the device.

    D. Ingest the data into Cloud Bigtable. Create a row key based on the event timestamp. 

Answer: D

Keyword need to look for
    - "High Throughput",
    - "Consistent",
    - "Property based data insert/fetch like ngine status, distance traveled, fuel level, and more." which can be designed in column,
    - "Large Scale Customer Base + Each Customer has multiple sensor which send event in seconds" This will go for pera bytes situation,
    - Export data based on the time of the event.
    - Atomic

BigTable will fit all requirement.
DataStore is not fully Atomic
CloudStorage is not a option where we can export data based on time of event. We need another solution to do that
FireStore can be used with MobileSDK.

140. You are asked to set up application performance monitoring on Google Cloud projects A, B, and C as a single pane of glass. You want to monitor CPU, memory, and disk. What should you do?
    A. Enable API and then share charts from project A, B, and C.

    B. Enable API and then give the metrics.reader role to projects A, B, and C.

    C. Enable API and then use default dashboards to view all projects in sequence.

    D. Enable API, create a workspace under project A, and then add projects B and C.

D. workspaces is made for monitoring multiple projects.
