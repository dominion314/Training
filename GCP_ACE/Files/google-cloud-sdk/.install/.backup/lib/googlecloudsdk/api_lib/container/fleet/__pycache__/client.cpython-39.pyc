a
    ��$X  �                   @   s�   d Z ddlmZ ddlmZ ddlmZ ddlZddlmZ ddlmZ ddl	m
Z
 dd	lmZ dd
lmZ ddlmZ G dd� de�ZG dd� de�ZdS )z"Version-agnostic Fleet API client.�    )�absolute_import)�division)�unicode_literalsN)�encoding)�
list_pager)�util)�waiter)�base)�	resourcesc                   @   s�   e Zd ZdZejjfdd�Zdd� Zdd� Z	dd	� Z
d
d� Zddd�Zedd� �Zedd� �Zedd� �Zedd� �Zdd� Zdd� ZdS )�	HubClienta8  Client for the GKE Hub API with related helper methods.

  If not provided, the default client is for the GA (v1) track. This client
  is a thin wrapper around the base client, and does not handle any exceptions.

  Fields:
    client: The raw GKE Hub API client for the specified release track.
    messages: The matching messages module for the client.
    resourceless_waiter: A waiter.CloudOperationPollerNoResources for polling
      LROs that do not return a resource (like Deletes).
    feature_waiter: A waiter.CloudOperationPoller for polling Feature LROs.
  c                 C   sF   t �|�| _t �|�| _tj| jjd�| _tj	| jj
| jjd�| _d S �N)�operation_service)�result_servicer   )r   �GetClientInstance�client�GetMessagesModule�messagesr   �CloudOperationPollerNoResources�projects_locations_operations�resourceless_waiter�CloudOperationPoller�projects_locations_featuresZfeature_waiter��self�release_track� r   �4lib/googlecloudsdk/api_lib/container/fleet/client.py�__init__-   s    ��zHubClient.__init__c                 C   s    | j j|||d�}| jj�|�S )a�  Creates a Feature and returns the long-running operation message.

    Args:
      parent: The parent in the form /projects/*/locations/*.
      feature_id: The short ID for this Feature in the Hub API.
      feature: A Feature message specifying the Feature data to create.

    Returns:
      The long running operation reference. Use the feature_waiter and
      OperationRef to watch the operation and get the final status, typically
      using waiter.WaitFor to present a user-friendly spinner.
    )�featureZ	featureId�parent)r   Z,GkehubProjectsLocationsFeaturesCreateRequestr   r   �Create)r   r   Z
feature_idr   �reqr   r   r   �CreateFeature7   s    �zHubClient.CreateFeaturec                 C   s   | j j|d�}| jj�|�S )z�Gets a Feature from the Hub API.

    Args:
      name: The full resource name in the form
        /projects/*/locations/*/features/*.

    Returns:
      The Feature message.
    ��name)r   Z)GkehubProjectsLocationsFeaturesGetRequestr   r   �Get�r   r$   r!   r   r   r   �
GetFeatureK   s    
zHubClient.GetFeaturec                 C   s"   | j j|d�}| jj�|�}|jS )z�Lists Features from the Hub API.

    Args:
      parent: The parent in the form /projects/*/locations/*.

    Returns:
      A list of Features.
    )r   )r   Z*GkehubProjectsLocationsFeaturesListRequestr   r   �Listr
   )r   r   r!   �respr   r   r   �ListFeaturesX   s
    	�zHubClient.ListFeaturesc                 C   s&   | j j|d�|�|d�}| jj�|�S )a%  Creates a Feature and returns the long-running operation message.

    Args:
      name: The full resource name in the form
        /projects/*/locations/*/features/*.
      mask: A string list of the field paths to update.
      feature: A Feature message containing the Feature data to update using the
        mask.

    Returns:
      The long running operation reference. Use the feature_waiter and
      OperationRef to watch the operation and get the final status, typically
      using waiter.WaitFor to present a user-friendly spinner.
    �,)r$   �
updateMaskr   )r   Z+GkehubProjectsLocationsFeaturesPatchRequest�joinr   r   �Patch)r   r$   �maskr   r!   r   r   r   �UpdateFeatureg   s    �zHubClient.UpdateFeatureFc                 C   s   | j j||d�}| jj�|�S )a�  Deletes a Feature and returns the long-running operation message.

    Args:
      name: The full resource name in the form
        /projects/*/locations/*/features/*.
      force: Indicates the Feature should be force deleted.

    Returns:
      The long running operation. Use the feature_waiter and OperationRef to
      watch the operation and get the final status, typically using
      waiter.WaitFor to present a user-friendly spinner.
    )r$   �force)r   Z,GkehubProjectsLocationsFeaturesDeleteRequestr   r   �Delete)r   r$   r1   r!   r   r   r   �DeleteFeature}   s
    �zHubClient.DeleteFeaturec                 C   s   t jj| jdd�S )z6Parses a gkehub Operation reference from an operation.z$gkehub.projects.locations.operations)�
collection)r
   �REGISTRY�ParseRelativeNamer$   )�opr   r   r   �OperationRef�   s    �zHubClient.OperationRefc                 C   s,   | du s| j du ri S t�dd� | j D ��S )a�  Helper to convert proto map Values to normal dictionaries.

    encoding.MessageToPyValue recursively converts values to dicts, while this
    method leaves the map values as proto objects.

    Args:
      proto_map_value: The map field "Value". For example, the `Feature.labels`
        value (of type `Features.LabelsValue`). Can be None.

    Returns:
      An OrderedDict of the map's keys/values, in the original order.
    Nc                 s   s   | ]}|j |jfV  qd S )N)�key�value)�.0�pr   r   r   �	<genexpr>�   s   z%HubClient.ToPyDict.<locals>.<genexpr>)�additionalProperties�collections�OrderedDict)�proto_map_valuer   r   r   �ToPyDict�   s
    
�zHubClient.ToPyDictc                 C   s   t �| |du ri nt�|��S )a�  Helper to convert proto map Values to default dictionaries.

    encoding.MessageToPyValue recursively converts values to dicts, while this
    method leaves the map values as proto objects.

    Args:
      default_factory: Pass-through to collections.defaultdict.
      proto_map_value: The map field "Value". For example, the `Feature.labels`
        value (of type `Features.LabelsValue`). Can be None.

    Returns:
      An defaultdict of the map's keys/values.
    N)r?   �defaultdictr   rB   )�default_factoryrA   r   r   r   �ToPyDefaultDict�   s    �zHubClient.ToPyDefaultDictc                 C   s   t j|| dd�S )zCencoding.DictToAdditionalPropertyMessage wrapper to match ToPyDict.T)�
sort_items)r   �DictToAdditionalPropertyMessage)Zmap_value_clsr:   r   r   r   �
ToProtoMap�   s    �zHubClient.ToProtoMapc                 C   s   | � | jjj|�S )z?Convenience wrapper for ToProtoMap for Feature.membershipSpecs.)rH   r   �FeatureZMembershipSpecsValue�r   �spec_mapr   r   r   �ToMembershipSpecs�   s    zHubClient.ToMembershipSpecsc                 C   s   | � | jjj|�S )z:Convenience wrapper for ToProtoMap for Feature.scopeSpecs.)rH   r   rI   ZScopeSpecsValuerJ   r   r   r   �ToScopeSpecs�   s    zHubClient.ToScopeSpecsN)F)�__name__�
__module__�__qualname__�__doc__r	   �ReleaseTrack�GAr   r"   r'   r*   r0   r3   �staticmethodr8   rB   rE   rH   rL   rM   r   r   r   r   r      s"   





r   c                   @   s�   e Zd ZdZejjfdd�Zdd� Zdd� Z	dd	� Z
d
d� Zdd� Zdd� Zdd� Zdd� Zdd� Zdd� Zdd� Zdd� Zdd� Zdd� Zd d!� Zd"d#� Zd$d%� Zd.d'd(�Zd)d*� Zd+d,� Zd-S )/�FleetClienta3  Client for the Fleet API with related helper methods.

  If not provided, the default client is for the alpha (v1) track. This client
  is a thin wrapper around the base client, and does not handle any exceptions.

  Fields:
    client: The raw Fleet API client for the specified release track.
    messages: The matching messages module for the client.
    resourceless_waiter: A waiter.CloudOperationPollerNoResources for polling
      LROs that do not return a resource (like Deletes).
    fleet_waiter: A waiter.CloudOperationPoller for polling fleet LROs.
  c                 C   sF   t �|�| _t �|�| _tj| jjd�| _tj	| jj
| jjd�| _d S r   )r   r   r   r   r   r   r   r   r   r   �projects_locations_fleetsZfleet_waiterr   r   r   r   r   �   s    ��zFleetClient.__init__c                 C   s"   | j jt�|�d�}| jj�|�S )z�Gets a fleet resource from the Fleet API.

    Args:
      project: the project containing the fleet.

    Returns:
      A fleet resource

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r#   )r   Z'GkehubProjectsLocationsFleetsGetRequestr   �FleetResourceNamer   rV   r%   �r   �projectr!   r   r   r   �GetFleet�   s    �zFleetClient.GetFleetc                 C   s:   | j j|t�|�d�}| j j|t�|�d�}| jj�|�S )a  Creates a fleet resource from the Fleet API.

    Args:
      displayname: the fleet display name.
      project: the project containing the fleet.

    Returns:
      A fleet resource

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    )�displayNamer$   )�fleetr   )	r   �Fleetr   rW   Z*GkehubProjectsLocationsFleetsCreateRequest�FleetParentNamer   rV   r    )r   �displaynamerY   r\   r!   r   r   r   �CreateFleet�   s    
�
�zFleetClient.CreateFleetc                 C   s"   | j jt�|�d�}| jj�|�S )z�Deletes a fleet resource from the Fleet API.

    Args:
      project: the project containing the fleet.

    Returns:
      A fleet resource

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r#   )r   Z*GkehubProjectsLocationsFleetsDeleteRequestr   rW   r   rV   r2   rX   r   r   r   �DeleteFleet  s    �zFleetClient.DeleteFleetc                 C   s8   | j j|d�}d}| j j|t�|�|d�}| jj�|�S )a  Updates a fleet resource from the Fleet API.

    Args:
      displayname: the fleet display name.
      project: the project containing the fleet.

    Returns:
      A fleet resource

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    )r[   �display_name)r\   r$   r,   )r   r]   Z)GkehubProjectsLocationsFleetsPatchRequestr   rW   r   rV   r.   )r   r_   rY   r\   r/   r!   r   r   r   �UpdateFleet  s    �zFleetClient.UpdateFleetc                 C   s@   |rt �|�}n
t �|�}| jjd|d�}tj| jj|ddd�S )a'  Lists fleets in an organization.

    Args:
      project: the project to search.
      organization: the organization to search.

    Returns:
      A ListFleetResponse (list of fleets and next page token)

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    � ��	pageTokenr   ZfleetsN��field�batch_size_attribute)	r   ZFleetOrgParentNamer^   r   Z(GkehubProjectsLocationsFleetsListRequestr   �YieldFromListr   rV   )r   rY   �organizationr   r!   r   r   r   �
ListFleets*  s    
��zFleetClient.ListFleetsc                 C   s$   | j jt�||�d�}| jj�|�S )a  Gets a namespace resource from the GKEHub API.

    Args:
      project: the project containing the namespace.
      name: the namespace name.

    Returns:
      A namespace resource

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r#   )r   Z+GkehubProjectsLocationsNamespacesGetRequestr   �NamespaceResourceNamer   �projects_locations_namespacesr%   �r   rY   r$   r!   r   r   r   �GetNamespaceD  s    
�zFleetClient.GetNamespacec                 C   s<   | j jt�||�d�}| j j||t�|�d�}| jj�|�S )a  Creates a namespace resource from the GKEHub API.

    Args:
      name: the namespace name.
      project: the project containing the namespace.

    Returns:
      A namespace resource

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r#   )�	namespaceZnamespaceIdr   )	r   �	Namespacer   rm   Z.GkehubProjectsLocationsNamespacesCreateRequest�NamespaceParentNamer   rn   r    )r   r$   rY   rq   r!   r   r   r   �CreateNamespaceU  s    
��zFleetClient.CreateNamespacec                 C   s$   | j jt�||�d�}| jj�|�S )a  Deletes a namespace resource from the fleet.

    Args:
      project: the project containing the namespace.
      name: the name of the namespace.

    Returns:
      An operation

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r#   )r   Z.GkehubProjectsLocationsNamespacesDeleteRequestr   rm   r   rn   r2   ro   r   r   r   �DeleteNamespacej  s    
�zFleetClient.DeleteNamespacec                 C   sB   | j jt�||�d�}d}| j j|t�||�|d�}| jj�|�S )a  Updates a namespace resource in the fleet.

    Args:
      name: the namespace name.
      project: the project containing the namespace.

    Returns:
      An operation

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r#   rd   )rq   r$   r,   )r   rr   r   rm   Z-GkehubProjectsLocationsNamespacesPatchRequestr   rn   r.   )r   r$   rY   rq   r/   r!   r   r   r   �UpdateNamespace{  s    
�
�zFleetClient.UpdateNamespacec                 C   s,   | j jdt�|�d�}tj| jj|ddd�S )a  Lists namespaces in a project.

    Args:
      project: the project to list namespaces from.

    Returns:
      A ListNamespaceResponse (list of namespaces and next page token)

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    rd   re   �
namespacesNrg   )r   Z,GkehubProjectsLocationsNamespacesListRequestr   rs   r   rj   r   rn   rX   r   r   r   �ListNamespaces�  s    
��zFleetClient.ListNamespacesc                 C   s   | j j|d�}| jj�|�S )z�Gets an RBACRoleBinding resource from the GKEHub API.

    Args:
      name: the full rolebinding resource name.

    Returns:
      An RBACRoleBinding resource

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r#   )r   Z;GkehubProjectsLocationsNamespacesRbacrolebindingsGetRequestr   �.projects_locations_namespaces_rbacrolebindingsr%   r&   r   r   r   �GetRBACRoleBinding�  s    �zFleetClient.GetRBACRoleBindingc                 C   sj   | j j|| j j| j j�|�� �d�||d�}tjj|ddd�}| j j||�	� |�
� �� d�}| jj�|�S )a�  Creates an RBACRoleBinding resource from the GKEHub API.

    Args:
      name: the full rbacrolebinding resource name.
      role: the role.
      user: the user.
      group: the group.

    Returns:
      An RBACRoleBinding resource

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
      calliope_exceptions.RequiredArgumentException: if a required field is
        missing
    �ZpredefinedRole)r$   �role�user�groupz5gkehub.projects.locations.namespaces.rbacrolebindings�v1alpha��api_version)�rBACRoleBindingZrbacrolebindingIdr   )r   �RBACRoleBinding�Role�PredefinedRoleValueValuesEnum�upperr
   r5   r6   Z>GkehubProjectsLocationsNamespacesRbacrolebindingsCreateRequest�Name�Parent�RelativeNamer   ry   r    )r   r$   r|   r}   r~   �rolebinding�resourcer!   r   r   r   �CreateRBACRoleBinding�  s.    ����
��z!FleetClient.CreateRBACRoleBindingc                 C   s   | j j|d�}| jj�|�S )z�Deletes an RBACRoleBinding resource from the fleet.

    Args:
      name: the resource name of the rolebinding.

    Returns:
      An operation

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r#   )r   Z>GkehubProjectsLocationsNamespacesRbacrolebindingsDeleteRequestr   ry   r2   r&   r   r   r   �DeleteRBACRoleBinding�  s    ��z!FleetClient.DeleteRBACRoleBindingc              	   C   sN   | j j|||| j j| j j�|�� �d�d�}| j j||j|d�}| jj�	|�S )aH  Updates an RBACRoleBinding resource in the fleet.

    Args:
      name: the rolebinding name.
      user: the user.
      group: the group.
      role: the role.
      mask: a mask of the fields to update.

    Returns:
      An operation

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r{   )r$   r}   r~   r|   )r�   r$   r,   )
r   r�   r�   r�   r�   Z=GkehubProjectsLocationsNamespacesRbacrolebindingsPatchRequestr$   r   ry   r.   )r   r$   r}   r~   r|   r/   r�   r!   r   r   r   �UpdateRBACRoleBinding�  s     ����z!FleetClient.UpdateRBACRoleBindingc                 C   s.   | j jdt�||�d�}tj| jj|ddd�S )af  Lists rolebindings in a namespace.

    Args:
      project: the project containing the namespace to list rolebindings from.
      namespace: the namespace to list rolebindings from.

    Returns:
      A ListNamespaceResponse (list of rolebindings and next page token)

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    rd   re   ZrbacrolebindingsNrg   )r   Z<GkehubProjectsLocationsNamespacesRbacrolebindingsListRequestr   ZRBACRoleBindingParentNamer   rj   r   ry   )r   rY   rq   r!   r   r   r   �ListRBACRoleBindings  s    ��z FleetClient.ListRBACRoleBindingsc                 C   s   | j j|d�}| jj�|�S )a  Gets a Membership-Binding resource from the GKEHub API.

    Args:
      name: the full membership-binding resource name.

    Returns:
      A Membership-Binding resource

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r#   )r   Z4GkehubProjectsLocationsMembershipsBindingsGetRequestr   �'projects_locations_memberships_bindingsr%   r&   r   r   r   �GetMembershipBinding   s    �z FleetClient.GetMembershipBindingc                 C   sP   | j j|||d�}tjj|ddd�}| j j||�� |�� �� d�}| j	j
�|�S )a�  Creates a Membership-Binding resource from the GKEHub API.

    Args:
      name: the full binding resource name.
      scope: the Scope to be associated with Binding.
      fleet: the Fleet for which all related scopes are updated.

    Returns:
      A Membership-Binding resource

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
      calliope_exceptions.RequiredArgumentException: if a required field is
        missing
    �r$   �scoper\   z.gkehub.projects.locations.memberships.bindingsr   r�   )�membershipBindingZmembershipBindingIdr   )r   �MembershipBindingr
   r5   r6   Z7GkehubProjectsLocationsMembershipsBindingsCreateRequestr�   r�   r�   r   r�   r    )r   r$   r�   r\   �bindingr�   r!   r   r   r   �CreateMembershipBinding0  s$    ��
��z#FleetClient.CreateMembershipBinding�globalc                 C   s0   | j jdt�|||�d�}tj| jj|ddd�S )a�  Lists Bindings in a Membership.

    Args:
      project: the project containing the Membership to list Bindings from.
      membership: the Membership to list Bindings from.
      location: the Membrship location to list Bindings

    Returns:
      A ListMembershipBindingResponse (list of bindings and next page token)

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    rd   re   ZmembershipBindingsNrg   )r   Z5GkehubProjectsLocationsMembershipsBindingsListRequestr   ZMembershipBindingParentNamer   rj   r   r�   )r   rY   �
membership�locationr!   r   r   r   �ListMembershipBindingsP  s    ���z"FleetClient.ListMembershipBindingsc                 C   s4   | j j|||d�}| j j||j|d�}| jj�|�S )af  Updates a Membership-Binding resource.

    Args:
      name: the Binding name.
      scope: the Scope associated with binding.
      fleet: the Fleet for which all related scopes are updated.
      mask: a mask of the fields to update.

    Returns:
      An operation

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r�   )r�   r$   r,   )r   r�   Z6GkehubProjectsLocationsMembershipsBindingsPatchRequestr$   r   r�   r.   )r   r$   r�   r\   r/   r�   r!   r   r   r   �UpdateMembershipBindingg  s    ��z#FleetClient.UpdateMembershipBindingc                 C   s   | j j|d�}| jj�|�S )z�Deletes a Membership-Binding resource.

    Args:
      name: the resource name of the Binding.

    Returns:
      An operation

    Raises:
      apitools.base.py.HttpError: if the request returns an HTTP error
    r#   )r   Z7GkehubProjectsLocationsMembershipsBindingsDeleteRequestr   r�   r2   r&   r   r   r   �DeleteMembershipBinding�  s    ��z#FleetClient.DeleteMembershipBindingN)r�   )rN   rO   rP   rQ   r	   rR   �ALPHAr   rZ   r`   ra   rc   rl   rp   rt   ru   rv   rx   rz   r�   r�   r�   r�   r�   r�   r�   r�   r�   r   r   r   r   rU   �   s,   	$ 
rU   )rQ   �
__future__r   r   r   r?   �apitools.base.pyr   r   Z&googlecloudsdk.api_lib.container.fleetr   �googlecloudsdk.api_lib.utilr   �googlecloudsdk.callioper	   �googlecloudsdk.corer
   �objectr   rU   r   r   r   r   �<module>   s    -