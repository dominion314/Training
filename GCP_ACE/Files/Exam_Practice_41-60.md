41. You have 32 GB of data in a single file that you need to upload to a Nearline Storage bucket. The WAN connection you are using is rated at 1 Gbps, and you are the only one on the connection. You want to use as much of the rated 1 Gbps as possible to transfer the file rapidly. How should you upload the file?

    A. Use the GCP Console to transfer the file instead of gsutil.

    <b>B. Enable parallel composite uploads using gsutil on the file transfer.</b>

    C. Decrease the TCP window size on the machine initiating the transfer.

    D. Change the storage class of the bucket from Nearline to Multi-Regional.

<i>Correct answer is B as the bandwidth is good and its a single file, gsutil parallel composite uploads can be used to split the large file and upload in parallel.Refer GCP documentation - Transferring Data to GCP &amp
https://cloud.google.com/storage/docs/parallel-composite-uploads
</i>

42. You've deployed a microservice called myapp1 to a Google Kubernetes Engine cluster using the YAML file. You need to refactor this configuration so that the database password is not stored in plain text. You want to follow Google-recommended practices. What should you do?

    A. Store the database password inside the Docker image of the container, not in the YAML file.

    <b>B. Store the database password inside a Secret object. Modify the YAML file to populate the DB_PASSWORD environment variable from the Secret.</b>

    C. Store the database password inside a ConfigMap object. Modify the YAML file to populate the DB_PASSWORD environment variable from the ConfigMap.

    D. Store the database password in a file inside a Kubernetes persistent volume, and use a persistent volume claim to mount the volume to the container.

<i>it is good practice to use Secrets for confidential data (like API keys) and ConfigMaps for non-confidential data (like port numbers). B is correct. https://cloud.google.com/kubernetes-engine/docs/concepts/secret</i>


43. You are running an application on multiple virtual machines within a managed instance group and have autoscaling enabled. The autoscaling policy is configured so that additional instances are added to the group if the CPU utilization of instances goes above 80%. VMs are added until the instance group reaches its maximum limit of five VMs or until CPU utilization of instances lowers to 80%. The initial delay for HTTP health checks against the instances is set to 30 seconds. The virtual machine instances take around three minutes to become available for users. You observe that when the instance group autoscales, it adds more instances then necessary to support the levels of end-user traffic. You want to properly maintain instance group sizes when autoscaling. What should you do?

    A. Set the maximum number of instances to 1.

    B. Decrease the maximum number of instances to 3.

    C. Use a TCP health check instead of an HTTP health check.

    <b>D. Increase the initial delay of the HTTP health check to 200 seconds.</b>

<i>D is correct. The reason is that when you do health check, you want the VM to be working. Do the first check after initial setup time of 3 mins = 180 s < 200 s is reasonable.</i>


44. You need to select and configure compute resources for a set of batch processing jobs. These jobs take around 2 hours to complete and are run nightly. You want to minimize service costs. What should you do?

    A. Select Google Kubernetes Engine. Use a single-node cluster with a small instance type.

    B. Select Google Kubernetes Engine. Use a three-node cluster with micro instance types.

    <b>C. Select Compute Engine. Use preemptible VM instances of the appropriate standard machine type.</b>

    D. Select Compute Engine. Use VM instance types that support micro bursting.

<i> "For example, batch processing jobs can run on preemptible instances. If some of those instances stop during processing, the job slows but does not completely stop. Preemptible instances complete your batch processing tasks without placing additional workload on your existing instances and without requiring you to pay full price for additional normal instances." srouce: https://cloud.google.com/compute/docs/instances/preemptible</i>


45. You recently deployed a new version of an application to App Engine and then discovered a bug in the release. You need to immediately revert to the prior version of the application. What should you do?

    A. Run gcloud app restore.

    B. On the App Engine page of the GCP Console, select the application that needs to be reverted and click Revert.

    <b>C. On the App Engine Versions page of the GCP Console, route 100% of the traffic to the previous version.</b>

    D. Deploy the original version as a separate application. Then go to App Engine settings and split traffic between applications so that the original version serves 100% of the requests.

<i>correct is C NOT D.
Option A is wrong as gcloud app restore was used for backup and restore and has been deprecated.Option B is wrong as there is no application revert functionality available.Option D is wrong as App Engine maintains version and need not be redeployed.</i>


46. You deployed an App Engine application using gcloud app deploy, but it did not deploy to the intended project. You want to find out why this happened and where the application deployed. What should you do?

    A. Check the app.yaml file for your application and check project settings.

    B. Check the web-application.xml file for your application and check project settings.

    C. Go to Deployment Manager and review settings for deployment of applications.

    <b>D. Go to Cloud Shell and run gcloud config list to review the Google Cloud configuration used for deployment.</b>

<i>I would opt option D : as it would help to check the config details and Option A is not correct, as app.yaml would have only the runtime and script to run parameters and not the Project details

Option D - The config list will give the name of the project

    C:\GCP\appeng>gcloud config list
    [core]
    account = xxx@gmail.com
    disable_usage_reporting = False
    project = my-first-demo-xxxx
</i>


47. You want to configure 10 Compute Engine instances for availability when maintenance occurs. Your requirements state that these instances should attempt to automatically restart if they crash. Also, the instances should be highly available including during system maintenance. What should you do?

    <b>A. Create an instance template for the instances. Set the 'Automatic Restart' to on. Set the 'On-host maintenance' to Migrate VM instance. Add the instance template to an instance group.</b>

    B. Create an instance template for the instances. Set 'Automatic Restart' to off. Set 'On-host maintenance' to Terminate VM instances. Add the instance template to an instance group.

    C. Create an instance group for the instances. Set the 'Autohealing' health check to healthy (HTTP).

    D. Create an instance group for the instance. Verify that the 'Advanced creation options' setting for 'do not retry machine creation' is set to off.

<i>A
https://cloud.google.com/compute/docs/instances/setting-instance-scheduling-options

onHostMaintenance: Determines the behavior when a maintenance event occurs that might cause your instance to reboot.

[Default] MIGRATE, which causes Compute Engine to live migrate an instance when there is a maintenance event.
TERMINATE, which stops an instance instead of migrating it.
automaticRestart: Determines the behavior when an instance crashes or is stopped by the system.

[Default] true, so Compute Engine restarts an instance if the instance crashes or is stopped.
false, so Compute Engine does not restart an instance if the instance crashes or is stopped.</i>


48. You host a static website on Cloud Storage. Recently, you began to include links to PDF files on this site. Currently, when users click on the links to these PDF files, their browsers prompt them to save the file onto their local system. Instead, you want the clicked PDF files to be displayed within the browser window directly, without prompting the user to save the file locally. What should you do?

    A. Enable Cloud CDN on the website frontend.

    B. Enable 'Share publicly' on the PDF file objects.

    <b>C. Set Content-Type metadata to application/pdf on the PDF file objects.</b>

    D. Add a label to the storage bucket with a key of Content-Type and value of application/pdf.

<i>Option C:
To display PDF files directly within the browser window on a website hosted on Cloud Storage, you can follow these steps:

In the Google Cloud Console, navigate to the Cloud Storage section and select the "Buckets" page.

Select the bucket that contains the static website and the PDF files.

From the "Actions" menu, select "Edit bucket" and then go to the "Website" tab.

In the "Website Configuration" section, select the "Serve objects with this content type" option and enter "application/pdf" in the text field. This will cause PDF files to be served with the correct content type.

Save the changes to the bucket configuration.

After completing these steps, the PDF files on your website will be served with the correct content type and will be displayed directly within the browser window when clicked, without prompting the user to save the file locally.</i>


49. You have a virtual machine that is currently configured with 2 vCPUs and 4 GB of memory. It is running out of memory. You want to upgrade the virtual machine to have 8 GB of memory. What should you do?

    A. Rely on live migration to move the workload to a machine with more memory.

    B. Use gcloud to add metadata to the VM. Set the key to required-memory-size and the value to 8 GB.

    C. Stop the VM, change the machine type to n1-standard-8, and start the VM.

    <b>D. Stop the VM, increase the memory to 8 GB, and start the VM.</b>


50. You have production and test workloads that you want to deploy on Compute Engine. Production VMs need to be in a different subnet than the test VMs. All the VMs must be able to reach each other over Internal IP without creating additional routes. You need to set up VPC and the 2 subnets. Which configuration meets these requirements?

    <b>A. Create a single custom VPC with 2 subnets. Create each subnet in a different region and with a different CIDR range.</b>

    B. Create a single custom VPC with 2 subnets. Create each subnet in the same region and with the same CIDR range.

    C. Create 2 custom VPCs, each with a single subnet. Create each subnet in a different region and with a different CIDR range.

    D. Create 2 custom VPCs, each with a single subnet. Create each subnet in the same region and with the same CIDR range.
 
<i>Vote A
https://cloud.google.com/vpc/docs/using-vpc#subnet-rules
Primary and secondary ranges for subnets cannot overlap with any allocated range, any primary or secondary range of another subnet in the same network, or any IP ranges of subnets in peered networks.</i>   


51. You need to create an autoscaling managed instance group for an HTTPS web application. You want to make sure that unhealthy VMs are recreated. What should you do?

    <b>A. Create a health check on port 443 and use that when creating the Managed Instance Group.</b>

    B. Select Multi-Zone instead of Single-Zone when creating the Managed Instance Group.

    C. In the Instance Template, add the label 'health-check'.

    D. In the Instance Template, add a startup script that sends a heartbeat to the metadata server.

<i>A, MIGs support autohealing, load balancing, autoscaling, and auto-updating. no the Images templates, this is set up in the MIG

https://cloud.google.com/compute/docs/instance-groups/autohealing-instances-in-migs#setting_up_an_autohealing_policy</i>


52. Your company has a Google Cloud Platform project that uses BigQuery for data warehousing. Your data science team changes frequently and has few members. You need to allow members of this team to perform queries. You want to follow Google-recommended practices. What should you do?

    A. 1. Create an IAM entry for each data scientist's user account. 2. Assign the BigQuery jobUser role to the group.

    B. 1. Create an IAM entry for each data scientist's user account. 2. Assign the BigQuery dataViewer user role to the group.

    <b>C. 1. Create a dedicated Google group in Cloud Identity. 2. Add each data scientist's user account to the group. 3. Assign the BigQuery jobUser role to the group.</b>

    D. 1. Create a dedicated Google group in Cloud Identity. 2. Add each data scientist's user account to the group. 3. Assign the BigQuery dataViewer user role to the group.

<i>C, BigQuery Job User 
(roles/bigquery.jobUser)

Provides permissions to run jobs, including queries, within the project.

Lowest-level resources where you can grant this role:

Project</i>

53. Your company has a 3-tier solution running on Compute Engine. The configuration of the current infrastructure. Each tier has a service account that is associated with all instances within it. You need to enable communication on TCP port 8080 between tiers as follows:
* Instances in tier #1 must communicate with tier #2.
* Instances in tier #2 must communicate with tier #3.

    A. 1. Create an ingress firewall rule with the following settings:  Targets: all instances  Source filter: IP ranges (with the range set to 10.0.2.0/24)  Protocols: allow all 2. Create an ingress firewall rule with the following settings:  Targets: all instances  Source filter: IP ranges (with the range set to 10.0.1.0/24)  Protocols: allow all

   <b>B. 1. Create an ingress firewall rule with the following settings:  Targets: all instances with tier #2 service account  Source filter: all instances with tier #1 service account  Protocols: allow TCP:8080 2. Create an ingress firewall rule with the following settings:  Targets: all instances with tier #3 service account  Source filter: all instances with tier #2 service account  Protocols: allow TCP: 8080</b>

    C. 1. Create an ingress firewall rule with the following settings:  Targets: all instances with tier #2 service account  Source filter: all instances with tier #1 service account  Protocols: allow all 2. Create an ingress firewall rule with the following settings:  Targets: all instances with tier #3 service account  Source filter: all instances with tier #2 service account  Protocols: allow all

    D. 1. Create an egress firewall rule with the following settings:  Targets: all instances  Source filter: IP ranges (with the range set to 10.0.2.0/24)  Protocols: allow TCP: 8080 2. Create an egress firewall rule with the following settings:  Targets: all instances  Source filter: IP ranges (with the range set to 10.0.1.0/24)  Protocols: allow TCP: 8080

<i>Two answers mention port 8080, and two mention all ports. Obviously we just need port 8080, so we can immediately eliminate those two questions that want all ports open. That gives us a 50/50 chance of getting this question right. This answer is designed to waste time.

Of the remaining answers, one says "ingress" and the other "egress". We know that by default egress is permitted and ingress is not, so that makes "b" the only surviving choice.</i>


54. You are given a project with a single Virtual Private Cloud (VPC) and a single subnetwork in the us-central1 region. There is a Compute Engine instance hosting an application in this subnetwork. You need to deploy a new instance in the same project in the europe-west1 region. This new instance needs access to the application. You want to follow Google-recommended practices. What should you do?

    <b>A. 1. Create a subnetwork in the same VPC, in europe-west1. 2. Create the new instance in the new subnetwork and use the first instance's private address as the endpoint.</b>

    B. 1. Create a VPC and a subnetwork in europe-west1. 2. Expose the application with an internal load balancer. 3. Create the new instance in the new subnetwork and use the load balancer's address as the endpoint.

    C. 1. Create a subnetwork in the same VPC, in europe-west1. 2. Use Cloud VPN to connect the two subnetworks. 3. Create the new instance in the new subnetwork and use the first instance's private address as the endpoint.

    D. 1. Create a VPC and a subnetwork in europe-west1. 2. Peer the 2 VPCs. 3. Create the new instance in the new subnetwork and use the first instance's private address as the endpoint.

<i>A is correct. VPC allows you to spawn multiple subnets in different zones. Routing is handled automatically (because Routers are created automatically).

"use the first instance's private address as the endpoint" means that this new instance will be accessing the app via first intance's private IP (so there should be some routing rules created). Question says: "This new instance needs access to the application." ..</i>


55. Your projects incurred more costs than you expected last month. Your research reveals that a development GKE container emitted a huge number of logs, which resulted in higher costs. You want to disable the logs quickly using the minimum number of steps. What should you do?

    <b>A. 1. Go to the Logs ingestion window in Stackdriver Logging, and disable the log source for the GKE container resource.</b>

    B. 1. Go to the Logs ingestion window in Stackdriver Logging, and disable the log source for the GKE Cluster Operations resource.

    C. 1. Go to the GKE console, and delete existing clusters. 2. Recreate a new cluster. 3. Clear the option to enable legacy Stackdriver Logging.

    D. 1. Go to the GKE console, and delete existing clusters. 2. Recreate a new cluster. 3. Clear the option to enable legacy Stackdriver Monitoring.

<i>A is right.
https://cloud.google.com/logging/docs/api/v2/resource-list

GKE Containers have more log than GKE Cluster Operations:

.-GKE Containe:
cluster_name: An immutable name for the cluster the container is running in.
namespace_id: Immutable ID of the cluster namespace the container is running in.
instance_id: Immutable ID of the GCE instance the container is running in.
pod_id: Immutable ID of the pod the container is running in.
container_name: Immutable name of the container.
zone: The GCE zone in which the instance is running.

VS

.-GKE Cluster Operations
project_id: The identifier of the GCP project associated with this resource, such as "my-project".
cluster_name: The name of the GKE Cluster.
location: The location in which the GKE Cluster is running.</i>


56. You have a website hosted on App Engine standard environment. You want 1% of your users to see a new test version of the website. You want to minimize complexity. What should you do?

    A. Deploy the new version in the same application and use the --migrate option.

    <b>B. Deploy the new version in the same application and use the --splits option to give a weight of 99 to the current version and a weight of 1 to the new version.</b>

    C. Create a new App Engine application in the same project. Deploy the new version in that application. Use the App Engine library to proxy 1% of the requests to the new version.

    D. Create a new App Engine application in the same project. Deploy the new version in that application. Configure your network load balancer to send 1% of the traffic to that new application.

<i>more over, in app engine we cannot create "new application", we have to create a new Project to do that, an app engine projet has 1 application (which can have multiple versions and services)</i>


57. You have a web application deployed as a managed instance group. You have a new version of the application to gradually deploy. Your web application is currently receiving live web traffic. You want to ensure that the available capacity does not decrease during the deployment. What should you do?

    A. Perform a rolling-action start-update with maxSurge set to 0 and maxUnavailable set to 1.

    <b>B. Perform a rolling-action start-update with maxSurge set to 1 and maxUnavailable set to 0.</b>

    C. Create a new managed instance group with an updated instance template. Add the group to the backend service for the load balancer. When all instances in the new managed instance group are healthy, delete the old managed instance group.

    D. Create a new instance template with the new application version. Update the existing managed instance group with the new instance template. Delete the instances in the managed instance group to allow the managed instance group to recreate the instance using the new instance template.

<i>Correct option is B. We need to ensure the global capacity remains intact, for that reason we need to establish maxUnavailable to 0. On the other hand, we need to ensure new instances can be created. We do that by establishing the maxSurge to 1. Option C is more expensive and more difficult to set up and option D won't meet requirements since it won't keep global capacity intact.</i>


58. You are building an application that stores relational data from users. Users across the globe will use this application. Your CTO is concerned about the scaling requirements because the size of the user base is unknown. You need to implement a database solution that can scale with your user growth with minimum configuration changes. Which storage solution should you use?

    A. Cloud SQL

    <b>B. Cloud Spanner</b>

    C. Cloud Firestore

    D. Cloud Datastore

<i>B

Cloud SQL for small relational data, scaled manually

Cloud Spanner for relational data, scaled automatically

Cloud Firestore for app-based data, non-relational noSql

Cloud Datastore for non-relational data</i>


59. You are the organization and billing administrator for your company. The engineering team has the Project Creator role on the organization. You do not want the engineering team to be able to link projects to the billing account. Only the finance team should be able to link a project to a billing account, but they should not be able to make any other changes to projects. What should you do?

    <b>A. Assign the finance team only the Billing Account User role on the billing account.</b>

    B. Assign the engineering team only the Billing Account User role on the billing account.

    C. Assign the finance team the Billing Account User role on the billing account and the Project Billing Manager role on the organization.

    D. Assign the engineering team the Billing Account User role on the billing account and the Project Billing Manager role on the organization.

<i>Option A is correct, as we don't want the engineering team to link projects to billing account and want only the Finance team. Billing Account User role will help to link projects to the billing account. Option A makes the most sense since Billing Account User can link projects to the billing account and the question reinforces principle of least privilege. Source: https://cloud.google.com/billing/docs/how-to/billing-access</i>


60. You have an application running in Google Kubernetes Engine (GKE) with cluster autoscaling enabled. The application exposes a TCP endpoint. There are several replicas of this application. You have a Compute Engine instance in the same region, but in another Virtual Private Cloud (VPC), called gce-network, that has no overlapping IP ranges with the first VPC. This instance needs to connect to the application on GKE. You want to minimize effort. What should you do?

    A. 1. In GKE, create a Service of type LoadBalancer that uses the application's Pods as backend. 2. Set the service's externalTrafficPolicy to Cluster. 3. Configure the Compute Engine instance to use the address of the load balancer that has been created.

    B. 1. In GKE, create a Service of type NodePort that uses the application's Pods as backend. 2. Create a Compute Engine instance called proxy with 2 network interfaces, one in each VPC. 3. Use iptables on this instance to forward traffic from gce-network to the GKE nodes. 4. Configure the Compute Engine instance to use the address of proxy in gce-network as endpoint.

    <b>C. 1. In GKE, create a Service of type LoadBalancer that uses the application's Pods as backend. 2. Add an annotation to this service: cloud.google.com/load-balancer-type: Internal 3. Peer the two VPCs together. 4. Configure the Compute Engine instance to use the address of the load balancer that has been created.</b>

    D. 1. In GKE, create a Service of type LoadBalancer that uses the application's Pods as backend. 2. Add a Cloud Armor Security Policy to the load balancer that whitelists the internal IPs of the MIG's instances. 3. Configure the Compute Engine instance to use the address of the load balancer that has been created.

<i>https://cloud.google.com/load-balancing/docs/choosing-load-balancer#external-internalIts not A. External traffic Policy, the name is a bit misleading. What it is referring to is the load balancing of the pods. Do you perform no load balancing of the pods and keep it internal to the node (local) or do you perform pod load balancing outside/externally from the local node (i.e. across the cluster). Has nothing to do with the routing over the internet or how traffic gets routed to it. It has all to do with how to route traffic once it gets there.

It's not B cause nodeport is a one for one mapping, so this wouldn't scale , not to mention the 2 network interfaces witha proxy in two vpc statement...

It's not D cause there is no need for cloud armor security policy.

So that leaves C as the only viable source.

https://medium.com/pablo-perez/k8s-externaltrafficpolicy-local-or-cluster-40b259a19404</i>

