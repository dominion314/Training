# Controlling Costs and Budget Alerts

All resources found here:

https://github.com/antonitz/google-cloud-associate-cloud-engineer

https://cloud.google.com/products/calculator

Spend-based commitment

Discount for a commitment to spend a minimum amount for a service (hours) in a particular region
25% discount for 1 year – 52% discount on a 3 year
Available for Cloud SQL database instances and Google Cloud VMWare Engine
Applies only to CPU and memory usage

Resource-based commitment

Discount for commitment to spend a minimum amount for Compute Engine resource in a particular region.
Available for vCPU, Memory, GPU and Local SSD
57% discount for most resources
70% for memory-optimized machine types
For use across Projects

Sustained-use discounts

Automatic discounts of running Compute Engine resources a significant portion of the billing month
Applies to VCPUs and memory for most Compute Engine instance types
Includes VM’s created by GKE
Does not apply to App Engine flexible, Dataflow and E2 machine types

## Billing Export

Billing Export

Billing export enables granular billing data (such as usage, cost details, and pricing data) to be exported automatically to BigQuery for detailed analysis 
Not retroactive
Daily cost detail data
Pricing data

## Cloud APIs

Just remember to use any service in google, you have to enable the API.

## Cloud SDK and CLI

<b>There are four ways to access and interact with Google Cloud. The Cloud Console, the Cloud SDK in Cloud Shell, the APIs, and the Cloud Console Mobile App.</b>

gcloud, gsutil,bq,kubectl - most common commands
Gcloud Init - Authorizes access and performs other common Cloud SDK setup steps.
gcloud auth login - Authorize access for gcloud with Google user credentials
Gcloud config - Allows you to configure accounts and projects
gcloud components list/install/remove - Allow you to install, update and delete the components of the sdk
gcloud config set account 'ACCOUNT'
gcloud auth list

gcloud info - all configurations
gcloud config list 
    [compute]
    region = us-central1
    zone = us-central1-a
    [core]
    account = dominickhrndz314@gmail.com
    disable_usage_reporting = True
    project = big-genre-303821


gcloud beta interactive - after you've installed data component this will help with autocomplete and provide an interactive sh

## Script to Install Terraform in CloudShell Editor

create a .customize_environment file in cloudshell. Edit the file and paste the following. Test by checking the version.

    #!/bin/sh
    TERRAFORM_VERSION="0.13.0"
    curl https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip > terraform_${TERRAFORM_VERSION}_linux_amd64.zip
    unzip -o terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/local/bin

# IAM

https://github.com/antonitz/google-cloud-associate-cloud-engineer/tree/master/05-Identity-and-Access-Mangament/01_iam_policies

You manage access control by defining who (identity) has what access (role) for which resource. This also includes organizations, folders, and projects.

<b>The hierarchy is bottom level resources, projects, folders, and an organization node top level.</b> Policies are inherited downward.

Projects are the basis for enabling and using Google Cloud services, like managing APIs, enabling billing, adding and removing collaborators, and enabling other Google services. Each project is a separate entity under the organization node, and each resource belongs to exactly one project

Folders let you assign policies to resources at a level of granularity you choose. The resources in a folder inherit policies and permissions assigned to that folder. A folder can contain projects, other folders, or a combination of both.

The who part of an IAM policy can be a Google account, a Google group, a service account, or a Cloud Identity domain. The can-do what part of an IAM policy is defined by a role. 

There are three kinds of roles in IAM: Basic, Predefined, and Custom.

A <b>policy</b> is a collection of bindings, audit configuration, and metadata.

A <b>binding</b> specifies how access should be granted on resources. It binds one or more members with a single role and any context-specific conditions that change how and when the role is granted.

The metadata includes additional information about the policy, such as an etag and version to facilitate policy management.

The <b>AuditConfig</b>  field specifies the configuration data for how access attempts should be audited.

<b>Google Account</b> - Any email address that's associated with a Google Account, including gmail.com or other domains.

<b>Service Account</b> - An account for an application instead of an individual end user. Service accounts are named with an email address, but instead of passwords they use cryptographic keys to access resources. 

<b>Google Groups</b> - A named collection of Google Accounts and service accounts

<b>G Suite Domain</b> - Google Accounts that have been created in an organization's G Suite account

<b>Cloud Identity Domain</b> - Google Accounts in an organization that are not tied to any G Suite applications or features. With a tool called Cloud Identity, organizations can define policies and manage their users and groups using the Google Admin Console. Admins can log in and manage Google Cloud resources using the same user names and passwords they already used in existing Active Directory or LDAP systems. Using Cloud Identity also means that when someone leaves an organization, an administrator can use the Google Admin Console to disable their account and remove them from groups.

<b>AllAuthenticatedUsers</b> - A special identifier that represents all service accounts and all users on the internet who have authenticated with a Google Account

<b>AllUsers</b> - A special identifier that represents anyone who is on the internet, including authenticated and unauthenticated users

## Roles

This is a named collection of permissions that grant access to perform actions on Google Cloud resources.
You cannot grant a permission to the user directly
You grant a role to a user and all the permissions that the role contains.

## Permissions

Determines what operations are allowed on a resource
Correspond one-to-one with REST API methods
Not granted to users directly
E.g., compute.instances.list

## Primitive 

Roles historically available in the Google Cloud

    Owner
    Editor
    Viewer
    Avoid using these roles if possible

Predefined - Finer-grained access control than the primitive roles

Custom - Tailor permissions to the needs of your organization

## Conditions

Used to define and enforce conditional, attribute-based access control for Google Cloud resources.
Conditions allow you to choose granting resource access to identities only if configured conditions are met
When a condition exists, the access request is only granted if the condition expression = true

## Metadata

To help prevent a race condition when updating the policy, IAM supports concurrency control through the use of an etag field in the policy

## Audit Config

Determines which permission types are logged, and what identities, if any, are exempted from logging


## Policies and Conditions

<b>Policy Limitations</b>

1 policy per resource (including organizations, folders, projects)
1500 members or 250 Google groups per policy
Up to 7 minutes for policy changes to fully propagate across GCP
Limit of 100 conditional role bindings per policy
Conditions - Condition attributes are either based on resource or based on details about the request (timestamp, originating/destination IP address)

<b>Condition Limitations</b>

Limited to specific services
Primitive roles are unsupported
Members cannot be allUsers or allAuthenticatedUsers
Limit of 100 conditional role bindings per policy
20 role bindings for same role and same member

<b>AuditConfig Logs</b>

Specifies the audit configuration for a service. The configuration determines which permission types are logged, and what identities, if any, are exempted from logging. An AuditConfig must have one or more AuditLogConfigs.

## Example of adding a role to a user from CLI

This will add the storage admin role to user ourst1951@gmail.com 

    gcloud projects add-iam-policy-binding big-genre-303821 --member user:ourst1951@gmail.com --role roles/storage.admin

## Service Accounts

A service account is a special kind of account used by an application or a virtual machine (VM) instance, not a person.

An application uses the service account to authenticate between the application and GCP services so that the users aren't directly involved

A special type of Google account intended to represent a non-human user that needs to authenticate and be authorized to access data in Google APIs.

<b>Service Account types</b> 

<b>User-managed</b> - User created, You choose the name

<b>Default</b> - Using some GCP services create user-managed service accounts
Automatically granted the Editor role for the project

<b>Google-managed</b> - Managed by Google, and they are used by Google services. Some are visible, some hidden. Name ends with "Service Agent" or "Service Account”

<b>Service Account Keys</b> 

Key Management – None, All handled by Google
Key Management - Key storage, Key distribution, Key revocation, Key rotation, Protecting the keys from unauthorized users, Key recovery

<b>Access scopes</b> 

Service Account scopes are the legacy method of specifying permissions for your instance
And they are used in substitution of IAM roles
These are used specifically for default or automatically created service accounts
Based on enabled API's

## Check Bucket access from Service Account and add permissions to write file

    gsutils ls gs://ace-inc-dominick # shows the files in the bucket we created from the service account 
    gcloud iam service-accounts create write-buckets --display-name='write-buckets' # create account
    gcloud projects add-iam-policy-binding big-genre-303821 --member 'serviceAccount:write-buckets@big-genre-303821.iam.gserviceaccount.com' --role 'roles/storage.objectViewer' # Assign role binding to new SA:

        bindings:
        - members:
        - serviceAccount:service-11613812069@gcp-sa-bigquerydatatransfer.iam.gserviceaccount.com
        role: roles/bigquerydatatransfer.serviceAgent
        - members:
        - serviceAccount:service-11613812069@compute-system.iam.gserviceaccount.com
        role: roles/compute.serviceAgent
        - members:
        - serviceAccount:service-11613812069@container-engine-robot.iam.gserviceaccount.com
        role: roles/container.serviceAgent
        - members:
        - serviceAccount:service-11613812069@containerregistry.iam.gserviceaccount.com
        role: roles/containerregistry.ServiceAgent
        - members:
        - serviceAccount:11613812069-compute@developer.gserviceaccount.com
        - serviceAccount:11613812069@cloudservices.gserviceaccount.com
        role: roles/editor
        - members:
        - user:DominickHrndz314@gmail.com
        role: roles/owner
        - members:
        - serviceAccount:service-11613812069@gcp-sa-pubsub.iam.gserviceaccount.com
        role: roles/pubsub.serviceAgent
        - members:
        - serviceAccount:write-to-buckets@big-genre-303821.iam.gserviceaccount.com
        role: roles/storage.admin
        - members:
        - serviceAccount:write-to-buckets@big-genre-303821.iam.gserviceaccount.com
        role: roles/storage.objectAdmin
        - members:
        - serviceAccount:write-to-buckets@big-genre-303821.iam.gserviceaccount.com
        role: roles/storage.objectCreator
        - members:
        - serviceAccount:write-buckets@big-genre-303821.iam.gserviceaccount.com
        role: roles/storage.objectViewer
        etag: BwXvaNV4juw=
        version: 1

## Cloud Identity

Cloud Identity is an Identity as a Service (IDaaS) solution that centrally manages users and groups. This would be the sole system for authentication and that provides a single sign-on experience for all employees of an organization to be used for all your internal and external applications.

<b>Device management</b> - lets people in any organization access their work accounts from mobile devices while keeping the organization's data more secure. You can keep coroporate data safe by working even from a personal device. This will also allow you to wipe unused devices. 

<b>Security</b> - Helps by applying security best practices along with being able to deploy 2SV for the whole company along with enforcement controls and can also manage passwords to make sure they are meeting the enforced password requirements automatically. Google Auth, MFA. 

<b>Single Sign-on</b> - With single sign-on (SSO), users can access many applications without having to enter their username and password for each application

<b>Reporting</b> - This covers audit logs for logins, groups, devices and even tokens. You are even able to export these logs to BigQuery for analysis. You can then create reports from these logs that cover security, applications and activity.

<b>Directory Management</b> - Provides profile information for users in your organization, email and group addresses, and shared external contacts in the Directory. Using Google Cloud Directory Sync (GCDS), you can synchronize the data in your Google Account with your Microsoft Active Directory or LDAP server. GCDS doesn't migrate any content (such as email messages, calendar events, or files) to your Google Account. You use GCDS to synchronize all your users, groups, and shared contacts to match the information in your LDAP server.

<b>Google Cloud Directory Sync</b> is a free Google-provided tool that implements the synchronization process and can be run either on Google Cloud or in your on-premises environment. Synchronization is one-way so that Active Directory remains the source of truth. This can automatically sync AD forests from your on prem env into GCP using GCDS via Cloud ID. This will allow you to keep your AD as the single source of truth. 

# Networking Services

VPCs manage the virtualized network wtihin GCP. It is a global resource and encapsulated within a project. They do not have IP ranges but are associateed with subnets. Firewalls rules control ingress and egress. 

VPCs only support IPv4 within the network, but you can create IPv6 for global load balancers. VPCs contain a default network and firewall rule for ssh,rdp, and icmp traffic. 

Automode and Custom mode networks define the size of CIDR blocks and whether they are automatically created or manually. Custom are better suited for production.

Communication within the VPC is allowed by default, comms between VPCs is denied. 

The default VPC is automatically created with a subnet in every region.

## Increase Subnet Range for US-West-1 from /20 to /16

    gcloud compute networks subnets expand-ip-range default --region=us-west1 --prefix-length=16

    The IP range of subnetwork [default] will be expanded from 10.138.0.0/20 to 10.138.0.0/16. This operation may take several minutes to complete and cannot be undone.

    Do you want to continue (Y/n)?  y

    Updated [https://www.googleapis.com/compute/v1/projects/big-genre-303821/regions/us-west1/subnetworks/default].

    gcloud compute networks subnets describe default --region=us-west1 # verify
        creationTimestamp: '2022-08-23T08:03:05.217-07:00'
        fingerprint: CHh7N5WTo2g=
        gatewayAddress: 10.138.0.1
        id: '4688153799735988934'
        ipCidrRange: 10.138.0.0/16
        kind: compute#subnetwork
        name: default
        network: https://www.googleapis.com/compute/v1/projects/big-genre-303821/global/networks/default
        privateIpGoogleAccess: false
        privateIpv6GoogleAccess: DISABLE_GOOGLE_ACCESS
        purpose: PRIVATE
        region: https://www.googleapis.com/compute/v1/projects/big-genre-303821/regions/us-west1
        selfLink: https://www.googleapis.com/compute/v1/projects/big-genre-303821/regions/us-west1/subnetworks/default
        stackType: IPV4_ONLY

The name of a region or subnet cannot be changed after created and they must not overlap with other subnets in the same network. Once the subnet has been expanded, it cannot be undone. Going from /20 to /16 irreversable. 

There are 4 reserve IP addresses, the first 2 and last 2. These are reservered for the network, default gateway, google cloud, and broadcast. 

## Routes

When a subnet is created a corresponding subnet route is creted for primary and secondary IP ranges. You cannot delete a subnet route unless you delete the subnet. 

Static routes use next hop and are created manually. Theyre created automaticcally when creating cloud VPN tunnels. A load balancer IP address can be the next hop in a static route. You will set a priority and destination IP range for your static route when creating. 

Dynamic routes are managed by cloud routes. They exchange routes between VPC and on premise networks. They also work for VPNs.

Special Return Routes are defined outside of your VPC. You can control traffic to these using firewall rules and cloud DNS. 

Private Google Access allows VMs to reach external IPs or APIs

Private Service Access allows external 3rd parties to reach internal VPC resources.

## IP Addressing

Internal or Private IPs has Auto and Custome subnet options. Those IPs can be ephemeral or static. Alias IPs can be used for GKE containers. 

External or Public IPs can be ephermeral or static.

## How to assign IP

Go to the console and create you instance with a static reservation in the network settings

    gcloud compute addresses list
        NAME        ADDRESS/RANGE  TYPE      PURPOSE       NETWORK  REGION       SUBNET   STATUS
        static-res  10.128.0.3     INTERNAL  GCE_ENDPOINT           us-central1  default  IN_USE

Delete the instance from the console and you should see the IP as reserved. 

    gcloud compute addresses list
        NAME        ADDRESS/RANGE  TYPE      PURPOSE       NETWORK  REGION       SUBNET   STATUS
        static-res  10.128.0.3     INTERNAL  GCE_ENDPOINT           us-central1  default  RESERVED

You should then be able to create another instance with an ephemeral IP then edit the instance to choose static in the network settings. 

## Firewall Rules

Firewall rules apply to VPC that allow or deny conenctions to VMs ingress ro egress. Every VPC works a s distributed firewall. 169.254.169.254 is googles default metadata API server that always has access. DHCP, DNS, NTP, Instance Metadata is allowed on egress. 

Firewall rules only support IPv4 addresses or subnets. Each action is allow or deny, not both. You cannot share firewall rules between VPC networks. Firewall rules are stateful so return traffic must match the 5 tuple in the connection. If a connection is allowed, all response traffic is allowed.

Firewalls can also match target tags for subnets so you dont need to use IPs. You can specify ports and protocols. 

## Firewall Exercise

First create the VPC and then your subnets for private and public. 

    gcloud compute networks create custom --project=PROJECT_NAME --description=custom\ vpc\ network --subnet-mode=custom --bgp-routing-mode=regional

    gcloud compute networks subnets create public --project=bowtieinc --range=10.0.0.0/24 --network=custom --region=us-east1
    gcloud compute networks subnets create private --project=bowtieinc --range=10.0.5.0/24 --network=custom --region=us-east4

Upload images to a bucket.

Create an instance within the public subnet with the appropriate access scope for the instance and bucket

    gcloud compute instances create public-instance --project=firewall-exercise --zone=us-east1-b --machine-type=e2-micro --network-interface=network-tier=PREMIUM,subnet=public-sub --maintenance-policy=MIGRATE --provisioning-model=STANDARD --service-account=340043529601-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/compute,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/trace.append,https://www.googleapis.com/auth/devstorage.read_write --tags=public --create-disk=auto-delete=yes,boot=yes,device-name=public-instance,image=projects/debian-cloud/global/images/debian-11-bullseye-v20221206,mode=rw,size=10,type=projects/firewall-exercise/zones/us-central1-a/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --labels=env=public --reservation-affinity=any


Create a private instance 

    gcloud compute instances create private-instance --project=firewall-exercise --zone=us-east4-c --machine-type=e2-micro --network-interface=network-tier=PREMIUM,subnet=private-sib --maintenance-policy=MIGRATE --provisioning-model=STANDARD --service-account=340043529601-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/compute,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/trace.append,https://www.googleapis.com/auth/devstorage.read_write --tags=private --create-disk=auto-delete=yes,boot=yes,device-name=private-instance,image=projects/debian-cloud/global/images/debian-11-bullseye-v20221206,mode=rw,size=10,type=projects/firewall-exercise/zones/us-central1-a/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --labels=env=private --reservation-affinity=any

Youve created at VPC, storage bucket, and private/public instance this far. 

Now we will create firewall rules for giving public instance access to internet

    gcloud compute --project=firewall-exercise firewall-rules create public-access --direction=INGRESS --priority=1000 --network=firewall-exercise --action=ALLOW --rules=tcp:22,icmp --source-ranges=0.0.0.0/0 --target-tags=public

Now create the private rule that allows access to 22 on 10.0.0.0/24

SSH into the public instance

Test access to bucket 

    gsutil ls gs://firewall-test-dominick1
        gs://firewall-test-dominick1/brown-striped-bowtie.jpg
        gs://firewall-test-dominick1/orange-plaid-bowtie.jpg
        gs://firewall-test-dominick1/orange-striped-bowtie.jpeg

Ping the private instance

    ping 10.0.5.2
        PING 10.0.5.2 (10.0.5.2) 56(84) bytes of data.
        64 bytes from 10.0.5.2: icmp_seq=1 ttl=64 time=14.4 ms
        64 bytes from 10.0.5.2: icmp_seq=2 ttl=64 time=11.9 ms
        64 bytes from 10.0.5.2: icmp_seq=3 ttl=64 time=12.0 ms

SSH from the public instance to the private

    gcloud compute ssh --project firewall-exercise --zone us-east4-c private-instance --internal-ip
        WARNING: The private SSH key file for gcloud does not exist.
        WARNING: The public SSH key file for gcloud does not exist.
        WARNING: You do not have an SSH key for gcloud.
        WARNING: SSH keygen will be executed to generate a key.
        Generating public/private rsa key pair.
        Enter passphrase (empty for no passphrase): 
        Enter same passphrase again: 
        Your identification has been saved in /home/dominickhrndz314/.ssh/google_compute_engine
        Your public key has been saved in /home/dominickhrndz314/.ssh/google_compute_engine.pub
        The key fingerprint is:
        SHA256:V+kznPvvMaBUMJZBM9lxXekyKbLYirsX70NvjS6ydv4 dominickhrndz314@public-instance
        The key's randomart image is:
        +---[RSA 3072]----+
        |          .O=...=|
        |          .o=o...|
        |            o.o  |
        |         . =.= . |
        |        S +.B.o  |
        |       o =. .+.  |
        |      . = ..+  o |
        |     . = = + o  o|
        |     o=.*o*E  .oo|
        +----[SHA256]-----+
        Updating project ssh metadata...⠼Updated [https://www.googleapis.com/compute/v1/projects/firewall-exercise].  
        Updating project ssh metadata...done.                                                                         
        Waiting for SSH key to propagate.
        Warning: Permanently added 'compute.7187590363865814846' (ECDSA) to the list of known hosts.
        Linux private-instance 5.10.0-19-cloud-amd64 #1 SMP Debian 5.10.149-2 (2022-10-21) x86_64

        The programs included with the Debian GNU/Linux system are free software;
        the exact distribution terms for each program are described in the
        individual files in /usr/share/doc/*/copyright.

        Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
        permitted by applicable law.
        
    dominickhrndz314@private-instance:~$ 

Ping the public instance from the private

    ping 10.0.0.2
        PING 10.0.0.2 (10.0.0.2) 56(84) bytes of data.
        64 bytes from 10.0.0.2: icmp_seq=1 ttl=64 time=15.0 ms
        64 bytes from 10.0.0.2: icmp_seq=2 ttl=64 time=11.5 ms
        64 bytes from 10.0.0.2: icmp_seq=3 ttl=64 time=11.6 ms
        ^C
        --- 10.0.0.2 ping statistics ---
        3 packets transmitted, 3 received, 0% packet loss, time 2003ms
        rtt min/avg/max/mdev = 11.520/12.735/15.040/1.630 ms

List the bucket objects from the private instance. This will fail becaus eyou need to enable Private Google Access from the VPC subnet for private.

    dominickhrndz314@private-instance:~$ gsutil ls gs://firewall-test-dominick1

Go to VPC, sleect private sub, enable PGA. Test again. This will allow you to access public API's

    gsutil ls gs://firewall-test-dominick1
        gs://firewall-test-dominick1/brown-striped-bowtie.jpg
        gs://firewall-test-dominick1/orange-plaid-bowtie.jpg
        gs://firewall-test-dominick1/orange-striped-bowtie.jpeg

## VPC Network Peering

https://github.com/antonitz/google-cloud-associate-cloud-engineer/tree/master/06-Networking-Services/03_vpc_network_peering

VPC peering provides private connectivity across two networks and can peer across the same or different projects. It reduces latency, increaases networks security, and reduces costs. 

You must create ingress firewall rules. Cidr ranges cannot overlap. Transitive peering is NOT supported, and internal DNS is not available. 

Create new VPCs and subnets

    gcloud compute networks create vpc-peer-a --project=PROJECT_NAME --description=vpc-peer-a --subnet-mode=custom --bgp-routing-mode=regional

    gcloud compute networks subnets create subnet-a --project=PROJECT_NAME --range=10.0.0.0/20 --network=custom --region=us-east1

    gcloud compute networks create vpc-peer-b --project=PROJECT_NAME --description=vpc-peer-b --subnet-mode=custom --bgp-routing-mode=regional

    gcloud compute networks subnets create subnet-b --project=PROJECT_NAME --range=10.4.0.0/20 --network=custom --region=us-east4

Create the firewall rules

    gcloud compute --project=PROJECT_NAME firewall-rules create vpc-peer-a --direction=INGRESS --priority=1000 --network=custom --action=ALLOW --rules=tcp:22,icmp --source-ranges=0.0.0.0/0

    gcloud compute --project=PROJECT_NAME firewall-rules create vpc-peer-b --direction=INGRESS --priority=1000 --network=custom --action=ALLOW --rules=tcp:22,icmp --source-ranges=0.0.0.0/0


Create the instances

    gcloud beta compute --project=PROJECT_NAME instances create vpc-peer-a --zone=us-east1-b --machine-type=e2-micro --subnet=subnet-a --no-address --maintenance-policy=MIGRATE --service-account=852385812993-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --image=debian-10-buster-v20200910 --image-project=debian-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=instance-1 --no-shielded-secure-boot --no-shielded-vtpm --no-shielded-integrity-monitoring --reservation-affinity=any

    gcloud beta compute --project=PROJECT_NAME instances create vpc-peer-b --zone=us-east4-c --machine-type=e2-micro --subnet=subnet-b --no-address --maintenance-policy=MIGRATE --service-account=852385812993-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --image=debian-10-buster-v20200910 --image-project=debian-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=instance-1 --no-shielded-secure-boot --no-shielded-vtpm --no-shielded-integrity-monitoring --reservation-affinity=any

Create the VPC peers in both projects. You will need the project name and VPC ID to configure from both ends. 

    gcloud compute networks peerings create peering-ab --network=vpc-peer-a --peer-project PROJECT_ID --peer-network vpc-peer-b

    gcloud compute networks peerings create peering-ba --network=vpc-peer-b --peer-project PROJECT_ID --peer-network vpc-peer-a

You should have a green check mark to indicate peering is successful. Now SSH into each instances and test connectivity from both ends. 

    dominickhrndz314@vpc-peer-b:~$ ping 10.0.0.2
        PING 10.0.0.2 (10.0.0.2) 56(84) bytes of data.
        64 bytes from 10.0.0.2: icmp_seq=1 ttl=64 time=13.8 ms
        64 bytes from 10.0.0.2: icmp_seq=2 ttl=64 time=11.6 ms
        64 bytes from 10.0.0.2: icmp_seq=3 ttl=64 time=11.7 ms
    
    dominickhrndz314@vpc-peer-a:~$ ping 10.4.0.2
        PING 10.4.0.2 (10.4.0.2) 56(84) bytes of data.
        64 bytes from 10.4.0.2: icmp_seq=1 ttl=64 time=13.3 ms
        64 bytes from 10.4.0.2: icmp_seq=2 ttl=64 time=11.4 ms
        64 bytes from 10.4.0.2: icmp_seq=3 ttl=64 time=11.5 ms


## Shared VPC
Shared VPC allows an org to connect resources from multiple projects to a common VPC network so they can communicate with each other securely using internal IPs from that network. 

Shared VPCs can be either a host project or service project but not both. A Shared VPC Admin has project level permissions and subnet level permissions. 

You can have a host project attached to a service project which has a VM that can access an internal VM within the Shared VPC Network. Simultaneously you can have Service Project B which can access a spearate subnet in the same VPC under a different region and subnet.

Hyrbid env would be like an onprem network using VPNs to reach a Shared VPC network. This may be useful for database or active directory services. 

Two tier web services use a load balancer to go from a service project into a shared VPC that has different subnets. 

## VPC Flow Logs

FLow logs record netowrk flows between VM instances. When enabled, they are enable for all VMs on a subnet by subnet basis. These logs can be sent to Cloud Logging or Stack Driver. For long term storage they can be sent to a cloud storage bucket. Stack Driver will hold logs for 30 days.

Flow logs are great for realtime visibility into the network. You can analuze and optimize network traffic expenses. They can be used for realtime security analysis using PubSub, Splunk, or Rapid7. 

Their record formate has core fields and metadata fields. Core could be base or IP details like the 5 tuple. You can create scripts in the query build via Log Viewer, that will allow you filter the information you need specifically. 

## DNS 

Domain Names System is matches IPs to hostnames. It is a gloabal decentralized DB. DNS records link IPs to these websites. IANA manages root domains and top level domains are done by companies such as Verasign. .Com, .Org and so on are managed by IANA. Second level domains are authoritative name servers managed by companies such as google, apple, etc. 

DNS Zone files contain A record for ipv4 and AAAA for ipv6. CNAME records map one domain name to another, this way if the IP address every changes, you simply point it to the hostname. TXT records are used to verify domain ownership. MX records are mail exchange record which directs mail to mail servers. MX records need a domain name and priority. SOA records are created for managed zones and store important info like the email of the admin and records for updates. It contains core info for the zone. 

Google also has a global system of edge caches. Edge caching refers to the use of caching servers to store content closer to end users.

Create Cloud DNS

    gcloud dns --project=big-genre-303821 managed-zones create demo-dns --description="" --dns-name="demo-dns." --visibility="private" --networks="default"

NS and SOA records are automatically created. Describe the zone

    gcloud dns --project=big-genre-303821 managed-zones describe demo-dns
        cloudLoggingConfig:
        kind: dns#managedZoneCloudLoggingConfig
        creationTime: '2022-12-10T16:25:37.856Z'
        description: ''
        dnsName: demo-dns.
        id: '1350101290548991786'
        kind: dns#managedZone
        name: demo-dns
        nameServers:
        - ns-gcp-private.googledomains.com.
        privateVisibilityConfig:
        kind: dns#managedZonePrivateVisibilityConfig
        networks:
        - kind: dns#managedZonePrivateVisibilityConfigNetwork
            networkUrl: https://www.googleapis.com/compute/v1/projects/big-genre-303821/global/networks/vpc-peer-b
        - kind: dns#managedZonePrivateVisibilityConfigNetwork
            networkUrl: https://www.googleapis.com/compute/v1/projects/big-genre-303821/global/networks/default
        visibility: private

Delete your Cloud DNS.

# Compute Engine

 cloud.google.com/compute/docs/machine-types

https://github.com/antonitz/google-cloud-associate-cloud-engineer/tree/master/07-Compute-Engine

Virtal Machines are GCPs IaaS. Instances are available in different sizes and types, billed per second, laucned in a VPC, available in a zone, and can be either multi tenant host or sole tenant node.

<b>Machines Types</b> are for general compute and memory using intel or amd. the CPU is single hardware hyper thread.

<b>Operating System Images</b> Uses public images for linux, windows, or mac. You can also use customer or private images from snapshots or boot disks. Marketplace images offer software packages that include the OS and software packes.

<b>Storage</b>  This is where performance and cost corss Standard HDD or spinning hard drives are the cheapest but slowest. Balanced SSD or Solid State Drives are faster but more expensive, and SSDs are the fastest with the highest IOPS. Locla SSD have the highest throughput and lowest latency, but are only good for temp storage.

<b>Network</b> includes auto default or custom networks. It can include ingress and egress firewall rules and load balancers. All instances are part of a VPC by default. 

## Create Ubuntu Instance

    gcloud compute instances create compute-exercise --project=big-genre-303821 --zone=us-east1-b --machine-type=e2-micro --network-interface=network-tier=PREMIUM,subnet=default --maintenance-policy=MIGRATE --provisioning-model=STANDARD --service-account=11613812069-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --create-disk=auto-delete=yes,boot=yes,device-name=compute-exercise,image=projects/ubuntu-os-cloud/global/images/ubuntu-1804-bionic-v20221201,mode=rw,size=10,type=projects/big-genre-303821/zones/us-east1-b/diskTypes/pd-standard --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --labels=env=testing --reservation-affinity=any

## Compute Engine Machine Type

e2-standard-32

Generation - type - vCPUs

<b>General Purpose</b> - Good for day to day computing at a low cost. Good for web serving, apps, back office apps. e2 types are lowest on demand pricing and support up to 32vCPUS and 128GB of mem. n1 offers 96cpus and 624 gb of memory. n2d has 224 vCPUs and 895 GB of mem.

<b>Compute Optimize</b> - ultra high performance for HPC and gaming. c2 is compute intiensive and highest performance per core. max of 16 cpus and 240gb of mem.

<b>Memory Optimize</b> - large in memory databases. m1 is intensive memroy use. 4-cpus and 11,776GB of memory. Great for SQL. Only available in specifc zones.

## Managing Instances

Lifecycle starts with Provisiong, Staging, Running, STopping, and then Terminating.

<b>Provisioning</b> vCPU and mem, root disk, additional disk. When resrouces are being allocated. No cost

<b>Staging</b> Resources are acquired, internal and external IPs, booting. No cost
    Shielded VMs insure your instance hasnt been compromised. The boot process start with secure boot by verifying softwar through googles cert authority. integrity monitoring determines if there have been changes made which is loaded by the Virtual Trusted Platform Module. This insures a good baseline policy. 

<b>Running</b> Instance running, ssh and rdp, modify, repair, or reset instance. Set/get metadata, export system image, migrate. Cost for instance, static IP, and disks. A guest env is automatically installed with binaries through the metadata server. This guest env is availble on linux and windows. Compute engine provides a central point to restore metadata to be assigned to projects. You can retrieve this metadata from the project and the instance using the following URL:

    curl http://metadata.google.internal/computeMetadata/v1/project/
    curl http://metadata.google.internal/computeMetadata/v1/instance/

Once boot is complete you can access via ssh or rdp. You will need firewall rules for SSH or RDP.
Google recommends using OS Login to use IAM roles to login to your instance.

<b>Stopping</b> Shutdown scripts. Suspend. Cost for statick IPs and disk.

<b>Terminating</b> Availability policy and deletion of instance. Cost for static IPs and disk. 

## Metadata and Scripts

This will allow you to pull the metadata from an instance once you are SSH'd in.

    curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/project/

        attributes/
        numeric-project-id
        project-id
 
    curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/instance/
        attributes/
        cpu-platform
        description
        disks/
        guest-attributes/
        hostname
        id
        image
        licenses/
        machine-type
        maintenance-event
        name
        network-interfaces/
        preempted
        remaining-cpu-time
        scheduling/
        service-accounts/
        tags
        virtual-clock/
        zone

Add custome metadata

    gcloud compute instances add-metadata compute-exercise --metadata env=dev --zone us-east1-b
        Updated [https://www.googleapis.com/compute/v1/projects/big-genre-303821/zones/us-east1-b/instances/compute-exercise].

Verify by querying the instance under attributes   

    curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/instance/attributes/
        env
        ssh-keys

Add the following script to the metadata that will run a webserver. Also enable http for the firewall rules from the instance. You can just do port 80 and then reset the instance so the startup script will run

    Startup script 
        #! /bin/bash
        ENV=$(curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/instance/attributes/env)
        NAME=$(curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/instance/name)
        ZONE=$(curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/instance/zone | sed 's@.*/@@')
        PROJECT=$(curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/project/project-id)
        apt-get update
        apt-get install -y apache2
        cat <<EOF> /var/www/html/index.html
        <body style="font-family: sans-serif">
        <html><body><h1>Aaaand.... Success!</h1>
        <p>My machine name is <span style="color: #3BA959">$NAME</span> and I serve the <span style="color: #3BA959">$ENV</span> environment.</p>
        <p>I live comfortably in the <span style="color: #5383EC">$ZONE</span> datacenter and proudly serve Tony Bowtie on the <span style="color: #D85040">$PROJECT</span> project.</p>
        <p><img src="https://storage.googleapis.com/tony-bowtie-pics/tony-bowtie.svg" alt="Tony Bowtie"></p>
        </body></html>
        EOF

You can also createa  bucket and upload the script as a .sh file, then go to the instance metadata and add it under the bucket URI to the file to run the same thing. 

This should show a webpage.

# Storage Fundamentals

<b>Block Storage</b> refered to as block level storage, fastest available. Evenly sized blocks that are uniquely defined, mountable and bootable. Solid state dirves and spinning hard drives. 

<b>File Storage</b> file level that is used as a network storage file system. Works like a local hard drive but cant be adjusted after its booted. Directory tree structure. Cloud File Store manages the users and access.

<b>Object Storage</b> Flat collection of unstructured data. made of the data, metadata, and gloabl unique ID. infinitely scaleable, also known as Cloud Storage or Buckets. 

## Storage Performance Terms

IO - input output or read write request. 1 kb - 4mb

IO queue depth - number of pending IO requests. Occure when read or write are queued

IOPS - metric that stands for input output operations per second. 

Throughput - speed data is transfered in mb per second

Latency - measure of time between data request to data return

Sequential - large single file

Random Access - lots of little files all over the place, slower than sequential..

All of the characteristics have a role in achieving the performance needed.

## Persistent Disk and Local SSDs

https://cloud.google.com/compute/docs/disks - <b>need to know the basics of these PD!!</b>

PD - are network disk connected over googles internal network. Independent, resize while running, encrypted by default, up to 64tb in size. 

Zonal PD occur in one zone and one region, cannot survive an outage, thus the need for snapshot in Zonal PDs. 

Regional PD have durable storage and occur in two zones in the same region. Can fail over workload to another zone with the force attached flag. have 200gb size minimum. 

Zonal standard PD have better IOPS than Regional.

Balanced PD - Alternative to SSD, same max IOPS, general purpose. Lower IOPS per GB.

PD standard are the standard hard disks. Great for large sequenetial access workloads. Backed by standard hard disks. 

PD SSD - great for high performance DB that demand a lower altency. 5 times faster than balanced PD in IOPS.

Local SSD - physically attached to server, higher throughput and lower latency. Data persists until intance is stopped or deleted. Max of 9tb per insance. High IOPS low latency. Available in SCSI and NVME (non volatile memeory express). Fast scratch disk or cache. 

## Manage Disk on Compute Engine, Create, Attach, Format, and Mount Disk

Create your instance. Ensure you access scope of read write for compute engine. Then create your disk.

    gcloud compute disks create pd-disk --project=big-genre-303821 --type=pd-balanced --size=100GB --labels=env=testing --zone=us-east1-b

Login to the instance and list your block devices

    gcloud compute ssh --zone "us-east1-b" "compute-exercise"  --project "big-genre-303821"
    lsblk
        NAME    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
        loop0     7:0    0 302.1M  1 loop /snap/google-cloud-cli/89
        loop1     7:1    0  49.6M  1 loop /snap/snapd/17883
        loop2     7:2    0  55.6M  1 loop /snap/core18/2632
        loop3     7:3    0 303.4M  1 loop /snap/google-cloud-cli/91
        sda       8:0    0    10G  0 disk 
        ├─sda1    8:1    0   9.9G  0 part /
        ├─sda14   8:14   0     4M  0 part 
        └─sda15   8:15   0   106M  0 part /boot/efi

Attach your disk to the instance.

    gcloud compute instances attach-disk compute-exercise --disk pd-disk --zone us-east1-b
        Updated [https://www.googleapis.com/compute/v1/projects/big-genre-303821/zones/us-east1-b/instances/compute-exercise
    
    lsblk
        NAME    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
        loop0     7:0    0 302.1M  1 loop /snap/google-cloud-cli/89
        loop1     7:1    0  49.6M  1 loop /snap/snapd/17883
        loop2     7:2    0  55.6M  1 loop /snap/core18/2632
        loop3     7:3    0 303.4M  1 loop /snap/google-cloud-cli/91
        sda       8:0    0    10G  0 disk 
        ├─sda1    8:1    0   9.9G  0 part /
        ├─sda14   8:14   0     4M  0 part 
        └─sda15   8:15   0   106M  0 part /boot/efi
        sdb       8:16   0   100G  0 disk 

The sdb is your new disk. Check it.

    sudo file -s /dev/sdb
        /dev/sdb: data

Format the drive

    sudo mkfs.ext4 -F /dev/sdb
        mke2fs 1.44.1 (24-Mar-2018)
        Discarding device blocks: done                            
        Creating filesystem with 26214400 4k blocks and 6553600 inodes
        Filesystem UUID: 6e987741-c34b-45db-821b-4c8f48793db7
        Superblock backups stored on blocks: 
                32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
                4096000, 7962624, 11239424, 20480000, 23887872

        Allocating group tables: done                            
        Writing inode tables: done                            
        Creating journal (131072 blocks): done
        Writing superblocks and filesystem accounting information: done

    sudo file -s /dev/sdb 
        /dev/sdb: Linux rev 1.0 ext4 filesystem data, UUID=6e987741-c34b-45db-821b-4c8f48793db7 (extents) (64bit) (large files) (huge files)

Create new directory for the disk and Mount the disk using a mount point

    sudo mkdir /newpd
    sudo mount /dev/sdb /newpd
    lsblk
        NAME    MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
        loop0     7:0    0 302.1M  1 loop /snap/google-cloud-cli/89
        loop1     7:1    0  49.6M  1 loop /snap/snapd/17883
        loop2     7:2    0  55.6M  1 loop /snap/core18/2632
        loop3     7:3    0 303.4M  1 loop /snap/google-cloud-cli/91
        sda       8:0    0    10G  0 disk 
        ├─sda1    8:1    0   9.9G  0 part /
        ├─sda14   8:14   0     4M  0 part 
        └─sda15   8:15   0   106M  0 part /boot/efi
        sdb       8:16   0   100G  0 disk /newpd

Change to the mount directory and create a new file in the folder.

    cd /newpd
    sudo nano testing.txt
    cat testing.txt
    df -h
        Filesystem      Size  Used Avail Use% Mounted on
        udev            465M     0  465M   0% /dev
        tmpfs            98M  904K   97M   1% /run
        /dev/sda1       9.6G  2.3G  7.2G  25% /
        tmpfs           487M     0  487M   0% /dev/shm
        tmpfs           5.0M     0  5.0M   0% /run/lock
        tmpfs           487M     0  487M   0% /sys/fs/cgroup
        /dev/loop0      303M  303M     0 100% /snap/google-cloud-cli/89
        /dev/loop1       50M   50M     0 100% /snap/snapd/17883
        /dev/loop2       56M   56M     0 100% /snap/core18/2632
        /dev/loop3      304M  304M     0 100% /snap/google-cloud-cli/91
        /dev/sda15      105M  4.4M  100M   5% /boot/efi
        tmpfs            98M  4.0K   98M   1% /run/user/1002
        /dev/sdb         98G   28K   93G   1% /newpd
            
Test persistence by rebooting the instance and then logging back in to ensure the test file is still there.

    reboot

The file is not there because we need to edit the storage config file in linux. Take the uuid and append to the fstab filesudo nano

    sudo blkid /dev/sdb
        /dev/sdb: UUID="6e987741-c34b-45db-821b-4c8f48793db7" TYPE="ext4"
        
    sudo nano /etc/fstab 
        LABEL=cloudimg-rootfs   /        ext4   defaults        0 1
        LABEL=UEFI      /boot/efi       vfat    umask=0077      0 1
        UUID=6e987741-c34b-45db-821b-4c8f48793db7 /newpd ext4 defaults,nofail

Mount again

    sudo mount -a

Resize the disk

    gcloud compute disks resize pd-disk --size 150 --zone us-east1-b

        This command increases disk size. This change is not reversible.
        For more information, see:
        https://cloud.google.com/sdk/gcloud/reference/compute/disks/resize

        Do you want to continue (Y/n)?  y

        Updated [https://www.googleapis.com/compute/v1/projects/big-genre-303821/zones/us-east1-b/disks/pd-disk].
        ---
        creationTimestamp: '2022-12-10T13:55:58.509-08:00'
        id: '4801319736937332353'
        kind: compute#disk
        labelFingerprint: E-Rp3_dLV_0=
        labels:
        env: testing
        lastAttachTimestamp: '2022-12-10T13:58:56.932-08:00'
        name: pd-disk
        physicalBlockSizeBytes: '4096'
        selfLink: https://www.googleapis.com/compute/v1/projects/big-genre-303821/zones/us-east1-b/disks/pd-disk
        sizeGb: '150'
        status: READY
        type: https://www.googleapis.com/compute/v1/projects/big-genre-303821/zones/us-east1-b/diskTypes/pd-balanced
        users:
        - https://www.googleapis.com/compute/v1/projects/big-genre-303821/zones/us-east1-b/instances/compute-exercise
        zone: https://www.googleapis.com/compute/v1/projects/big-genre-303821/zones/us-east1-b
        

Resize on the instance

    sudo resize2fs /dev/sdb
        resize2fs 1.44.1 (24-Mar-2018)
        Filesystem at /dev/sdb is mounted on /newpd; on-line resizing required
        old_desc_blocks = 13, new_desc_blocks = 19
        The filesystem on /dev/sdb is now 39321600 (4k) blocks long.

    df -k
        Filesystem     1K-blocks    Used Available Use% Mounted on
        udev              475692       0    475692   0% /dev
        tmpfs              99668     900     98768   1% /run
        /dev/sda1        9974088 2408464   7549240  25% /
        tmpfs             498336       0    498336   0% /dev/shm
        tmpfs               5120       0      5120   0% /run/lock
        tmpfs             498336       0    498336   0% /sys/fs/cgroup
        /dev/loop0        309504  309504         0 100% /snap/google-cloud-cli/89
        /dev/loop1         50816   50816         0 100% /snap/snapd/17883
        /dev/loop2         56960   56960         0 100% /snap/core18/2632
        /dev/loop3        310784  310784         0 100% /snap/google-cloud-cli/91
        /dev/sda15        106858    4477    102382   5% /boot/efi
        tmpfs              99664       8     99656   1% /run/user/1002
        /dev/sdb       154232632      32 146855264   1% /newpd


Good Job!

Detatch the disk from the instance

    gcloud compute instances detach-disk compute-exercise --disk pd-disk --zone us-east1-b  
        Updated [https://www.googleapis.com/compute/v1/projects/big-genre-303821/zones/us-east1-b/instances/compute-exercise].

Delete disk from console.

## Snapshots

https://github.com/antonitz/google-cloud-associate-cloud-engineer/tree/master/07-Compute-Engine/04_managing_snapshots

PD Snashots are a great way to backup data and achieving uptime. They restore data from your PDs and are global resources so available to all resources within a project. You can create them through increametns and automatically. Stored in cloud storage. 

The best way to back up your data is scheduled snapshots. Must be in same region as PD. 

Create your instance

    gcloud compute instances create snapshot-instance --zone=us-east1-b --machine-type=e2-micro --subnet=default --image=debian-10-buster-v20201112 --image-project=debian-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard

        WARNING: You have selected a disk size of under [200GB]. This may result in poor I/O performance. For more information, see: https://developers.google.com/compute/docs/disks#performance.
        Created [https://www.googleapis.com/compute/v1/projects/big-genre-303821/zones/us-east1-b/instances/snapshot-instance].
        WARNING: Some requests generated warnings:
        - The resource 'projects/debian-cloud/global/images/debian-10-buster-v20201112' is deprecated. A suggested replacement is 'projects/debian-cloud/global/images/debian-10-buster-v20201216'.

        NAME               ZONE        MACHINE_TYPE  PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP    STATUS
        snapshot-instance  us-east1-b  e2-micro                   10.142.0.3   35.231.151.12  RUNNING

SSH into the instance and create a file

    gcloud compute ssh --zone "us-east1-b" "snapshot-instance"  --project "big-genre-303821"

Now create a new snapshot from the console. Then create a new instance running that snapshot image. 

SSH into the new instance and see you file

    gcloud compute ssh --zone "us-east1-b" "instance-2"  --project "big-genre-303821"
    cypher@instance-2:~$ ls
        snapshot-text-file.txt

Create a snapshot schedule from the console then attach it to a disk.

Then create a snapshot instance via CLI on the instance that you created from a snapshot.

    gcloud compute resource-policies create snapshot-schedule schedule-1 --region=us-east1 --max-retention-days=14 --on-source-disk-delete=apply-retention-policy --daily-schedule --start-time=06:00 --storage-location=us-east1

        ERROR: (gcloud.compute.resource-policies.create.snapshot-schedule) Could not fetch resource:
        - Request had insufficient authentication scopes.

You need to add the correct permissions. I had security issues with the URL verifications so I added the snapshot schedule to the instance disk via console. Snapshots will help prevent dataloss once set into place. 

## Deployment Manager

https://github.com/antonitz/google-cloud-associate-cloud-engineer/tree/master/07-Compute-Engine/05_deployment_manager

IaC that allows you deploy, update and destroy resources to the cloud. Uses YAML, JSON, and Jinja. Configuration files must have 3 componenets - name, type, proeperty.

You can use templates for general or speific configurations. Templates can be written in Jinja or Python. Template properties are arbitrary variables to abstract property values. 

Gcloud also offers predefined templates in the deploymnet manager. 

The manifest is read only and based on the configuation files. Its best to create separate configs for security, network, etc and to use references for different services in your cloud. You can also preiew deployments using the --preview flag. 

Create the Deployment_Manager Folder and Files. Make sure to enable the API in the console. 

Ensure you are in the correct project

    gcloud config list
        [core]
        account = dominickhrndz314@gmail.com
        disable_usage_reporting = False
        project = big-genre-303821
        [metrics]
        environment = interactive_shell

        Your active configuration is: [default]

Deploy your config, but preview it first

    gcloud deployment-manager deployments create bowtie-deploy --config bowtie-deploy.yaml --preview

        The fingerprint of the deployment is b'oVYtvyvzY7R0ncU7ixyA7A=='
        Waiting for create [operation-1670771799431-5ef8edf9b1b1e-ff602a1d-bc8ba3fd]...done.                                                                       
        Create operation operation-1670771799431-5ef8edf9b1b1e-ff602a1d-bc8ba3fd completed successfully.
        NAME               TYPE                   STATE       ERRORS  INTENT
        bowtie-network     compute.v1.network     IN_PREVIEW  []      CREATE_OR_ACQUIRE
        bowtie-website     compute.v1.instance    IN_PREVIEW  []      CREATE_OR_ACQUIRE
        public             compute.v1.subnetwork  IN_PREVIEW  []      CREATE_OR_ACQUIRE
        ssh-access         compute.v1.firewall    IN_PREVIEW  []      CREATE_OR_ACQUIRE
        web-server-access  compute.v1.firewall    IN_PREVIEW  []      CREATE_OR_ACQUIRE

You should be able to see the deployment and manifest file from the console. However, none of the resources have actually been deployed yet, so do that now. You can either hit Deploy from the console or run it from the CLI. 

    gcloud deployment-manager deployments update bowtie-deploy 


The deployment will fail because the image used in this sample doesn't include Oath tokens to deploy the debian image. Thats ok. 

Delete everything and move on. 

# HA and Autoscaling

NEED TO KNOW FOR EXAM - https://cloud.google.com/load-balancing/docs/choosing-load-balancer

https://github.com/antonitz/google-cloud-associate-cloud-engineer/tree/master/08-High-Availability-and-Autoscaling

Load Balancing is the defacto for balancing traffic. it distrubutes user traffic across multiple instances with a single point of entry and multiple backends. It is completely software defined, there is either global or regional to serve content as close as possible to uers. It uses autoscaling for health checks. 

 If you need cross-regional load balancing for a web application, use Global HTTP(S) load balancing. For Secure Sockets Layer traffic that is not HTTP, use the Global SSL Proxy load balancer. If it’s other TCP traffic that doesn’t use SSL, use the Global TCP Proxy load balancer. But what if you want to load balance traffic inside your project, say, between the presentation layer and the business layer of your application? For that, use the Regional internal load balancer.

Global LB are great for external HTTP(S) LB, SSL proxy LB, and TCP proxy LB.
Regional LB is good for internal HTTP(S), Internal TCP/UDP,, and Network LB.

External LB
Internal LB

Traffic Type

There are 5 Load Balancers available. Backend services are what define how cloud lb distributes traffic. NEG is a network endpoint group. Session affinity sends the ame clients to the same backend. Service timeout determines how long a connection will wait. 

HTTP(S) LB distributes traffic via forwarding rules. When you configure this LB user requests are sent tot he clostest instance group thats not at capacity. This is a global proxy based layer 7 LB. Its the only layer 7 LB. you an serve apps worldwide with a single unicast IP address. Its implelmenet on GFE or Google Front eEnds which offer cross region load balancing. Its handled regionally, but gloabl, etxternal, and intneral. It can do Ipv4 and Ipv6, distributed by location or content. URL maps direct request based on rules using ports 8,8080,443.

SSL Proxy LB client ssl sessions terminated at the LB. Is a reverse proxy LB that distributes ssl traffic from the tinernet to the VMs. Global and external, distribute traffic by location only, layer 4, LB, sypport for TVP SSL offload. Used for protocols such as websockets and IMAP. 

TCP Proxy is reverse proxy LB from from internet to VMs. FOrewards traffic as SSSL or TCP with a single unicast IP address. L4 LB. Disctributes by location only and intended for non HTTP traffic. Ipv4 and Ipv6 enabled. Supports ports like 25 or SMTP.

Netowkr LB is a pass through load balancer that distrubtes TCP and UDP to VMs. Not a proxy, respinded go gfrom backedn to client. Regional and external. Supports either tcp or udp not both. Not tsls offloading or proxy. SSL decrytped by backedns not LB. 

Internal LB is a l4 regional LB that distributes TCP and UDP traffic to VMs, but not both. Regional and internal, cannot be used to balance internet traffic. 


## Instance Groups and Instance Templates

Instance groups are a colletion of VM instances that you can manage as a single entity. 

Managed instance groups - stateless workloads like website front ends and web apps. High performance. Autohealing, controlled upates. Maintain HA by keeping apps in running states. Recreates VMs when not running. App based autohealing. Zonal or regional. Regional provides HA. Google recommend regional MIGs in case of a zonal failure. Load Balancing can use instance groups to serve traffic and work together to handle traffic. Can autscale to add or remove instances form the MIG to meet load demands or shrink. Can safely role out updates, scope and speed. Rolling updates or canary testing which validates software to a small group of users. 

Unmanaged instance groups - do not offer autoscaling, auto healing etc. Only used for LB to mixed isntance types. 

Instance Template - is used to create VM insances and MIGs. Global resource not bound to zone or region. If you want to create a group of idnetical instanece you must use an instance template. 

## Create Instance Template and Load Balancer

Startup script to create instance group from console. Use n1 as compute type.

    #! /bin/bash
    NAME=$(curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/instance/name)
    ZONE=$(curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/instance/zone | sed 's@.*/@@')
    sudo apt-get update
    sudo apt-get install -y stress apache2
    sudo systemctl start apache2
    cat <<EOF> /var/www/html/index.html
    <html>
        <head>
            <title> Managed Bowties </title>
        </head>
        <style>
    h1 {
    text-align: center;
    font-size: 50px;
    }
    h2 {
    text-align: center;
    font-size: 40px;
    }
    h3 {
    text-align: right;
    }
    </style>
        <body style="font-family: sans-serif"></body>
        <body>
            <h1>Aaaand.... Success!</h1>
            <h2>MACHINE NAME <span style="color: #3BA959">$NAME</span> DATACENTER <span style="color: #5383EC">$ZONE</span></h2>
            <section id="photos">
                <p style="text-align:center;"><img src="https://storage.googleapis.com/tony-bowtie-pics/managing-bowties.svg" alt="Managing Bowties"></p>
            </section>
        </body>
    </html>
    EOF

Create instance group. You can either do it from the console or CLI

    gcloud beta compute health-checks create tcp healthyinstance --project=big-genre-303821 --port=80 --proxy-header=NONE --no-enable-logging --check-interval=5 --timeout=5 --unhealthy-threshold=2 --healthy-threshold=2 && gcloud beta compute instance-groups managed create instance-group-1 --project=big-genre-303821 --base-instance-name=instance-group-1 --size=3 --template=instance-template-1 --zones=us-east1-b,us-east1-c,us-east1-d --target-distribution-shape=EVEN --instance-redistribution-type=PROACTIVE --list-managed-instances-results=PAGELESS --health-check=healthyinstance --initial-delay=300 && gcloud beta compute instance-groups managed set-autoscaling instance-group-1 --project=big-genre-303821 --region=us-east1 --cool-down-period=60 --max-num-replicas=6 --min-num-replicas=3 --mode=on --target-cpu-utilization=0.6

You should already have a firewall rule that will allow the healthc checks. If not you need to add the IPs to a new rule. The IP for healthchecks is 130.211.0.0/22 and 35.191.0.0/16

Create the HTTP(S) Load Balancer. from console configure the backend to use your instance group, the frontend on port 80.

Once its created keep in mind it takes time for the LB to deploy the backedn for your instances. Ensure all 3 are healthy. You wont be able to view the webpage because the script he provided isn't working as usual. 

However, the script basically brings up a web page where each time you refresh it will show the machine name and datacetner changing to show load balancing in action. 

SSH into one of the group instance. Now we will run the stress test. This stress test will end up creating new instances in the group to handle the load. 

    gcloud compute ssh --zone "us-east1-b" "instance-group-1-l2ql"  --project "big-genre-303821"

    stress --cpu 2 --io 1 --vm 1 --vm-bytes 128M --timeout 30s
        stress: info: [2206] dispatching hogs: 2 cpu, 1 io, 1 vm, 0 hdd
        stress: info: [2206] successful run completed in 30s

From the Console you can see the scale has gone from 3 - 6 instances in the group. Now we want to scale in our instances. To do this simply hit restart in the instance group from the console. THIS SHOULD NEVER BE DONE IN PROD. You can also do so from the cli

    gcloud beta compute instance-groups managed rolling-action start-update instance-group-1 --project=big-genre-303821 --type='proactive' --max-surge=0 --max-unavailable=3 --min-ready=0 --minimal-action='restart' --most-disruptive-allowed-action='' --replacement-method='substitute' --version=template=https://www.googleapis.com/compute/beta/projects/big-genre-303821/global/instanceTemplates/instance-template-1 --region=us-east1

Once you see the instances scaled down, delete all of these resources. First delete the LB and backend service. Delete instance group and template. 

# GKE

Much of Kubernetes was covered in my CKA exam. For that reason I will only do the labs and skip lectures related to theory. To create a cluster in the google cli run

    gcloud container clusters create <name>

Docker containers are created from a docker image.

Docker Image 

Layers - Base Image, Installed Software, Working Directories, Expose, Command

    FROM ubuntu:latest
    LABEL vendor="Bowtie Inc"
    RUN apt-get update && apt-get install -y apache2
    COPY index.html /var/www/html/
    COPY boxofbowties*.jpg /var/www/html/
    ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
    EXPOSE 80

GKE manages all the control plane in Kubernetes. Its conencted through the cloud controller manager.  The Cluster IP is exposed to GKE to get access to the cluster. 

You can deploy single zone or multi zonal clusters. The control plane will distribute nodes acrorss zone for multi zonal.  You can also regional clusters which has a control plane in each region instead of just scatted nodes. 

For private clusters you can use VPC peering so that a google maneged project can have access to a customer managed project. The control plane will sit in the google managed project and nodes in the customer. 

Control plane and nodes dont always run the same version. Control plane is always upgraded first then the nodes. During upgrades you can control the number of nodes GKE can updgrade at once using the surge upgrade parameters max-surge-upgrade and max-unavailable-upgrade. These work similar to rolling updates and node availability. 

<b>Anthos</b>  Anthos is a hybrid and multi cloud solution powered by the latest innovations in distributed systems and service management software from Google.  The Anthos framework rests on Kubernetes and GKE On-Prem. This provides the foundation for an architecture that's fully integrated and has centralized management for a central control plane that supports policy based application lifecycle delivery across hybrid and multiple cloud environments. Anthos also provides a rich set of tools for monitoring and maintaining the consistency of your applications across all of your network, whether on premises in the cloud or in multiple clouds.

Its a service mesh essentially which can be connected to Istio where youre hosting on-prem resrouces. 

## GKE Lab

Create a cluster in GKE in one zone. You can do this in the console or from the cli.

    gcloud container clusters ccreate cluster-1 --zone us-east1-b --no-enable-basic-auth --machin-type e2-micro --disk-type pd-standard --disk-size 10 --num-nodes 3 --enable-stackdriver-kubernetes --enable-autoscaling --min-nodes 1 --max-nodes 3 --no-enable-master-authorized-networks --addon HttpLoadBalancing --enable-autoupgrade --enable-autorepair

Ensure you have the kubectl and gke-gcloud-auth-plugin component installed and then get the credentisla for the cluster.

    gcloud container clusters get-credentials cluster-1 --zone us-east1-b
        Fetching cluster endpoint and auth data.
        kubeconfig entry generated for cluster-1.

You should now be able to interact with your cluster. 

    kubectl get nodes 
        NAME                                       STATUS   ROLES    AGE     VERSION
        gke-cluster-1-default-pool-ad9b511e-3mwc   Ready    <none>   6m42s   v1.24.5-gke.600
        gke-cluster-1-default-pool-ad9b511e-6ss0   Ready    <none>   6m42s   v1.24.5-gke.600
        gke-cluster-1-default-pool-ad9b511e-h2xv   Ready    <none>   6m42s   v1.24.5-gke.600

Import the GKE repo for the deploying the new app. We then need to ue Cloud Build. Its a servelress CICD platform that will allow us to package our code into a container. We will use it just to push it out, not to build the pipeline. It will allow you to build images without leaving GCP.

    gcloud services enable cloudbuild.googleapis.com
        Operation "operations/acf.p2-11613812069-fdeafbb5-432d-462f-8f18-fce6ea0d4912" finished successfully.


Build the image.

    gcloud builds submit --tag gcr.io/${GOOGLE_CLOUD_PROJECT}/boxofbowties:1.0.0 .

The container registry image hes using is not accessible and I keep recieving an error. 

    starting build "e814625a-0af1-44ce-864c-b7a0b3dff0fe"

    FETCHSOURCE
    Fetching storage object: gs://big-genre-303821_cloudbuild/source/1670781242.167727-6cb4521402cc488b97a9670843025924.tgz#1670781243173104
    Copying gs://big-genre-303821_cloudbuild/source/1670781242.167727-6cb4521402cc488b97a9670843025924.tgz#1670781243173104...
    / [0 files][    0.0 B/ 82.9 KiB]                                                
    / [1 files][ 82.9 KiB/ 82.9 KiB]                                                
    Operation completed over 1 objects/82.9 KiB.                                     
    BUILD
    Already have image (with digest): gcr.io/cloud-builders/docker
    invalid argument "gcr.io//boxofbowties:1.0.0" for "-t, --tag" flag: invalid reference format
    See 'docker build --help'.
    ERROR
    ERROR: build step 0 "gcr.io/cloud-builders/docker" failed: step exited with non-zero status: 125

From workloads you can then create the deployment in the console and deploy. In the work load you can also view the YAML of the deployment. 

From the CLI run a kubectl get deployment to see your deployment is there. 

Do a kubectl get pods to see the pods. 

From Workloads you can also expose the deployment in the console to enable nodeport. 

I should try this exercise again with a new image. 

# Hybrid Connectivity

<b>Cloud VPN</b> connects your peer netowrk to you VPC network through an IPsec VPN to on prem, another cloud provider, or another VPN.

It an encrypted tunnel over the public internet and decrypted by the other VPN gteway. Regional service, siste to site VPN only so you cant use it on a laptop. Allows Private Google Access for onprem hosts. Support 3GB totla in synamic or static routing. Ikev1 and Ikev2 supported using shared secrt.

Classic VPN and HA VPN. ALthough this may not be true any longer. 

HA VPN supports dynamic routing only 2 external IPs to be configured for 2 interfaces. New default VPN. Supports double connections or multiple tunnels. 

Classic does static and dynamic, 1 IP. No redundancy.

Use Vloud VPN when pun;oc internet access is needed, peering location is not availablem and budget constraints.

<b>Cloud Interconnect</b> allows onprem connectivity to VPCs. Demand for fast low latency connections. HA connection. Provides private google access using internal ip address. Doesnt traverse public internet. Not encrypted but dedicated connection. Very costly. used to connect to providers.  

<b>Dedicated Interconnect</b> provides phsycial connection. 8 X 10 GBPS of 80 GBS totla. 2 X 100 GBPS for 200 GBS total. Supported colo facility. uitable for non critical applications. This option allows for one or more direct private connections to Google. Also, these connections can be backed up by a VPN for even greater reliability.

<b>Partner Interconnect</b> 50 mbps to 50 gbps Vlan attachments. Google peering edge in colo to service provider edge at ISP. The ISP provides vlan connection. You can use multiple redundant service providers. A Partner Interconnect connection is useful if a data center is in a physical location that can't reach a Dedicated Interconnect co location facility. Or if the data needs don't warrant an entire 10 GigaBytes per second connection depending on availability, needs, Partner Interconnect can be configured to support mission critical services or applications that can tolerate some downtime.

<b>Direct Peering</b> Carrier peering gives you direct access from your on premises network, through a service provider's network to google workspace and to google cloud products that can be exposed through one or more public IP addresses. allows you to establish a cconnection from onprem to google edge network. 100 locations in 33 countries. 

# Serverless Services

https://github.com/antonitz/google-cloud-associate-cloud-engineer/tree/master/11-Serverless-Services

<b>API Gateway</b> is another API management tool. Web-based services today provide a huge variety of functionality, meaning everything from map, weather, and image services, to games, auctions, and many other service types. Service providers have many options for how to implement, deploy, and manage their services. For example, one service might be developed in Java or .NET, while another uses Node.js. Backend implementations can also vary for a single service provider. A service provider might have legacy services implemented using one architecture, and new services implemented using a completely different architecture. API Gateway enables you to provide secure access to your backend services through a well-defined REST API that is consistent across all of your services, regardless of the service implementation.

<b>Cloud Run</b> a managed compute platform that lets you run stateless Containers via web requests or Pub/Sub events. Cloud Run is serverless.  First, you write your application using your favorite programming language. This application should start a server that listens for web requests. Second, you build and package your application into a container image. Third, the container image is pushed to Artifact Registry where Cloud Run, will deploy it. You can use a container-based workflow as well as a source-based workflow. If you use the source-based approach, you'll deploy your source code instead of a container image. Cloud Run then builds your source and packages the application into a container image for you. Cloud Run does this using buildpacks, an open source project. Cloud Run handles HTTPS serving for you. This means you only have to worry about handling web requests and you can let Cloud Run take care of adding the encryption. 

    gcloud services enable run.googleapis.com

    JSON File 
        {
        "name": "helloworld",
        "description": "Simple hello world sample in Node",
        "version": "1.0.0",
        "main": "index.js",
        "scripts": {
            "start": "node index.js"
        },
        "author": "Google LLC",
        "license": "Apache-2.0",
        "dependencies": {
            "express": "^4.17.1"
        }
        }

    JS FIle

    const express = require('express');
    const app = express();
    const port = process.env.PORT || 8080;
    app.get('/', (req, res) => {
    const name = process.env.NAME || 'World';
    res.send(`Hello ${name}!`);
    });
    app.listen(port, () => {
    console.log(`helloworld: listening on port ${port}`);
    });

    DOCKERFILE

    # Use the official lightweight Node.js 12 image.
    # https://hub.docker.com/_/node
    FROM node:12-slim
    # Create and change to the app directory.
    WORKDIR /usr/src/app
    # Copy application dependency manifests to the container image.
    # A wildcard is used to ensure copying both package.json AND package-lock.json (when available).
    # Copying this first prevents re-running npm install on every code change.
    COPY package*.json ./
    # Install production dependencies.
    # If you add a package-lock.json, speed your build by switching to 'npm ci'.
    # RUN npm ci --only=production
    RUN npm install --only=production
    # Copy local code to the container image.
    COPY . ./
    # Run the web service on container startup.
    CMD [ "npm", "start" ]

    gcloud builds submit --tag gcr.io/$GOOGLE_CLOUD_PROJECT/helloworld
    gcloud container images list
    docker run -d -p 8080:8080 gcr.io/$GOOGLE_CLOUD_PROJECT/helloworld
    curl localhost:8080
        Hello World!
    
Deploy you conatinerized application to cloud run is done, so now add it to the project

    gcloud run deploy --image gcr.io/$GOOGLE_CLOUD_PROJECT/helloworld --allow-unauthenticated --region=$LOCATION

Navigate to Cloud Run in the console to see your deployment


<b>App Engine</b> Fully managed serverless platform PaaS for hosting web apps. Launch code as is or in container. Autoscaling based on load. Deals with rapid scaling and allows you to switch versions. Great for testing apps before full deployment. Support external storage such as cloud sql or firestore. Standard and flexible env. 

Standard - Standard environment features include persistent storage with queries, sorting and transactions, automatic scaling and load balancing, asynchronous task queues for performing work outside the scope of a request, scheduled tasks for triggering events at specified times or regular intervals, and integration with other Google Cloud services and APIs. runs in a sandbox env independent of location or hardware. free or very low cost. designed for sudden spikes in trffic. pricing based on instance hours.  You must use specified versions of Java, Python, PHP, Go, Node.js, and Ruby and your application must conform to sandbox constraints that are dependent on runtime.

First, a web application is developed and tested locally. Second, the SDK is used to deploy the application to App Engine. Third, App Engine scales and services the application. But the standard environment, you can't use SSH to connect to the virtual machines on which your application runs and you can't write to a local disk. 

Flexible - This option lets an application run inside Docker containers on Google Cloud's Compute Engine virtual machines. In this case, App Engine manages Compute Engine machines for you. This means that instances are health-checked, healed as necessary, and co-located with other module instances within the project.  runs in docker containers, no free quota, designed for consisyent traffic, pricing based on VM resources. Managed VMs.

Go to the serverless repo and run the gcloud command to deploy version 1

    gcloud app deploy --version 1

    You are creating an app for project [big-genre-303821].
    WARNING: Creating an App Engine application for a project is irreversible and the region
    cannot be changed. More information about regions is at
    <https://cloud.google.com/appengine/docs/locations>.

    Please choose the region where you want your App Engine application located:

    [1] asia-east1    (supports standard and flexible)
    [2] asia-east2    (supports standard and flexible and search_api)
    [3] asia-northeast1 (supports standard and flexible and search_api)
    [4] asia-northeast2 (supports standard and flexible and search_api)
    [5] asia-northeast3 (supports standard and flexible and search_api)
    [6] asia-south1   (supports standard and flexible and search_api)
    [7] asia-southeast1 (supports standard and flexible)
    [8] asia-southeast2 (supports standard and flexible and search_api)
    [9] australia-southeast1 (supports standard and flexible and search_api)
    [10] europe-central2 (supports standard and flexible)
    [11] europe-west   (supports standard and flexible and search_api)
    [12] europe-west2  (supports standard and flexible and search_api)
    [13] europe-west3  (supports standard and flexible and search_api)
    [14] europe-west6  (supports standard and flexible and search_api)
    [15] northamerica-northeast1 (supports standard and flexible and search_api)
    [16] southamerica-east1 (supports standard and flexible and search_api)
    [17] us-central    (supports standard and flexible and search_api)
    [18] us-east1      (supports standard and flexible and search_api)
    [19] us-east4      (supports standard and flexible and search_api)
    [20] us-west1      (supports standard and flexible)
    [21] us-west2      (supports standard and flexible and search_api)
    [22] us-west3      (supports standard and flexible and search_api)
    [23] us-west4      (supports standard and flexible and search_api)
    [24] cancel
    Please enter your numeric choice:  18

    Creating App Engine application in project [big-genre-303821] and region [us-east1]....done.                                                               
    Services to deploy:

    descriptor:                  [/home/cypher/Repositories/Training/GCP_ACE/google-cloud-associate-cloud-engineer/11-Serverless-Services/01_serverless-bowties/sitev1/app.yaml]
    source:                      [/home/cypher/Repositories/Training/GCP_ACE/google-cloud-associate-cloud-engineer/11-Serverless-Services/01_serverless-bowties/sitev1]
    target project:              [big-genre-303821]
    target service:              [default]
    target version:              [1]
    target url:                  [https://big-genre-303821.ue.r.appspot.com]
    target service account:      [App Engine default service account]


    Do you want to continue (Y/n)?  y

    Beginning deployment of service [default]...
    Created .gcloudignore file. See `gcloud topic gcloudignore` for details.
    ╔════════════════════════════════════════════════════════════╗
    ╠═ Uploading 5 files to Google Cloud Storage                ═╣
    ╚════════════════════════════════════════════════════════════╝
    File upload done.
    Updating service [default]...done.                                                                                                                      
    Setting traffic split for service [default]...done.                                                                                                        
    Deployed service [default] to [https://big-genre-303821.ue.r.appspot.com]

Verify the site is up 

     gcloud app browse
        Opening [https://big-genre-303821.ue.r.appspot.com] in a new tab in your default browser.

Now move into the sitev2 directory and deploy that

    gcloud app deploy --version 2

        Services to deploy:

        descriptor:                  [/home/cypher/Repositories/Training/GCP_ACE/google-cloud-associate-cloud-engineer/11-Serverless-Services/01_serverless-bowties/sitev2/app.yaml]
        source:                      [/home/cypher/Repositories/Training/GCP_ACE/google-cloud-associate-cloud-engineer/11-Serverless-Services/01_serverless-bowties/sitev2]
        target project:              [big-genre-303821]
        target service:              [default]
        target version:              [2]
        target url:                  [https://big-genre-303821.ue.r.appspot.com]
        target service account:      [App Engine default service account]


        Do you want to continue (Y/n)?  y

        Beginning deployment of service [default]...
        Created .gcloudignore file. See `gcloud topic gcloudignore` for details.
        ╔════════════════════════════════════════════════════════════╗
        ╠═ Uploading 2 files to Google Cloud Storage                ═╣
        ╚════════════════════════════════════════════════════════════╝
        File upload done.
        Updating service [default]...done.                                                                                                                         
        Setting traffic split for service [default]...done.                                                                                                        
        Deployed service [default] to [https://big-genre-303821.ue.r.appspot.com]

Verfy version 2 is in the console and the migrate traffic back to version 1

Now split the traffic between both versions

Go to settings and disable the app. you cant delete it

## Cloud Functions

Serverless execution env. All handled by google. FaaS where code is executed and you are charged when used. Event driven. Trigger can be firestore, firebase, cloud storage, HTTP, pub sub. 

Billing is based on usage. After selecting name and region you will choose trigger.  You act on event based on trigger. Your written code is put into the function. TLS cert is automatically invoked for secure connection. 

Only one trigger can be bound to a function at once. Function will use a container to react to file being added in a bucket changes in sql or whatever the trigger is that exectues. Cloud function grabs image from cloud registry for the instance.

Functions are not peristsent but have default internet access. 

Enable you API.

Create a default cloud function from the console

Trigger you function    

    gcloud functions describe function-1 --region us-east1

        availableMemoryMb: 256
        buildId: f027f675-5c59-40d9-ad5a-9365dbf346ad
        buildName: projects/11613812069/locations/us-east1/builds/f027f675-5c59-40d9-ad5a-9365dbf346ad
        dockerRegistry: CONTAINER_REGISTRY
        entryPoint: helloWorld
        httpsTrigger:
        securityLevel: SECURE_OPTIONAL
        url: https://us-east1-big-genre-303821.cloudfunctions.net/function-1
        ingressSettings: ALLOW_ALL
        labels:
        deployment-tool: console-cloud
        maxInstances: 3000
        name: projects/big-genre-303821/locations/us-east1/functions/function-1
        runtime: nodejs16
        serviceAccountEmail: big-genre-303821@appspot.gserviceaccount.com
        sourceUploadUrl: https://storage.googleapis.com/uploads-420434102585.us-east1.cloudfunctions.appspot.com/06de2257-401e-4e53-a35b-bee47624224a.zip
        status: ACTIVE
        timeout: 60s
        updateTime: '2022-12-11T20:39:48.089Z'
        versionId: '1'

Trigger it by going to the URL listed. You will be able to see the trigger from the logs.

    https://us-east1-big-genre-303821.cloudfunctions.net/function-1

Create a new function from the 02_you-called repo.

    gcloud functions deploy you_called --runtime python38 --trigger-http --allow-unauthenticated

Delete you functions.

# Storage Services

https://cloud.google.com/storage/docs/storage-classes

https://github.com/antonitz/google-cloud-associate-cloud-engineer/tree/master/12-Storage-Services

<b>Cloud storage</b> is a consistent scalable large capacity object storage. Worldwide accessibility. Great for text, data, pictures, video files, not OS's. Object storage is a computer data storage architecture that manages data as “objects” and not as a file and folder hierarchy (file storage), or as chunks of a disk (block storage).  Administrators have the option to either allow each new version to completely overwrite the older one, or to keep track of each change made to a particular object by enabling “versioning” within a bucket. If you choose to use versioning, Cloud Storage will keep a detailed history of modifications -- that is, overwrites or deletes -- of all objects contained in that bucket.

Buckets are containers that organize and control access to data. has a global unqiq name and geographic location. Region, dual region, and multi region. 

Classes are standard, nearline, coldline, and archive. 

Access is done wither through IAM or ACL's. 

Labels are optional. No limit on number of objects you can store in a bucket. Metadata are proerties such as name, storage class, etc. 

Storage class effect availability and pricing model. 

<b>Standard storage</b> - hot data, max avvaialbility, analutocal workloads and transcoding, no storage duration. Standard Storage is considered best for frequently accessed, or “hot,” data. 

<b>Nearline</b> - IA data, 30 day min sotrage, data back up and archiving. . Examples might include data backups, long-tail multimedia content, or data archiving

<b>Coldline</b> - cold data IA data backip and archiving 90 day min

<b>Archiving</b> - Cold data storage disaster recovery 365 day min

<b>Storage Transfer Service</b> enables you to import large amounts of online data into Cloud Storage quickly and cost-effectively. The Storage Transfer Service lets you schedule and manage batch transfers to Cloud Storage from another cloud provider, from a different Cloud Storage region, or from an HTTP(S) endpoint.

IAM - standard IAM permissions ingherited hieracrchially. Recommended
ACL - defines who has access to bucket and objects as well as level of access. Each access control list consists of two pieces of information. The first is a scope, which defines who can access and perform an action. This can be a specific user or group of users. The second is a permission, which defines what actions can be performed, like read or write. 
Signed URL - time limited read/write URL. Specify user or service account, allow users without credential for specifc actions on a resource. Dont need an account.
Signed Policy Document - specify what can be uploaded to a bucket

Object are immutable and dont change throughout their lifetime. 

lifecycle management sets a time to live for objects, delete or archive non current versions and dowgrade storage class to save money. Each lfc config has rules or set of conditions for actions. 

Create a bucket and an instance that has access scope read write for storage. I already have instances and buckets created from this exercise. 

Upload you rfile to the chosen bucket. 

    gsutil cp test.txt gs://ace-inc-dominick

        Copying file://test.txt [Content-Type=text/plain]...
        /   [1 files][   15.0 B/   15.0 B]                                                
        Operation completed over 1 objects/15.0 B.

Navigate tot he cloud-storage-management repo and upload the two JPG files

    gsutil cp *.jpg gs://ace-inc-dominick
    Copying file://pinkelephant-bowtie.jpg [Content-Type=image/jpeg]...
    Copying file://plaid-bowtie.jpg [Content-Type=image/jpeg]...                    
    - [2 files][157.1 KiB/157.1 KiB]                                                
    Operation completed over 2 objects/157.1 KiB.

Make the bucket public and add allUsers to the principles. Set the role to Storage Object Viewer. Access a file

Remove public permissions.

Add an ACL to an object, this will allow you to create a public URL while keeping the bucket secure.

Delete your buckets.

## Cloud SQL

Cloud DB offering fully managed cloud native relational database DBaaS. RDBMS. Low latency, transctional, and relational db workoad. Cloud SQL offers fully managed relational databases, including MySQL, PostgreSQL, and SQL Server as a service.

On demand and automatic backups, point in time recovery, 30TB storage capacity, automatic storage increase, ecnryyption at rest and in transit. Only available in shared, standardd, and high mem instance rtypes or db-n1,f1. HDD or SDD 30 TB available. 

Can configure with public or privatet IP to Cloud SQL. Can authenticate via cloud sql proxy using IAM permissions. Only allowed if it comes from an authorized network or range. Cloud SQL can be used with App Engine using standard drivers like connector J for java or MySQL DB for Python. 

Cloud SQL Proxy validates permissions and wraps the connection in a TSL tunnel. Acts as intermediary server and then passes them to cloud sql instance. Only create outgoing on port 3307.

## Cloud Spanner 

Global Relational DB, fully managed relational db. Dbaas, supports schemas, ACID transcations and sql queries. Globally distributed, handles replicas and sharding. Battle tested by Google’s own mission-critical applications and services, Spanner is the service that powers Google’s $80 billion business. Cloud Spanner is especially suited for applications that require: A SQL relational database management system with joins and secondary indexes Built-in high availability Strong global consistency And high numbers of input and output operations per second. We’re talking tens of thousands of reads and writes per second

Sync data replication is automatic. Data layer encryption, audit logging, and IAM integrations

Mainly for global supplpy chain, gaminig, ad tech, and retail. 

## NoSQL Databases

<b>Cloud Big Table</b> - PB and TB workoads. Built for real time app serving and large scale workloads. Regional service. Fully managed, wide column no sql database. Store large amount of single keyed data. Used for graph data, time series, marketting, financial data analysis, IoT data, stock prices and is expensive.

It's the same database that powers many core Google services including search, analytics, maps, and Gmail. 

<b>Cloud Data Store</b> - Fully managed highly scaleable NoSQL document db for automatic scaling and hih performance. Uses GQL instead of SQL, atomic transcations. Encrypted at rest. FIRESTORE is the newest version of datastore. Datastore emulator allows you to test apps locally. Product cataolgs, user profiles, transfering fund from one bank to another. 

<b>Firestore or Firebase</b> - With Firestore, data is stored in documents and then organized into collections. Documents can contain complex nested objects in addition to subcollections. It caches data that an app is actively using, so the app can write, read, listen to, and query data even if the device is offline. When the device comes back online, Firestore synchronizes any local changes back to Firestore.  Flexible, scalable NoSQL database to store and sync data for client and server side development. Collections have documents that contain data to build data structures. Serverless, milti region, expressive queriying, data sync to update real time data. Caches data as well for offline support. Free tier. 

Firebase is a movile app develpment platform that provides tols and cloud service to enable developers to develp apps faster and easily. Twitter could use firebase to store tweets, likes, and othe metadata. 

<b>Memory Store</b> - Redis or Memcahced in memory data to build app caches. HA, fully managed, scale as needed. Secure using VPC and private IPs. Caching, gaming leaders board and user profiles, stream processing. 

## Big Data Services

Big data is basically data too expenssive to store. 

using this big data makes companies successful to gain insight, increase revenue, improve operations, and utilize machine learning. 

BigQuery - managed service warehouse at petabyte scale. Ingest data or stream it for real time analytics. Support automatic backups and HA, Standard SQL. Data governance, IAM and VPC mgmt, geo expansion.

Pub/Sub - realtime messagin service between applications. Publisher creates messages and send them to messaging service. Message is sent to a topic and message storage. Then forwards that subscription and subscribers. its good for event notifcations.

Composer - workflow orchestration service that uses airflow to author schedule and manage workflows. Uses DAGs to executs collection of tasks you want to schedule and run. 

Dataflow - is a serverless processing service for executing apache beam pipeline for batch and realtime data streaming. 

Dataproc - run spark hadoop hive, or pig in the cloud. 

Cloud data lab - interactive data tool to explore, analyze, visualization, and machine learning. Similar to sage maker. 

# Monitoring

Latency - Latency measures how long it takes a particular part of a system to return a result. Latency is important because it directly affects the user experience. Changes in latency could indicate emerging issues, its values may be tied to capacity demands, and it can be used to measure system improvements.

Traffic - measures how many requests are reaching your system. Traffic is important because it's an indicator of current system demand, its historical trends are used for capacity planning, and it's a core measure when calculating infrastructure spend. 

Saturation - measures how close to capacity a system is. It's important to note though, that capacity is often a subjective measure that depends on the underlying service or application. Saturation is important because it's an indicator of how full the service is. It focuses on the most constrained resources, and it's frequently tied to degrading performance as capacity is reached. 

Error - Errors are often raised when a flaw, failure or faults in a computer program or system causes it to produce incorrect or unexpected results or behave in unintended ways. Errors are important because they may indicate that something is failing, they may indicate configuration or capacity issues.

SLI - Service level indicators carefully selected monitoring metrics that measure one aspect of a services reliability. SLI should have a close linear relationship with your users experience of that reliability. We recommend expressing them as the ratio of two numbers, the number of good events divided by the count of all valid events.

SLO - Service level objective combines a service level indicator with a target reliability. If you express your SLIs as is commonly recommended, your SLOs will generally be somewhere just short of 100 percent. For example, 99.9 or three nines. Should be obtinable and achievable.

SLA - Service level agreement commitments made to your customers that your systems and applications will only have a certain amount of downtime. An SLA describes the minimum levels of service that you promised to provide to your customers and what happens when you break that promise. If your service has paying customers, an SLA might include some way of compensating them with refunds or credits when that service has an outage that is longer than this agreement allows.

