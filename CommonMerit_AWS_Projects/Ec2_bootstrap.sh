#!/bin/bash
# User data script
# Install httpd linux version 2

yum update -y
yum install -y httpd
systemctl start httpd
systemctl enable httpd
echo "<h1>Everything installed on $(hostname -f)</h1>" > /var/www/html/index.html