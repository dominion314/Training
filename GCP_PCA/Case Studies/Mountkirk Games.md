# Company Overview 

Mountkirk Games makes online, sessionbased, multiplayer games for the most popular mobile platforms. They build all of their games using some serverside integration. Historically, they have used cloud providers to lease physical servers.

Due to the unexpected popularity of some of their games, they have had problems scaling their global audience, application servers MySQL databases, and analytics tools.

Their current model is to write game statistics to files and send them through an ETL tool that loads them into a centralized MySQL database for reporting.

## Solution Concept 

Mountkirk Games is building a new game, which they expect to be very popular. They plan to deploy the game's backend on Google Compute Engine so they can capture streaming metrics run intensive analytics, and take advantage of its autoscaling server environment and integrate with a managed NoSQL database.

## Business Requirements 

Increase to a global footprint
Improve uptime as downtime is a loss of players
Increase efficiency of the cloud resources we use
Reduce latency to all customers

## Technical Requirements 

Requirements for Game Backend Platform
1. Dynamically scale up or down based on game activity
2. Connect to a managed NoSQL database service
3. Run customize Linux distro
   
Requirements for Game Analytics Platform
1. Dynamically scale up or down based on game activity
2. Process incoming data on the fly directly from the game servers
3. Process data that arrives late because of slow mobile networks
4. Allow SQL queries to access at least 10 TB of historical data
5. Process files that are regularly uploaded by users' mobile devices
6. Use only fully managed services

## CEO Statement 

Our last successful game did not scale well with our previous cloud provider, resulting in lower user adoption and affecting the game's reputation. Our investors want more key performance indicators (KPIs) to evaluate the speed and stability of the game, as well as other metrics that provide deeper insight into usage patterns so we can adapt the game to target users.

## CTO Statement 

Our current technology stack cannot provide the scale we need, so we want to replace MySQL and move to an environment that provides autoscaling, low latency load balancing, and frees us up from managing physical servers.

## CFO Statement 

We are not capturing enough user demographic data, usage metrics, and other KPIs. As a result, we do not engage the right users, we are not confident that our marketing is targeting the right users, and we are not selling enough premium BlastUps inside the games, which dramatically impacts our revenue.

# Questions

## Question 1 

Mountkirk Games wants you to design their new testing strategy. How should the test coverage differ from their existing backends on the other platforms?

A. Tests should scale well beyond the prior approaches

B. Unit tests are no longer required, only end-to-end tests

C. Tests should be applied after the release is in the production environment

D. Tests should include directly testing the Google Cloud Platform (GCP) infrastructure

    I'd pick A. 
    
    One of the technical requirements says "Dynamically scale up or down based on game activity". In addition, most testers would write black box or grey box test cases without knowing the cloud infrastructure, nor how GCP services are built. In addition, how do you write test cases which 'directly test GCP products'?

## Question 2

Mountkirk Games has deployed their new backend on Google Cloud Platform (GCP). You want to create a through testing process for new versions of the backend before they are released to the public. You want the testing environment to scale in an economical way. How should you design the process?

A. Create a scalable environment in GCP for simulating production load

B. Use the existing infrastructure to test the GCP-based backend at scale

C. Build stress tests into each component of your application using  resources internal to GCP to simulate load

D. Create a set of static environments in GCP to test different levels of load for example, high, medium, and low

      A is correct because simulating production load in GCP can scale in an economical way.

      B is not correct because one of the pain points about the existing infrastructure was precisely that the environment did not scale well. 
      
      C is not correct because it is a best practice to have a clear separation between test and production environments. Generating test load should not be done from a production environment. 
      
      D is not correct because Mountkirk Games wants the testing environment to scale as needed. Defining several static environments for specific levels of load goes against this requirement.


## Question 3

Mountkirk Games wants to set up a continuous delivery pipeline. Their architecture includes many small services that they want to be able to update and roll back quickly. Mountkirk Games has the following requirements:

- Services are deployed redundantly across multiple regions in the US and Europe
- Only frontend services are exposed on the public internet
- They can provide a single frontend IP for their fleet of services
- Deployment artifacts are immutable

Which set of products should they use?

A. Google Cloud Storage, Google Cloud Dataflow, Google Compute Engine

B. Google Cloud Storage, Google App Engine, Google Network Load Balancer

C. Google Kubernetes Registry, Google Container Engine, Google HTTP(S) Load Balancer

D. Google Cloud Functions, Google Cloud Pub/Sub, Google Cloud Deployment Manager

    
    C is correct because:
    
      Google Kubernetes Engine is ideal for deploying small services that can be updated and rolled back quickly. It is a best practice to manage services using immutable containers. -Cloud Load Balancing supports globally distributed services across multiple regions. It provides a single global IP address that can be used in DNS records. Using URL Maps, the requests can be routed to only the services that Mountkirk wants to expose. -Container Registry is a single place for a team to manage Docker images for the services.

      A is not correct because Mountkirk Games wants to set up a continuous delivery pipeline, not a data processing pipeline. Cloud Dataflow is a fully managed service for creating data processing pipelines.

      B is not correct because a Cloud Load Balancer distributes traffic to Compute Engine instances. App Engine and Cloud Load Balancer are parts of different solutions.

      D is not correct because you cannot reserve a single frontend IP for cloud functions. When deployed, an HTTP-triggered cloud function creates an endpoint with an automatically assigned IP.

## Question 4

Mountkirk Games' gaming servers are not automatically scaling properly. Last month, they rolled out a new feature, which suddenly became very popular. A record number of users are trying to use the service, but many of them are getting 503 errors and very slow response times. What should they investigate first?

A. Verify that the database is online

B. Verify that the project quota hasn't been exceeded

C. Verify that the new feature code did not introduce any performance bugs

D. Verify that the load-testing team is not running their tool against production

    B is the correct answer

      Error code starting like 5xx is something related to server
      503 UNAVAILABLE Service unavailable. Typically the server is down.

## Question 5 

Mountkirk Games needs to create a repeatable and configurable mechanism for deploying isolated application environments. Developers and testers can access each other's environments and resources, but they cannot access staging or production resources. The staging environment needs access to some services from production. What should you do to isolate development environments from staging and production?

A. Create a project for development and test and another for staging and production

B. Create a network for development and test and another for staging and production

C. Create one subnetwork for development and another for staging and production

D. Create one project for development, a second for staging and a third for production

      D

      In the requirement, the staging environment needs access to production, not the other way around. Answer A could allow staging and production to access each other. In answer D, staging and production are in different project, you can limit the access from either side. So D is correct.

      https://cloud.google.com/appengine/docs/standard/php/creating-separate-dev-environments


## Question 6

Mountkirk Games wants to set up a real-time analytics platform for their new game. The new platform must meet their technical requirements. Which combination of Google technologies will meet all of their requirements?

A. Kubernetes Engine, Cloud Pub/Sub, and Cloud SQL

B. Cloud Dataflow, Cloud Storage, Cloud Pub/Sub, and BigQuery

C. Cloud SQL, Cloud Storage, Cloud Pub/Sub, and Cloud Dataflow

D. Cloud Dataproc, Cloud Pub/Sub, Cloud SQL, and Cloud Dataflow

E. Cloud Pub/Sub, Compute Engine, Cloud Storage, and Cloud Dataproc


      B 
      
      Ingest millions of streaming events per second from anywhere in the world with Cloud Pub/Sub, powered by Google's unique, high-speed private network. Process the streams with Cloud Dataflow to ensure reliable, exactly-once, low-latency data transformation. Stream the transformed data into BigQuery, the cloud-native data warehousing service, for immediate analysis via SQL or popular visualization tools.

      From scenario: They plan to deploy the game's backend on Google Compute Engine so they can capture streaming metrics, run intensive analytics.
      Requirements for Game Analytics Platform

      1. Dynamically scale up or down based on game activity
      2. Process incoming data on the fly directly from the game servers
      3. Process data that arrives late because of slow mobile networks
      4. Allow SQL queries to access at least 10 TB of historical data
      5. Process files that are regularly uploaded by users' mobile devices
      6. Use only fully managed services
         
      Reference:
      https://cloud.google.com/solutions/big-data/stream-analytics/

