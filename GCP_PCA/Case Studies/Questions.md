# EHR

## Question 1

 You are responsible for ensuring that EHR's use of Google Cloud will pass an upcoming privacy compliance audit. What should you do? (Choose two.)

A. Verify EHR's product usage against the list of compliant products on the Google Cloud compliance page.

B. Advise EHR to execute a Business Associate Agreement (BAA) with Google Cloud.

C. Use Firebase Authentication for EHR's user facing applications.

D. Implement Prometheus to detect and prevent security breaches on EHR's web-based applications.

E. Use GKE private clusters for all Kubernetes workloads.

      AB

      A - OK (Google Cloud compliance page will give list of products those are HIPAA compliant https://cloud.google.com/security/compliance/offerings?skip_cache=true#/regions=USA&industries=Healthcare_and_life_sciences&focusArea=Privacy)

      B - OK (BAA means HIPAA Business Associate amendment or Business Associate Agreement entered into between Google and Customer. With EHR being a leading provider of health record software, this agreement is required. https://cloud.google.com/files/gcp-hipaa-overview-guide.pdf?hl=en)

      C - Eliminated (Firebase authentication provides backend services, easy-to-use SDKs and ready-made libraries to users on App. https://firebase.google.com/docs/auth)

      D - Eliminated (more of an observability platform)

      E - Eliminated (Running distributed services in GKE private clusters gives enterprises both secure and reliable services. Not sure how this may help with Private Compliance Audit)

## Questions 2 

 You need to define the technical architecture for securely deploying workloads to Google Cloud. You also need to ensure that only verified containers are deployed using Google Cloud services. What should you do? (Choose two.)

A. Enable Binary Authorization on GKE, and sign containers as part of a CI/CD pipeline.

B. Configure Jenkins to utilize Kritis to cryptographically sign a container as part of a CI/CD pipeline.

C. Configure Container Registry to only allow trusted service accounts to create and deploy containers from the registry.

D. Configure Container Registry to use vulnerability scanning to confirm that there are no vulnerabilities before deploying the workload.

    A & D

    Binary Authorization to ensure only verified containers are deployed
    To ensure deployment are secure and and consistent, automatically scan images for vulnerabilities with container analysis (https://cloud.google.com/docs/ci-cd/overview?hl=en&skip_cache=true)

## Question 3

You need to upgrade the EHR connection to comply with their requirements. The new connection design must support business-critical needs and meet the same network and security policy requirements. What should you do?

A. Add a new Dedicated Interconnect connection.

B. Upgrade the bandwidth on the Dedicated Interconnect connection to 100 G.

C. Add three new Cloud VPN connections.

D. Add a new Carrier Peering connection.

        A ; 99.9% availability requires 2 interconnect. 
        
        https://cloud.google.com/network-connectivity/docs/interconnect/how-to/dedicated/modifying-interconnects says

        " It is not possible to change the link type on an Interconnect connection circuit from 10 Gbps to 100 Gbps. If you want to migrate to 100 Gbps, you must first provision a new 100-Gbps Interconnect connection alongside your existing 10-Gbps connection, and then migrate the traffic onto the 100-Gbps connection."
        

## Question 4

 You need to define the technical architecture for hybrid connectivity between EHR's on-premises systems and Google Cloud. You want to follow Google's recommended practices for production-level applications. Considering the EHR Healthcare business and technical requirements, what should you do?

A. Configure two Partner Interconnect connections in one metro (City), and make sure the Interconnect connections are placed in different metro zones.

B. Configure two VPN connections from on-premises to Google Cloud, and make sure the VPN devices on-premises are in separate racks.

C. Configure Direct Peering between EHR Healthcare and Google Cloud, and make sure you are peering at least two Google locations.

D. Configure two Dedicated Interconnect connections in one metro (City) and two connections in another metro, and make sure the Interconnect connections are placed in different metro zones.

        D (based on the requirement of secure and high-performance connection between on-premises systems to Google Cloud)

        Between A and D, picked D as with Direct Connect EHR can get the bandwidth of 10 GBS to 100GBS (VPN ruled out as traffic is over internet and due to bandwidth. Direct Peering is more for Workspace rather than Google Cloud)

        "Google's recommended practices for production-level applications" 
        
        https://cloud.google.com/network-connectivity/docs/interconnect/tutorials/production-level-overview 
        
         https://cloud.google.com/network-connectivity/docs/interconnect/tutorials/non-critical-overview. 
        
        It is clear answer should be D , which is topology for production level applications recommended by Google


## Question 5 

 You are a developer on the EHR customer portal team. Your team recently migrated the customer portal application to Google Cloud. The load has increased on the application servers, and now the application is logging many timeout errors. You recently incorporated Pub/Sub into the application architecture, and the application is not logging any Pub/Sub publishing errors. You want to improve publishing latency. What should you do?

A. Increase the Pub/Sub Total Timeout retry value.

B. Move from a Pub/Sub subscriber pull model to a push model.

C. Turn off Pub/Sub message batching.

D. Create a backup Pub/Sub message queue.

    C - The cost of batching is latency for individual messages, which are queued in memory until their corresponding batch is filled and ready to be sent over the network. To minimize latency, batching should be turned off.

    https://cloud.google.com/pubsub/docs/publisher?hl=en#batching

    A incorrect. Application timeout because of publisher latency, nothing to do with timeout retry with publish request.

    D does not make sense at all.

    B is about receiver, not publisher.

## Question 6

 In the past, configuration errors put public IP addresses on backend servers that should not have been accessible from the Internet. You need to ensure that no one can put external IP addresses on backend Compute Engine instances and that external IP addresses can only be configured on frontend Compute Engine instances. What should you do?

A. Create an Organizational Policy with a constraint to allow external IP addresses only on the frontend Compute Engine instances.

B. Revoke the compute.networkAdmin role from all users in the project with front end instances.

C. Create an Identity and Access Management (IAM) policy that maps the IT staff to the compute.networkAdmin role for the organization.

D. Create a custom Identity and Access Management (IAM) role named GCE_FRONTEND with the compute.addresses.create permission.


    It's A.

    Following is from Google page - https://cloud.google.com/blog/ja/products/identity-security/limiting-public-ips-google-cloud

    "Using an Organization Policy, you can restrict external IP addresses to specific VMs with constraints to control use of external IP addresses for your VM instances within an organization or a project."

## Question 7

You are responsible for designing the Google Cloud network architecture for Google Kubernetes Engine. You want to follow Google best practices. Considering the EHR Healthcare business and technical requirements, what should you do to reduce the attack surface?

A. Use a private cluster with a private endpoint with master authorized networks configured.

B. Use a public cluster with firewall rules and Virtual Private Cloud (VPC) routes.

C. Use a private cluster with a public endpoint with master authorized networks configured.

D. Use a public cluster with master authorized networks enabled and firewall rules.


    It should be A.

    Public endpoint access disabled is the most secure option as it prevents all internet access to the control plane. This is a good choice if you have configured your on-premises network to connect to Google Cloud using Cloud Interconnect (EHR has enabled this) or Cloud VPN.

    If you disable public endpoint access, then you must configure authorized networks for the private endpoint. If you don't do this, you can only connect to the private endpoint from cluster nodes or VMs in the same subnet as the cluster.

    Public endpoint access enabled, authorized networks enabled: This is a good choice if you need to administer the cluster from source networks that are not connected to your cluster's VPC network using Cloud Interconnect or Cloud VPN (but EHR is already using interconnect) So answer C is wrong.
    
    Reference- https://cloud.google.com/kubernetes-engine/docs/concepts/private-cluster-concept



# Terraearth




## Question 1

TerraEarth's CTO wants to use the raw data from connected vehicles to help identify approximately when a vehicle in the field will have a catastrophic failure. You want to allow analysts to centrally query the vehicle data. Which architecture should you recommend?

    A is correct

    As described in the Designing a Connected Vehicle Platform on Cloud IoT Core case study,

    1. Google Cloud Dataflow is essential to transform, enrich and then store telemetry data by using distributed data pipelines

    2. Cloud Pub/Sub is essential to handle the streams of vehicle data while at the same time decoupling the specifics of the backend processing implementation



    It now comes down to a choice between

    1. Cloud SQL vs BigQuery for analytics.

    2. GKE (with or without Anthos) + Cloud Load balancing vs App Engine.



    For the first point, there is no doubt that BigQuery is the preferred choice for analytics. Cloud SQL does not scale to this sort of data volume (9TB/day + data coming through when vehicles are serviced).

    For the second point, GKE with Cloud Load Balancing is a better fit than App Engine. App Engine is a regional service whereas, with the other option, you can have multiple GKE clusters in different regions. And Cloud Load Balancing can send requests to the cluster in the region that is closest to the vehicle. This option minimizes the latency and makes the feedback loop more real-time.

    The push endpoint can be a load balancer.
    A container cluster can be used.
    Cloud Pub/Sub for Stream Analytics

    https://cloud.google.com/pubsub/

    https://cloud.google.com/solutions/iot/

    https://cloud.google.com/solutions/designing-connected-vehicle-platform 
    
    https://cloud.google.com/solutions/designing-connected-vehicle-platform#data_ingestion 
    
    http://www.eweek.com/big-data-and-analytics/google-touts-value-of-cloud-iot-core-for-analyzing-connected-car-data
    
    https://cloud.google.com/solutions/iot/



## Question 2 


The TerraEarth development team wants to create an API to meet the company's business requirements. You want the development team to focus their development effort on business value versus creating a custom framework. Which method should they use?

A. Use Google App Engine with Google Cloud Endpoints. Focus on an API for dealers and partners

B. Use Google App Engine with a JAX-RS Jersey Java-based framework. Focus on an API for the public

C. Use Google App Engine with the Swagger (Open API Specification) framework. Focus on an API for the public

D. Use Google Container Engine with a Django Python container. Focus on an API for the public

E. Use Google Container Engine with a Tomcat container with the Swagger (Open API Specification) framework. Focus on an API for dealers and partners

    A 

    Develop, deploy, protect and monitor your APIs with Google Cloud Endpoints. Using an Open API Specification or one of our API frameworks, Cloud Endpoints gives you the tools you need for every phase of API development.
    From scenario:

    Business Requirements 

    Decrease unplanned vehicle downtime to less than 1 week, without increasing the cost of carrying surplus inventory
    Support the dealer network with more data on how their customers use their equipment to better position new products and services
    Have the ability to partner with different companies  especially with seed and fertilizer suppliers in the fast-growing agricultural business to create compelling joint offerings for their customers.

    Reference:
    https://cloud.google.com/certification/guides/cloud-architect/casestudy-Terramearth


## Question 3

Your development team has created a structured API to retrieve vehicle data. They want to allow third parties to develop tools for dealerships that use this vehicle event data. You want to support delegated authorization against this data. What should you do?

A. Build or leverage an OAuth-compatible access control system

B. Build SAML 2.0 SSO compatibility into your authentication system

C. Restrict data access based on the source IP address of the partner systems

D. Create secondary credentials for each dealer that can be given to the trusted third party

    A

    SAML is an authentication system.
    OAuth is an authorization system.

    Both can be used with SSO (Single sign on). SAML is for users and OAuth is more for applications.

    https://cloud.google.com/docs/authentication


## Question 4

TerraEarth plans to connect all 20 million vehicles in the field to the cloud. This increases the volume to 20 million 600 byte records a second for 40 TB an hour. How should you design the data ingestion?

A. Vehicles write data directly to GCS

B. Vehicles write data directly to Google Cloud Pub/Sub

C. Vehicles stream data directly to Google BigQuery

D. Vehicles continue to write data using the existing system (FTP)

    Its B

    it exceeds the streaming limit for BQ. We need to buffer, the default limit of BigQuery is 100 API calls per second, till now this cannot be changed. 


## Question 5

You analyzed TerraEarth's business requirement to reduce downtime, and found that they can achieve a majority of time saving by reducing customer's wait time for parts. You decided to focus on reduction of the 3 weeks aggregate reporting time. Which modifications to the company's processes should you recommend?

A. Migrate from CSV to binary format, migrate from FTP to SFTP transport, and develop machine learning analysis of metrics

B. Migrate from FTP to streaming transport, migrate from CSV to binary format, and develop machine learning analysis of metrics

C. Increase fleet cellular connectivity to 80%, migrate from FTP to streaming transport, and develop machine learning analysis of metrics

D. Migrate from FTP to SFTP transport, develop machine learning analysis of metrics, and increase dealer local inventory by a fixed factor

    C

    A is not correct because machine learning analysis is a good means toward the end of reducing downtime, but shuffling formats and transport doesn't directly help at all.

    B is not correct because machine learning analysis is a good means toward the end of reducing downtime, and moving to streaming can improve the freshness of the information in that analysis, but changing the format doesn't directly help at all.

    C is correct because using cellular connectivity will greatly improve the freshness of data used for analysis from where it is now, collected when the machines are in for maintenance. Streaming transport instead of periodic FTP will tighten the feedback loop even more. Machine learning is ideal for predictive maintenance workloads.

    D is not correct because machine learning analysis is a good means toward the end of reducing downtime, but the rest of these changes don't directly help at all.


## Question 6

Which of TerraEarth's legacy enterprise processes will experience significant change as a result of increased Google Cloud Platform adoption?

A. Opex/capex allocation, LAN changes, capacity planning

B. Capacity planning, TCO calculations, opex/capex allocation

C. Capacity planning, utilization measurement, data center expansion

D. Data Center expansion, TCO calculations, utilization measurement

    Correct Answer B

    Capacity planning, TCO calculations, opex/capex allocation

    From the case study, it can conclude that Management (CXO) all concern rapid provision of resources (infrastructure) for growing as well as cost management, such as Cost optimization in Infrastructure, trade up front capital expenditures (Capex) for ongoing operating expenditures (Opex), and Total cost of ownership (TCO)

## Question 7 

To speed up data retrieval, more vehicles will be upgraded to cellular connections and be able to transmit data to the ETL process. The current FTP process is error-prone and restarts the data transfer from the start of the file when connections fail, which happens often. You want to improve the reliability of the solution and minimize data transfer time on the cellular connections. What should you do?

A. Use one Google Container Engine cluster of FTP servers. Save the data to a Multi-Regional bucket. Run the ETL process using data in the bucket

B. Use multiple Google Container Engine clusters running FTP servers located in different regions. Save the data to Multi-Regional buckets in US, EU, and Asia. Run the ETL process using the data in the bucket

C. Directly transfer the files to different Google Cloud Multi-Regional Storage bucket locations in US, EU, and Asia using Google APIs over HTTP(S). Run the ETL process using the data in the bucket

D. Directly transfer the files to a different Google Cloud Regional Storage bucket location in US, EU, and Asia using Google APIs over HTTP(S). Run the ETL process to retrieve the data from each Regional bucket

    D seems to be the correct answer
    Sending Data to all Multi Region Buckets >> Incurs more cost , More Latency
    Sending Data only to Regional Bucket >> Incurs less cost , Less Latency

    https://cloud.google.com/storage/docs/locations#considerations
    Regional: 200 Gbps (per region, per project)
    Multi-regional: 50 Gbps (per region, per project)

## Question 8 

TerraEarth's 20 million vehicles are scattered around the world. Based on the vehicle's location, its telemetry data is stored in a Google Cloud Storage (GCS) regional bucket (US, Europe, or Asia). The CTO has asked you to run a report on the raw telemetry data to determine why vehicles are breaking down after 100 K miles. You want to run this job on all the data. What is the most cost-effective way to run this job?

A. Move all the data into 1 zone, then launch a Cloud Dataproc cluster to run the job

B. Move all the data into 1 region, then launch a Google Cloud Dataproc cluster to run the job

C. Launch a cluster in each region to preprocess and compress the raw data, then move the data into a multi-region bucket and use a Dataproc cluster to finish the job

D. Launch a cluster in each region to preprocess and compress the raw data, then move the data into a region bucket and use a Cloud Dataproc cluster to finish the job

    D

    A, B says "move all data" but analysis will try to reveal breaking down after 100K miles so there is no point of transferring data of the vehicles with less than 100K milage.
    Therefore, transferring all data is just waste of time and money.

    If we move/copy data between continents it will cost us money therefore compressing the data before copying to another region/continent makes sense.

    Preprocessing also makes sense because we probably want to process smaller chunks of data first (remember 100K milage).
    
    So now type of target bucket; multi-region or standard? multi-region is good for high-availability and low latency with a little more cost however question doesn't require any of these features.

    Therefore I think standard storage option is good to go given lower costs are always better.

## Question 9 

TerraEarth has equipped all connected trucks with servers and sensors to collect telemetry data. Next year they want to use the data to train machine learning models. They want to store this data in the cloud while reducing costs. What should they do?

A. Have the vehicle's computer compress the data in hourly snapshots, and store it in a Google Cloud Storage (GCS) Nearline bucket

B. Push the telemetry data in real-time to a streaming dataflow job that compresses the data, and store it in Google BigQuery

C. Push the telemetry data in real-time to a streaming dataflow job that compresses the data, and store it in Cloud Bigtable

D. Have the vehicle's computer compress the data in hourly snapshots, and store it in a GCS Coldline bucket

    D is most cost effective as don't want to use until 'next year'. Therefore moving the data to coldline storage makes more sense


## Question 10 

Your agricultural division is experimenting with fully autonomous vehicles. You want your architecture to promote strong security during vehicle operation. Which two architectures should you consider? (Choose two.)

A. Treat every micro service call between modules on the vehicle as untrusted.

B. Require IPv6 for connectivity to ensure a secure address space.

C. Use a trusted platform module (TPM) and verify firmware and binaries on boot.

D. Use a functional programming language to isolate code execution cycles.

E. Use multiple connectivity subsystems for redundancy.

F. Enclose the vehicle's drive electronics in a Faraday cage to isolate chips.

    AC is correct

    A. Treat every micro service call between modules on the vehicle as untrusted.
    C. Use a trusted platform module (TPM) and verify firmware and binaries on boot.

    B is not correct because IPv6 doesn't have any impact on the security during vehicle operation, although it improves system scalability and simplicity.
    D is not correct because merely using a functional programming language doesn't guarantee a more secure level of execution isolation. Any impact on security from this decision would be incidental at best.
    E is not correct because this improves system durability, but it doesn't have any impact on the security during vehicle operation.
    F is not correct because it doesn't have any impact on the security during vehicle operation, although it improves system durability.


## Question 11

Operational parameters such as oil pressure are adjustable on each of TerraEarth's vehicles to increase their efficiency, depending on their environmental conditions. Your primary goal is to increase the operating efficiency of all 20 million cellular and unconnected vehicles in the field. How can you accomplish this goal?

A. Have you engineers inspect the data for patterns, and then create an algorithm with rules that make operational adjustments automatically

B. Capture all operating data, train machine learning models that identify ideal operations, and run locally to make operational adjustments automatically

C. Implement a Google Cloud Dataflow streaming job with a sliding window, and use Google Cloud Messaging (GCM) to make operational adjustments automatically

D. Capture all operating data, train machine learning models that identify ideal operations, and host in Google Cloud Machine Learning (ML) Platform to make operational adjustments automatically

    B is correct. only 200k vehicle's are connected so need to run updates locally

    Google has announced two new products aimed at helping customers develop and deploy intelligent connected devices at scale: Edge TPU, a new hardware chip, and Cloud IoT Edge, a software stack that extends Google Cloud’s powerful AI capability to gateways and connected devices. This lets you build and train ML models in the cloud, then run those models on the Cloud IoT Edge device through the power of the Edge TPU hardware accelerator.

    - https://cloud.google.com/blog/products/gcp/bringing-intelligence-edge-cloud-iot



# Helicopter Racing League

## Question 1

Your team is in charge of creating a payment card data vault for card numbers used to bill tens of thousands of viewers, merchandise consumers, and season ticket holders. You need to implement a custom card tokenization service that meets the following requirements:

* It must provide low latency at minimal cost.
* It must be able to identify duplicate credit cards and must not store plaintext card numbers.
* It should support annual key rotation.
  
Which storage approach should you adopt for your tokenization service?

A. Store the card data in Secret Manager after running a query to identify duplicates.

B. Encrypt the card data with a deterministic algorithm stored in Firestore using Datastore mode.

C. Encrypt the card data with a deterministic algorithm and shard it across multiple Memorystore instances.

D. Use column-level encryption to store the data in Cloud SQL.

    Answer would be B

    https://cloud.google.com/community/tutorials/pci-tokenizer

    Deterministic output means that a given set of inputs (card number, expiration, and userID) will always generate the same token. This is useful if you want to rely on the token value to deduplicate your token stores. You can simply match a newly generated token to your existing catalog of tokens to determine whether the card has been previously stored. Depending on your application architecture, this can be a very useful feature. However, this could also be accomplished using a salted hash of the input values.


## Question 2

Recently HRL started a new regional racing league in Cape Town, South Africa. In an effort to give customers in Cape Town a better user experience, HRL has partnered with the Content Delivery Network provider, Fastly. HRL needs to allow traffic coming from all of the Fastly IP address ranges into their Virtual Private Cloud network (VPC network). You are a member of the HRL security team and you need to configure the update that will allow only the Fastly IP address ranges through the External HTTP(S) load balancer. Which command should you use?

A. gcloud compute security-policies rules update 1000 \
        --security-policy from-fastly \
        --src-ip-ranges * \
        --action "allow"

B. gcloud compute firewall rules update sourceiplist-fastly \
        --priority 1000 \
        --allow tcp:443

C.  gcloud compute firewall rules update sourceiplist-fastly \
        --priority 1000 \
        --target-tags=sourceiplist-fastly \
        --allow tcp:43

D.  gcloud compute security-policies rules update 1000 \
        --security-policy hlr-policy \
        --expression "evaluatePreconfiguredExpr('sourceiplist-fastly')" \
        --action "allow"

    The answer is D, for 2 reasons:

    1. They want to apply a firewall rule at HTTPS LB level. This can be done only with cloud armor. Cloud armor does not work with classic firewall; it works with security policies.
   
    2. D is the only answer that points to a list of IPs to whitelist, via preconfigured expression. The others will open to too broad IPs.


## Question 3

The HRL development team releases a new version of their predictive capability application every Tuesday evening at 3 a.m. UTC to a repository. The security team at HRL has developed an in-house penetration test Cloud Function called Airwolf. The security team wants to run Airwolf against the predictive capability application as soon as it is released every Tuesday. You need to set up Airwolf to run at the recurring weekly cadence. What should you do?

A. Set up Cloud Tasks and a Cloud Storage bucket that triggers a Cloud Function.

B. Set up a Cloud Logging sink and a Cloud Storage bucket that triggers a Cloud Function.

C. Configure the deployment job to notify a Pub/Sub queue that triggers a Cloud Function.

D. Set up Identity and Access Management (IAM) and Confidential Computing to trigger a Cloud Function.

    Answer C 

    Triggering Pub/Sub to invoke Cloud Functions seems to be relevant. Cloud Storage doesn't make any sense. It would have been straight forward if Cloud Scheduler is mentioned in Option C instead of Deployment Job. But if you make a bit of research on deployment jobs, it's pointing me to cron jobs which is making perfect sense.

    https://cloud.google.com/appengine/docs/flexible/nodejs/scheduling-jobs-with-cron-yaml

    https://cloud.google.com/scheduler/docs/tut-pub-sub


## Question 4

HRL wants better prediction accuracy from their ML prediction models. They want you to use Google's AI Platform so HRL can understand and interpret the predictions. What should you do?

A. Use Explainable AI.

B. Use Vision AI.

C. Use Google Cloud's operations suite.

D. Use Jupyter Notebooks.

    Answer A

    AI Explanations helps you understand your model's outputs for classification and regression tasks. Whenever you request a prediction on AI Platform, AI Explanations tells you how much each feature in the data contributed to the predicted result. You can then use this information to verify that the model is behaving as expected, recognize bias in your models, and get ideas for ways to improve your model and your training data.

    https://cloud.google.com/ai-platform/prediction/docs/ai-explanations/overview

## Question 5

HRL is looking for a cost-effective approach for storing their race data such as telemetry. They want to keep all historical records, train models using only the previous season's data, and plan for data growth in terms of volume and information collected. You need to propose a data solution. Considering HRL business requirements and the goals expressed by CEO S. Hawke, what should you do?

A. Use Firestore for its scalable and flexible document-based database. Use collections to aggregate race data by season and event.

B. Use Cloud Spanner for its scalability and ability to version schemas with zero downtime. Split race data using season as a primary key.

C. Use BigQuery for its scalability and ability to add columns to a schema. Partition race data based on season.

D. Use Cloud SQL for its ability to automatically manage storage increases and compatibility with MySQL. Use separate database instances for each season.

    C. Use BigQuery for its scalability and ability to add columns to a schema. Partition race data based on season.

    https://cloud.google.com/architecture/mobile-gaming-analysis-telemetry


## Question 6

A recent finance audit of cloud infrastructure noted an exceptionally high number of Compute Engine instances are allocated to do video encoding and transcoding. You suspect that these Virtual Machines are zombie machines that were not deleted after their workloads completed. You need to quickly get a list of which VM instances are idle. What should you do?

A. Log into each Compute Engine instance and collect disk, CPU, memory, and network usage statistics for analysis.

B. Use the gcloud compute instances list to list the virtual machine instances that have the idle: true label set.

C. Use the gcloud recommender command to list the idle virtual machine instances.

D. From the Google Console, identify which Compute Engine instances in the managed instance groups are no longer responding to health check probes.

    C is the Correct answer
    
    Identification has to be done quickly. Manually checking each machines will take lot of time. Moreover--- even option A says "CPUs" and not GPUs

    "Log into each Compute Engine instance and collect disk, CPU, memory, and network usage statistics for analysis."

    https://cloud.google.com/compute/docs/instances/viewing-and-applying-idle-vm-recommendations


# Mountkirk Games

## Question 1 

Mountkirk Games wants you to design their new testing strategy. How should the test coverage differ from their existing backends on the other platforms?

A. Tests should scale well beyond the prior approaches

B. Unit tests are no longer required, only end-to-end tests

C. Tests should be applied after the release is in the production environment

D. Tests should include directly testing the Google Cloud Platform (GCP) infrastructure

    I'd pick A. 
    
    One of the technical requirements says "Dynamically scale up or down based on game activity". In addition, most testers would write black box or grey box test cases without knowing the cloud infrastructure, nor how GCP services are built. In addition, how do you write test cases which 'directly test GCP products'?

## Question 2

Mountkirk Games has deployed their new backend on Google Cloud Platform (GCP). You want to create a through testing process for new versions of the backend before they are released to the public. You want the testing environment to scale in an economical way. How should you design the process?

A. Create a scalable environment in GCP for simulating production load

B. Use the existing infrastructure to test the GCP-based backend at scale

C. Build stress tests into each component of your application using  resources internal to GCP to simulate load

D. Create a set of static environments in GCP to test different levels of load for example, high, medium, and low

      A is correct because simulating production load in GCP can scale in an economical way.

      B is not correct because one of the pain points about the existing infrastructure was precisely that the environment did not scale well. 
      
      C is not correct because it is a best practice to have a clear separation between test and production environments. Generating test load should not be done from a production environment. 
      
      D is not correct because Mountkirk Games wants the testing environment to scale as needed. Defining several static environments for specific levels of load goes against this requirement.


## Question 3

Mountkirk Games wants to set up a continuous delivery pipeline. Their architecture includes many small services that they want to be able to update and roll back quickly. Mountkirk Games has the following requirements:

- Services are deployed redundantly across multiple regions in the US and Europe
- Only frontend services are exposed on the public internet
- They can provide a single frontend IP for their fleet of services
- Deployment artifacts are immutable

Which set of products should they use?

A. Google Cloud Storage, Google Cloud Dataflow, Google Compute Engine

B. Google Cloud Storage, Google App Engine, Google Network Load Balancer

C. Google Kubernetes Registry, Google Container Engine, Google HTTP(S) Load Balancer

D. Google Cloud Functions, Google Cloud Pub/Sub, Google Cloud Deployment Manager

    
    C is correct because:
    
      Google Kubernetes Engine is ideal for deploying small services that can be updated and rolled back quickly. It is a best practice to manage services using immutable containers. -Cloud Load Balancing supports globally distributed services across multiple regions. It provides a single global IP address that can be used in DNS records. Using URL Maps, the requests can be routed to only the services that Mountkirk wants to expose. -Container Registry is a single place for a team to manage Docker images for the services.

      A is not correct because Mountkirk Games wants to set up a continuous delivery pipeline, not a data processing pipeline. Cloud Dataflow is a fully managed service for creating data processing pipelines.

      B is not correct because a Cloud Load Balancer distributes traffic to Compute Engine instances. App Engine and Cloud Load Balancer are parts of different solutions.

      D is not correct because you cannot reserve a single frontend IP for cloud functions. When deployed, an HTTP-triggered cloud function creates an endpoint with an automatically assigned IP.

## Question 4

Mountkirk Games' gaming servers are not automatically scaling properly. Last month, they rolled out a new feature, which suddenly became very popular. A record number of users are trying to use the service, but many of them are getting 503 errors and very slow response times. What should they investigate first?

A. Verify that the database is online

B. Verify that the project quota hasn't been exceeded

C. Verify that the new feature code did not introduce any performance bugs

D. Verify that the load-testing team is not running their tool against production

    B is the correct answer

      Error code starting like 5xx is something related to server
      503 UNAVAILABLE Service unavailable. Typically the server is down.

## Question 5 

Mountkirk Games needs to create a repeatable and configurable mechanism for deploying isolated application environments. Developers and testers can access each other's environments and resources, but they cannot access staging or production resources. The staging environment needs access to some services from production. What should you do to isolate development environments from staging and production?

A. Create a project for development and test and another for staging and production

B. Create a network for development and test and another for staging and production

C. Create one subnetwork for development and another for staging and production

D. Create one project for development, a second for staging and a third for production

      D

      In the requirement, the staging environment needs access to production, not the other way around. Answer A could allow staging and production to access each other. In answer D, staging and production are in different project, you can limit the access from either side. So D is correct.

      https://cloud.google.com/appengine/docs/standard/php/creating-separate-dev-environments


## Question 6 

Mountkirk Games wants to set up a real-time analytics platform for their new game. The new platform must meet their technical requirements. Which combination of Google technologies will meet all of their requirements?

A. Kubernetes Engine, Cloud Pub/Sub, and Cloud SQL

B. Cloud Dataflow, Cloud Storage, Cloud Pub/Sub, and BigQuery

C. Cloud SQL, Cloud Storage, Cloud Pub/Sub, and Cloud Dataflow

D. Cloud Dataproc, Cloud Pub/Sub, Cloud SQL, and Cloud Dataflow

E. Cloud Pub/Sub, Compute Engine, Cloud Storage, and Cloud Dataproc


      B 
      
      Ingest millions of streaming events per second from anywhere in the world with Cloud Pub/Sub, powered by Google's unique, high-speed private network. Process the streams with Cloud Dataflow to ensure reliable, exactly-once, low-latency data transformation. Stream the transformed data into BigQuery, the cloud-native data warehousing service, for immediate analysis via SQL or popular visualization tools.

      From scenario: They plan to deploy the game's backend on Google Compute Engine so they can capture streaming metrics, run intensive analytics.
      Requirements for Game Analytics Platform

      1. Dynamically scale up or down based on game activity
      2. Process incoming data on the fly directly from the game servers
      3. Process data that arrives late because of slow mobile networks
      4. Allow SQL queries to access at least 10 TB of historical data
      5. Process files that are regularly uploaded by users' mobile devices
      6. Use only fully managed services
         
      Reference:
      https://cloud.google.com/solutions/big-data/stream-analytics/

