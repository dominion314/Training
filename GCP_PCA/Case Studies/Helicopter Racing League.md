# Company overview 

Helicopter Racing League (HRL) is a global sports league for competitive helicopter racing. Each year HRL holds the world championship and several regional league competitions where teams compete to earn a spot in the world championship. HRL offers a paid service to stream the races all over the world with live telemetry and predictions throughout each race.

## Solution concept 

HRL wants to migrate their existing service to a new platform to expand their use of managed AI and ML services to facilitate race predictions. Additionally, as new fans engage with the sport, particularly in emerging regions, they want to move the serving of their content, both realtime and recorded, closer to their users.

## Existing technical environment 

HRL is a public cloudfirst company; the core of their missioncritical applications runs on their current public cloud provider. Video recording and editing is performed at the race tracks, and the content is encoded and transcoded, where needed, in the cloud. Enterprisegrade connectivity and local compute is provided by truckmounted mobile data centers. Their race prediction services are hosted exclusively on their existing public cloud provider. Their existing technical environment is as follows:

Existing content is stored in an object storage service on their existing public cloud provider.
Video encoding and transcoding is performed on VMs created for each job.
Race predictions are performed using TensorFlow running on VMs in the current public cloud provider.

## Business requirements 

HRL's owners want to expand their predictive capabilities and reduce latency for their viewers in emerging markets. Their requirements are:

- Support ability to expose the predictive models to partners.
  
Increase predictive capabilities during and before races:

- Race results
- Mechanical failures
- Crowd sentiment
- Increase telemetry and create additional insights.
- Measure fan engagement with new predictions.
- Enhance global availability and quality of the broadcasts.
- Increase the number of concurrent viewers.
- Minimize operational complexity.
- Ensure compliance with regulations.
- Create a merchandising revenue stream.

## Technical requirements 

Maintain or increase prediction throughput and accuracy.
Reduce viewer latency.
Increase transcoding performance.
Create realtime analytics of viewer consumption patterns and engagement.
Create a data mart to enable processing of large volumes of race data.

## Executive statement 


Our CEO, S. Hawke, wants to bring highadrenaline racing to fans all around the world. We listen to our fans, and they want enhanced video streams that include predictions of events within the race (e.g., overtaking). Our current platform allows us to predict race outcomes but lacks the facility to support realtime predictions during races and the capacity to process seasonlong results.

# Questions

## Question 1

Your team is in charge of creating a payment card data vault for card numbers used to bill tens of thousands of viewers, merchandise consumers, and season ticket holders. You need to implement a custom card tokenization service that meets the following requirements:

* It must provide low latency at minimal cost.
* It must be able to identify duplicate credit cards and must not store plaintext card numbers.
* It should support annual key rotation.
  
Which storage approach should you adopt for your tokenization service?

A. Store the card data in Secret Manager after running a query to identify duplicates.

B. Encrypt the card data with a deterministic algorithm stored in Firestore using Datastore mode.

C. Encrypt the card data with a deterministic algorithm and shard it across multiple Memorystore instances.

D. Use column-level encryption to store the data in Cloud SQL.

    Answer would be B

    https://cloud.google.com/community/tutorials/pci-tokenizer

    Deterministic output means that a given set of inputs (card number, expiration, and userID) will always generate the same token. This is useful if you want to rely on the token value to deduplicate your token stores. You can simply match a newly generated token to your existing catalog of tokens to determine whether the card has been previously stored. Depending on your application architecture, this can be a very useful feature. However, this could also be accomplished using a salted hash of the input values.


## Question 2

Recently HRL started a new regional racing league in Cape Town, South Africa. In an effort to give customers in Cape Town a better user experience, HRL has partnered with the Content Delivery Network provider, Fastly. HRL needs to allow traffic coming from all of the Fastly IP address ranges into their Virtual Private Cloud network (VPC network). You are a member of the HRL security team and you need to configure the update that will allow only the Fastly IP address ranges through the External HTTP(S) load balancer. Which command should you use?

A. gcloud compute security-policies rules update 1000 \
        --security-policy from-fastly \
        --src-ip-ranges * \
        --action "allow"

B. gcloud compute firewall rules update sourceiplist-fastly \
        --priority 1000 \
        --allow tcp:443

C.  gcloud compute firewall rules update sourceiplist-fastly \
        --priority 1000 \
        --target-tags=sourceiplist-fastly \
        --allow tcp:43

D.  gcloud compute security-policies rules update 1000 \
        --security-policy hlr-policy \
        --expression "evaluatePreconfiguredExpr('sourceiplist-fastly')" \
        --action "allow"

    The answer is D, for 2 reasons:

    1. They want to apply a firewall rule at HTTPS LB level. This can be done only with cloud armor. Cloud armor does not work with classic firewall; it works with security policies.
   
    2. D is the only answer that points to a list of IPs to whitelist, via preconfigured expression. The others will open to too broad IPs.


## Question 3

The HRL development team releases a new version of their predictive capability application every Tuesday evening at 3 a.m. UTC to a repository. The security team at HRL has developed an in-house penetration test Cloud Function called Airwolf. The security team wants to run Airwolf against the predictive capability application as soon as it is released every Tuesday. You need to set up Airwolf to run at the recurring weekly cadence. What should you do?

A. Set up Cloud Tasks and a Cloud Storage bucket that triggers a Cloud Function.

B. Set up a Cloud Logging sink and a Cloud Storage bucket that triggers a Cloud Function.

C. Configure the deployment job to notify a Pub/Sub queue that triggers a Cloud Function.

D. Set up Identity and Access Management (IAM) and Confidential Computing to trigger a Cloud Function.

    Answer C 

    Triggering Pub/Sub to invoke Cloud Functions seems to be relevant. Cloud Storage doesn't make any sense. It would have been straight forward if Cloud Scheduler is mentioned in Option C instead of Deployment Job. But if you make a bit of research on deployment jobs, it's pointing me to cron jobs which is making perfect sense.

    https://cloud.google.com/appengine/docs/flexible/nodejs/scheduling-jobs-with-cron-yaml

    https://cloud.google.com/scheduler/docs/tut-pub-sub


## Question 4

HRL wants better prediction accuracy from their ML prediction models. They want you to use Google's AI Platform so HRL can understand and interpret the predictions. What should you do?

A. Use Explainable AI.

B. Use Vision AI.

C. Use Google Cloud's operations suite.

D. Use Jupyter Notebooks.

    Answer A

    AI Explanations helps you understand your model's outputs for classification and regression tasks. Whenever you request a prediction on AI Platform, AI Explanations tells you how much each feature in the data contributed to the predicted result. You can then use this information to verify that the model is behaving as expected, recognize bias in your models, and get ideas for ways to improve your model and your training data.

    https://cloud.google.com/ai-platform/prediction/docs/ai-explanations/overview

## Question 5

HRL is looking for a cost-effective approach for storing their race data such as telemetry. They want to keep all historical records, train models using only the previous season's data, and plan for data growth in terms of volume and information collected. You need to propose a data solution. Considering HRL business requirements and the goals expressed by CEO S. Hawke, what should you do?

A. Use Firestore for its scalable and flexible document-based database. Use collections to aggregate race data by season and event.

B. Use Cloud Spanner for its scalability and ability to version schemas with zero downtime. Split race data using season as a primary key.

C. Use BigQuery for its scalability and ability to add columns to a schema. Partition race data based on season.

D. Use Cloud SQL for its ability to automatically manage storage increases and compatibility with MySQL. Use separate database instances for each season.

    C. Use BigQuery for its scalability and ability to add columns to a schema. Partition race data based on season.

    https://cloud.google.com/architecture/mobile-gaming-analysis-telemetry


## Question 6

A recent finance audit of cloud infrastructure noted an exceptionally high number of Compute Engine instances are allocated to do video encoding and transcoding. You suspect that these Virtual Machines are zombie machines that were not deleted after their workloads completed. You need to quickly get a list of which VM instances are idle. What should you do?

A. Log into each Compute Engine instance and collect disk, CPU, memory, and network usage statistics for analysis.

B. Use the gcloud compute instances list to list the virtual machine instances that have the idle: true label set.

C. Use the gcloud recommender command to list the idle virtual machine instances.

D. From the Google Console, identify which Compute Engine instances in the managed instance groups are no longer responding to health check probes.

    C is the Correct answer
    
    Identification has to be done quickly. Manually checking each machines will take lot of time. Moreover--- even option A says "CPUs" and not GPUs

    "Log into each Compute Engine instance and collect disk, CPU, memory, and network usage statistics for analysis."

    https://cloud.google.com/compute/docs/instances/viewing-and-applying-idle-vm-recommendations

