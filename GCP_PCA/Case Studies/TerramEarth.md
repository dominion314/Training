# Company Overview 

TerraEarth manufactures heavy equipment for the mining and agricultural industries: about 80% of their business is from mining and 20% from agriculture. They currently have over 500 dealers and service centers in 100 countries. Their mission is to build products that make their customers more productive.

## Company background 

TerraEarth was formed in 1946, when several small, family owned companies combined to retool after World War II. The company cares about their employees and customers and considers them to be extended members of their family.

TerraEarth is proud of their ability to innovate on their core products and find new markets as their customers' needs change. For the past 20 years, trends in the industry have been largely toward increasing productivity by using larger vehicles with a human operator.

## Solution Concept 

There are 20 million TerraEarth vehicles in operation that collect 120 fields of data per second. Data is stored locally on the vehicle and can be accessed for analysis when a vehicle is serviced. The data is downloaded via a maintenance port. This same port can be used to adjust operational parameters, allowing the vehicles to be upgraded in the field with new computing modules.

Approximately 200,000 vehicles are connected to a cellular network, allowing TerraEarth to collect data directly. At a rate of 120 fields of data per second with 22 hours of operation per day, Terra Earth collects a total of about 9 TB/day from these connected vehicles.

## Existing Technical Environment 

<b><i>Company Machines>FTP Servers>ETL>Data Warehouse</i></b>

TerraEarth's existing architecture is composed of Linuxbased systems that reside in a data center. These systems gzip CSV files from the field and upload via FTP, transform and aggregate them, and place the data in their data warehouse. Because this process takes time, aggregated reports are based on data that is 3 weeks old.

With this data, TerraEarth has been able to preemptively stock replacement parts and reduce unplanned downtime of their vehicles by 60%. However, because the data is stale, some customers are without their vehicles for up to 4 weeks while they wait for replacement parts.

## Business Requirements 

Decrease unplanned vehicle downtime to less than 1 week, without increasing the cost of carrying surplus inventory
Support the dealer network with more data on how their customers use their equipment to better position new products and services
Have the ability to partner with different companies especially with seed and fertilizer suppliers in the fastgrowing agricultural business to create compelling joint offerings for their customers.

## CEO Statement 

We have been successful in capitalizing on the trend toward larger vehicles to increase the productivity of our customers. Technological change is occurring rapidly, and TerraEarth has taken advantage of connected devices technology to provide our customers with better services, such as our intelligent farming equipment. With this technology, we have been able to increase farmers' yields by 25%, by using past trends to adjust how our vehicles operate. These advances have led to the rapid growth of our agricultural product line, which we expect will generate 50% of our revenues by 2020.

## CTO Statement 

Our competitive advantage has always been in the manufacturing process, with our ability to build better vehicles for lower cost than our competitors. However, new products with different approaches are constantly being developed, and I'm concerned that we lack the skills to undergo the next wave of transformations in our industry. Unfortunately, our CEO doesn't take technology obsolescence seriously and he considers the many new companies in our industry to be niche players. My goals are to build our skills while addressing immediate market needs through incremental innovations.

# Questions

## Question 1

TerraEarth's CTO wants to use the raw data from connected vehicles to help identify approximately when a vehicle in the field will have a catastrophic failure. You want to allow analysts to centrally query the vehicle data. Which architecture should you recommend?

    A is correct

    As described in the Designing a Connected Vehicle Platform on Cloud IoT Core case study,

    1. Google Cloud Dataflow is essential to transform, enrich and then store telemetry data by using distributed data pipelines

    2. Cloud Pub/Sub is essential to handle the streams of vehicle data while at the same time decoupling the specifics of the backend processing implementation



    It now comes down to a choice between

    1. Cloud SQL vs BigQuery for analytics.

    2. GKE (with or without Anthos) + Cloud Load balancing vs App Engine.



    For the first point, there is no doubt that BigQuery is the preferred choice for analytics. Cloud SQL does not scale to this sort of data volume (9TB/day + data coming through when vehicles are serviced).

    For the second point, GKE with Cloud Load Balancing is a better fit than App Engine. App Engine is a regional service whereas, with the other option, you can have multiple GKE clusters in different regions. And Cloud Load Balancing can send requests to the cluster in the region that is closest to the vehicle. This option minimizes the latency and makes the feedback loop more real-time.

    The push endpoint can be a load balancer.
    A container cluster can be used.
    Cloud Pub/Sub for Stream Analytics

    https://cloud.google.com/pubsub/

    https://cloud.google.com/solutions/iot/

    https://cloud.google.com/solutions/designing-connected-vehicle-platform 
    
    https://cloud.google.com/solutions/designing-connected-vehicle-platform#data_ingestion 
    
    http://www.eweek.com/big-data-and-analytics/google-touts-value-of-cloud-iot-core-for-analyzing-connected-car-data
    
    https://cloud.google.com/solutions/iot/



## Question 2 


The TerraEarth development team wants to create an API to meet the company's business requirements. You want the development team to focus their development effort on business value versus creating a custom framework. Which method should they use?

A. Use Google App Engine with Google Cloud Endpoints. Focus on an API for dealers and partners

B. Use Google App Engine with a JAX-RS Jersey Java-based framework. Focus on an API for the public

C. Use Google App Engine with the Swagger (Open API Specification) framework. Focus on an API for the public

D. Use Google Container Engine with a Django Python container. Focus on an API for the public

E. Use Google Container Engine with a Tomcat container with the Swagger (Open API Specification) framework. Focus on an API for dealers and partners

    A 

    Develop, deploy, protect and monitor your APIs with Google Cloud Endpoints. Using an Open API Specification or one of our API frameworks, Cloud Endpoints gives you the tools you need for every phase of API development.
    From scenario:

    Business Requirements 

    Decrease unplanned vehicle downtime to less than 1 week, without increasing the cost of carrying surplus inventory
    Support the dealer network with more data on how their customers use their equipment to better position new products and services
    Have the ability to partner with different companies  especially with seed and fertilizer suppliers in the fast-growing agricultural business to create compelling joint offerings for their customers.

    Reference:
    https://cloud.google.com/certification/guides/cloud-architect/casestudy-Terramearth


## Question 3

Your development team has created a structured API to retrieve vehicle data. They want to allow third parties to develop tools for dealerships that use this vehicle event data. You want to support delegated authorization against this data. What should you do?

A. Build or leverage an OAuth-compatible access control system

B. Build SAML 2.0 SSO compatibility into your authentication system

C. Restrict data access based on the source IP address of the partner systems

D. Create secondary credentials for each dealer that can be given to the trusted third party

    A

    SAML is an authentication system.
    OAuth is an authorization system.

    Both can be used with SSO (Single sign on). SAML is for users and OAuth is more for applications.

    https://cloud.google.com/docs/authentication


## Question 4

TerraEarth plans to connect all 20 million vehicles in the field to the cloud. This increases the volume to 20 million 600 byte records a second for 40 TB an hour. How should you design the data ingestion?

A. Vehicles write data directly to GCS

B. Vehicles write data directly to Google Cloud Pub/Sub

C. Vehicles stream data directly to Google BigQuery

D. Vehicles continue to write data using the existing system (FTP)

    Its B

    it exceeds the streaming limit for BQ. We need to buffer, the default limit of BigQuery is 100 API calls per second, till now this cannot be changed. 


## Question 5

You analyzed TerraEarth's business requirement to reduce downtime, and found that they can achieve a majority of time saving by reducing customer's wait time for parts. You decided to focus on reduction of the 3 weeks aggregate reporting time. Which modifications to the company's processes should you recommend?

A. Migrate from CSV to binary format, migrate from FTP to SFTP transport, and develop machine learning analysis of metrics

B. Migrate from FTP to streaming transport, migrate from CSV to binary format, and develop machine learning analysis of metrics

C. Increase fleet cellular connectivity to 80%, migrate from FTP to streaming transport, and develop machine learning analysis of metrics

D. Migrate from FTP to SFTP transport, develop machine learning analysis of metrics, and increase dealer local inventory by a fixed factor

    C

    A is not correct because machine learning analysis is a good means toward the end of reducing downtime, but shuffling formats and transport doesn't directly help at all.

    B is not correct because machine learning analysis is a good means toward the end of reducing downtime, and moving to streaming can improve the freshness of the information in that analysis, but changing the format doesn't directly help at all.

    C is correct because using cellular connectivity will greatly improve the freshness of data used for analysis from where it is now, collected when the machines are in for maintenance. Streaming transport instead of periodic FTP will tighten the feedback loop even more. Machine learning is ideal for predictive maintenance workloads.

    D is not correct because machine learning analysis is a good means toward the end of reducing downtime, but the rest of these changes don't directly help at all.


## Question 6

Which of TerraEarth's legacy enterprise processes will experience significant change as a result of increased Google Cloud Platform adoption?

A. Opex/capex allocation, LAN changes, capacity planning

B. Capacity planning, TCO calculations, opex/capex allocation

C. Capacity planning, utilization measurement, data center expansion

D. Data Center expansion, TCO calculations, utilization measurement

    Correct Answer B

    Capacity planning, TCO calculations, opex/capex allocation

    From the case study, it can conclude that Management (CXO) all concern rapid provision of resources (infrastructure) for growing as well as cost management, such as Cost optimization in Infrastructure, trade up front capital expenditures (Capex) for ongoing operating expenditures (Opex), and Total cost of ownership (TCO)

## Question 7

To speed up data retrieval, more vehicles will be upgraded to cellular connections and be able to transmit data to the ETL process. The current FTP process is error-prone and restarts the data transfer from the start of the file when connections fail, which happens often. You want to improve the reliability of the solution and minimize data transfer time on the cellular connections. What should you do?

A. Use one Google Container Engine cluster of FTP servers. Save the data to a Multi-Regional bucket. Run the ETL process using data in the bucket

B. Use multiple Google Container Engine clusters running FTP servers located in different regions. Save the data to Multi-Regional buckets in US, EU, and Asia. Run the ETL process using the data in the bucket

C. Directly transfer the files to different Google Cloud Multi-Regional Storage bucket locations in US, EU, and Asia using Google APIs over HTTP(S). Run the ETL process using the data in the bucket

D. Directly transfer the files to a different Google Cloud Regional Storage bucket location in US, EU, and Asia using Google APIs over HTTP(S). Run the ETL process to retrieve the data from each Regional bucket

    D seems to be the correct answer
    Sending Data to all Multi Region Buckets >> Incurs more cost , More Latency
    Sending Data only to Regional Bucket >> Incurs less cost , Less Latency

    https://cloud.google.com/storage/docs/locations#considerations
    Regional: 200 Gbps (per region, per project)
    Multi-regional: 50 Gbps (per region, per project)

## Question 8

TerraEarth's 20 million vehicles are scattered around the world. Based on the vehicle's location, its telemetry data is stored in a Google Cloud Storage (GCS) regional bucket (US, Europe, or Asia). The CTO has asked you to run a report on the raw telemetry data to determine why vehicles are breaking down after 100 K miles. You want to run this job on all the data. What is the most cost-effective way to run this job?

A. Move all the data into 1 zone, then launch a Cloud Dataproc cluster to run the job

B. Move all the data into 1 region, then launch a Google Cloud Dataproc cluster to run the job

C. Launch a cluster in each region to preprocess and compress the raw data, then move the data into a multi-region bucket and use a Dataproc cluster to finish the job

D. Launch a cluster in each region to preprocess and compress the raw data, then move the data into a region bucket and use a Cloud Dataproc cluster to finish the job

    D

    A, B says "move all data" but analysis will try to reveal breaking down after 100K miles so there is no point of transferring data of the vehicles with less than 100K milage.
    Therefore, transferring all data is just waste of time and money.

    If we move/copy data between continents it will cost us money therefore compressing the data before copying to another region/continent makes sense.

    Preprocessing also makes sense because we probably want to process smaller chunks of data first (remember 100K milage).
    
    So now type of target bucket; multi-region or standard? multi-region is good for high-availability and low latency with a little more cost however question doesn't require any of these features.

    Therefore I think standard storage option is good to go given lower costs are always better.

## Question 9 

TerraEarth has equipped all connected trucks with servers and sensors to collect telemetry data. Next year they want to use the data to train machine learning models. They want to store this data in the cloud while reducing costs. What should they do?

A. Have the vehicle's computer compress the data in hourly snapshots, and store it in a Google Cloud Storage (GCS) Nearline bucket

B. Push the telemetry data in real-time to a streaming dataflow job that compresses the data, and store it in Google BigQuery

C. Push the telemetry data in real-time to a streaming dataflow job that compresses the data, and store it in Cloud Bigtable

D. Have the vehicle's computer compress the data in hourly snapshots, and store it in a GCS Coldline bucket

    D is most cost effective as don't want to use until 'next year'. Therefore moving the data to coldline storage makes more sense


## Question 10 

Your agricultural division is experimenting with fully autonomous vehicles. You want your architecture to promote strong security during vehicle operation. Which two architectures should you consider? (Choose two.)

A. Treat every micro service call between modules on the vehicle as untrusted.

B. Require IPv6 for connectivity to ensure a secure address space.

C. Use a trusted platform module (TPM) and verify firmware and binaries on boot.

D. Use a functional programming language to isolate code execution cycles.

E. Use multiple connectivity subsystems for redundancy.

F. Enclose the vehicle's drive electronics in a Faraday cage to isolate chips.

    AC is correct

    A. Treat every micro service call between modules on the vehicle as untrusted.
    C. Use a trusted platform module (TPM) and verify firmware and binaries on boot.

    B is not correct because IPv6 doesn't have any impact on the security during vehicle operation, although it improves system scalability and simplicity.
    D is not correct because merely using a functional programming language doesn't guarantee a more secure level of execution isolation. Any impact on security from this decision would be incidental at best.
    E is not correct because this improves system durability, but it doesn't have any impact on the security during vehicle operation.
    F is not correct because it doesn't have any impact on the security during vehicle operation, although it improves system durability.


## Question 11

Operational parameters such as oil pressure are adjustable on each of TerraEarth's vehicles to increase their efficiency, depending on their environmental conditions. Your primary goal is to increase the operating efficiency of all 20 million cellular and unconnected vehicles in the field. How can you accomplish this goal?

A. Have you engineers inspect the data for patterns, and then create an algorithm with rules that make operational adjustments automatically

B. Capture all operating data, train machine learning models that identify ideal operations, and run locally to make operational adjustments automatically

C. Implement a Google Cloud Dataflow streaming job with a sliding window, and use Google Cloud Messaging (GCM) to make operational adjustments automatically

D. Capture all operating data, train machine learning models that identify ideal operations, and host in Google Cloud Machine Learning (ML) Platform to make operational adjustments automatically

    B is correct. only 200k vehicle's are connected so need to run updates locally

    Google has announced two new products aimed at helping customers develop and deploy intelligent connected devices at scale: Edge TPU, a new hardware chip, and Cloud IoT Edge, a software stack that extends Google Cloud’s powerful AI capability to gateways and connected devices. This lets you build and train ML models in the cloud, then run those models on the Cloud IoT Edge device through the power of the Edge TPU hardware accelerator.

    - https://cloud.google.com/blog/products/gcp/bringing-intelligence-edge-cloud-iot