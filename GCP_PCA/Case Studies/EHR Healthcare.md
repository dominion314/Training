
# Company overview

EHR Healthcare is a leading provider of electronic health record software to the medical industry. EHR Healthcare provides their software as a service to multi-national medical offices, hospitals, and insurance providers.

## Solution concept

Due to rapid changes in the healthcare and insurance industry, EHR Healthcare's business has been growing exponentially year over year. They need to be able to scale their environment, adapt their disaster recovery plan, and roll out new continuous deployment capabilities to update their software at a fast pace. Google Cloud has been chosen to replace their current colocation facilities.

## Existing technical environment

EHR's software is currently hosted in multiple colocation facilities. The lease on one of the data centers is about to expire.
Customer-facing applications are web-based, and many have recently been containerized to run on a group of Kubernetes clusters. Data is stored in a mixture of relational and NoSQL databases (MySQL, MS SQL Server, Redis, and MongoDB).

EHR is hosting several legacy file and API-based integrations with insurance providers on-premises. These systems are scheduled to be replaced over the next several years. There is no plan to upgrade or move these systems at the current time.
Users are managed via Microsoft Active Directory. Monitoring is currently being done via various open source tools. Alerts are sent via email and are often ignored.

## Business requirements 

* On-board new insurance providers as quickly as possible.
* Provide a minimum 99.9% availability for all customer-facing systems.
* Provide centralized visibility and proactive action on system performance and usage.
* Increase ability to provide insights into healthcare trends.
* Reduce latency to all customers.
* Maintain regulatory compliance.
* Decrease infrastructure administration costs.
* Make predictions and generate reports on industry trends based on provider data.

## Technical requirements

* Maintain legacy interfaces to insurance providers with connectivity to both on-premises systems and cloud providers.
* Provide a consistent way to manage customer-facing applications that are container-based.
* Provide a secure and high-performance connection between on-premises systems and Google Cloud.
* Provide consistent logging, log retention, monitoring, and alerting capabilities.
* Maintain and manage multiple container-based environments.
* Dynamically scale and provision new environments.
* Create interfaces to ingest and process data from new providers.

## Executive statement

Our on-premises strategy has worked for years but has required a major investment of time and money in training our team on distinctly different systems, managing similar but separate environments, and responding to outages. Many of these outages have been a result of misconfigured systems, inadequate capacity to manage spikes in traffic, and inconsistent monitoring practices. We want to use Google Cloud to leverage a scalable, resilient platform that can span multiple environments seamlessly and provide a consistent and stable user experience that positions us for future growth.

# Questions

## Question 1

 You are responsible for ensuring that EHR's use of Google Cloud will pass an upcoming privacy compliance audit. What should you do? (Choose two.)

A. Verify EHR's product usage against the list of compliant products on the Google Cloud compliance page.

B. Advise EHR to execute a Business Associate Agreement (BAA) with Google Cloud.

C. Use Firebase Authentication for EHR's user facing applications.

D. Implement Prometheus to detect and prevent security breaches on EHR's web-based applications.

E. Use GKE private clusters for all Kubernetes workloads.

      AB

      A - OK (Google Cloud compliance page will give list of products those are HIPAA compliant https://cloud.google.com/security/compliance/offerings?skip_cache=true#/regions=USA&industries=Healthcare_and_life_sciences&focusArea=Privacy)

      B - OK (BAA means HIPAA Business Associate amendment or Business Associate Agreement entered into between Google and Customer. With EHR being a leading provider of health record software, this agreement is required. https://cloud.google.com/files/gcp-hipaa-overview-guide.pdf?hl=en)

      C - Eliminated (Firebase authentication provides backend services, easy-to-use SDKs and ready-made libraries to users on App. https://firebase.google.com/docs/auth)

      D - Eliminated (more of an observability platform)

      E - Eliminated (Running distributed services in GKE private clusters gives enterprises both secure and reliable services. Not sure how this may help with Private Compliance Audit)

## Questions 2 

 You need to define the technical architecture for securely deploying workloads to Google Cloud. You also need to ensure that only verified containers are deployed using Google Cloud services. What should you do? (Choose two.)

A. Enable Binary Authorization on GKE, and sign containers as part of a CI/CD pipeline.

B. Configure Jenkins to utilize Kritis to cryptographically sign a container as part of a CI/CD pipeline.

C. Configure Container Registry to only allow trusted service accounts to create and deploy containers from the registry.

D. Configure Container Registry to use vulnerability scanning to confirm that there are no vulnerabilities before deploying the workload.

    A & D

    Binary Authorization to ensure only verified containers are deployed
    To ensure deployment are secure and and consistent, automatically scan images for vulnerabilities with container analysis (https://cloud.google.com/docs/ci-cd/overview?hl=en&skip_cache=true)

## Question 3

You need to upgrade the EHR connection to comply with their requirements. The new connection design must support business-critical needs and meet the same network and security policy requirements. What should you do?

A. Add a new Dedicated Interconnect connection.

B. Upgrade the bandwidth on the Dedicated Interconnect connection to 100 G.

C. Add three new Cloud VPN connections.

D. Add a new Carrier Peering connection.

        A ; 99.9% availability requires 2 interconnect. 
        
        https://cloud.google.com/network-connectivity/docs/interconnect/how-to/dedicated/modifying-interconnects says

        " It is not possible to change the link type on an Interconnect connection circuit from 10 Gbps to 100 Gbps. If you want to migrate to 100 Gbps, you must first provision a new 100-Gbps Interconnect connection alongside your existing 10-Gbps connection, and then migrate the traffic onto the 100-Gbps connection."
        

## Question 4

 You need to define the technical architecture for hybrid connectivity between EHR's on-premises systems and Google Cloud. You want to follow Google's recommended practices for production-level applications. Considering the EHR Healthcare business and technical requirements, what should you do?

A. Configure two Partner Interconnect connections in one metro (City), and make sure the Interconnect connections are placed in different metro zones.

B. Configure two VPN connections from on-premises to Google Cloud, and make sure the VPN devices on-premises are in separate racks.

C. Configure Direct Peering between EHR Healthcare and Google Cloud, and make sure you are peering at least two Google locations.

D. Configure two Dedicated Interconnect connections in one metro (City) and two connections in another metro, and make sure the Interconnect connections are placed in different metro zones.

        D (based on the requirement of secure and high-performance connection between on-premises systems to Google Cloud)

        Between A and D, picked D as with Direct Connect EHR can get the bandwidth of 10 GBS to 100GBS (VPN ruled out as traffic is over internet and due to bandwidth. Direct Peering is more for Workspace rather than Google Cloud)

        "Google's recommended practices for production-level applications" 
        
        https://cloud.google.com/network-connectivity/docs/interconnect/tutorials/production-level-overview 
        
         https://cloud.google.com/network-connectivity/docs/interconnect/tutorials/non-critical-overview. 
        
        It is clear answer should be D , which is topology for production level applications recommended by Google


## Question 5 

 You are a developer on the EHR customer portal team. Your team recently migrated the customer portal application to Google Cloud. The load has increased on the application servers, and now the application is logging many timeout errors. You recently incorporated Pub/Sub into the application architecture, and the application is not logging any Pub/Sub publishing errors. You want to improve publishing latency. What should you do?

A. Increase the Pub/Sub Total Timeout retry value.

B. Move from a Pub/Sub subscriber pull model to a push model.

C. Turn off Pub/Sub message batching.

D. Create a backup Pub/Sub message queue.

    C - The cost of batching is latency for individual messages, which are queued in memory until their corresponding batch is filled and ready to be sent over the network. To minimize latency, batching should be turned off.

    https://cloud.google.com/pubsub/docs/publisher?hl=en#batching

    A incorrect. Application timeout because of publisher latency, nothing to do with timeout retry with publish request.

    D does not make sense at all.

    B is about receiver, not publisher.

## Question 6

 In the past, configuration errors put public IP addresses on backend servers that should not have been accessible from the Internet. You need to ensure that no one can put external IP addresses on backend Compute Engine instances and that external IP addresses can only be configured on frontend Compute Engine instances. What should you do?

A. Create an Organizational Policy with a constraint to allow external IP addresses only on the frontend Compute Engine instances.

B. Revoke the compute.networkAdmin role from all users in the project with front end instances.

C. Create an Identity and Access Management (IAM) policy that maps the IT staff to the compute.networkAdmin role for the organization.

D. Create a custom Identity and Access Management (IAM) role named GCE_FRONTEND with the compute.addresses.create permission.


    It's A.

    Following is from Google page - https://cloud.google.com/blog/ja/products/identity-security/limiting-public-ips-google-cloud

    "Using an Organization Policy, you can restrict external IP addresses to specific VMs with constraints to control use of external IP addresses for your VM instances within an organization or a project."

## Question 7

 You are responsible for designing the Google Cloud network architecture for Google Kubernetes Engine. You want to follow Google best practices. Considering the EHR Healthcare business and technical requirements, what should you do to reduce the attack surface?

A. Use a private cluster with a private endpoint with master authorized networks configured.

B. Use a public cluster with firewall rules and Virtual Private Cloud (VPC) routes.

C. Use a private cluster with a public endpoint with master authorized networks configured.

D. Use a public cluster with master authorized networks enabled and firewall rules.


    It should be A.

    Public endpoint access disabled is the most secure option as it prevents all internet access to the control plane. This is a good choice if you have configured your on-premises network to connect to Google Cloud using Cloud Interconnect (EHR has enabled this) or Cloud VPN.

    If you disable public endpoint access, then you must configure authorized networks for the private endpoint. If you don't do this, you can only connect to the private endpoint from cluster nodes or VMs in the same subnet as the cluster.

    Public endpoint access enabled, authorized networks enabled: This is a good choice if you need to administer the cluster from source networks that are not connected to your cluster's VPC network using Cloud Interconnect or Cloud VPN (but EHR is already using interconnect) So answer C is wrong.
    
    Reference- https://cloud.google.com/kubernetes-engine/docs/concepts/private-cluster-concept