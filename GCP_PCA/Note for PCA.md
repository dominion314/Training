# Anthos

This is a Google Cloud modern application management platform that provides consistent development/operations experience for cloud and on-premises environments.

It is for GCP, onprem, and 3rd party cloud provider meshes.

Provides management capability at the cluster level for creating, scaling, upgrading, and configuring clusters using the GKE orchestration. It doesnt manage GKE control plane or nodes, only Anthos services.

## Existing networking storage and compute

You can have a GCP, AWS, and Onprem workload operating as a cohesive structure. 

Anthos provides consitency across clusters and components. 

Anthos provides a uniform and observable expereince through multiple anthos compoenents. 

The Anthos cluster is for Infra mananagment

Anthos Ingress is for shared load balancing resources across clusters and regions

Anthos Service Mesh create managed and observable network comms across services

Anthos config management for deploying consistent configs, policies, and guard rails for security/governance.

These componenets automatically upload metrics and logs to monitoring for observvability. This way you are able to make changes and enforce policies with informed decisions to automate. 

The Environ is a google concept for managing multiple clsuters to combine and make them easier to manage. Env 1 may have the GCP and AWS environments, Env 2 may have Onprem and Other environments. 2 separate environments with 2 separate configs. This can separate services between tiers, location, teams, workloads. 

This helps to make it easier to manage your clusters in GKE. 

Cloud Run for Anthos is a serverless platform 

## Anthos Clusters

Offers same GKE experience. Provides a unified way to work with Kubernetes in multiple env

Can add conformantn non-GKE clusters but they must be consistent, have timeley updates, and portability.

Allow you to keep env in sync with the same version, patches, upgrades with no downtime, and policy enforcement.

You dont need VPNs to connect to onprem clusters. User clusters are where you deploy and run your containerized workloads and services.

Single pane of glass through cloud logging or Prometheus.

-Anthos on prem is kubernetes running on VMware or Baremetal. The software brings GKE to  onprem running in vSpehere. 
The admin cluster hold the admin and user control plane. Using baremetal runs anthos on existing hardware with no hypervisor. Runs on physical servers or data centers or edge. 

Because you control the node env you can control system sec and provide high availability. 

-Anthos on AWS Hybrid cloud software extends GKE to AWS
Anthos gke tool is used to create reosusrces from the command line
It uses aws resources -ec2,elb,ebs

-Anthos on Azure can enable a subset of anthos features such as config mgmt. 
Attach any conformant k8s cluster - eks, aks,oke,ocp,rke,kind
All clusters must be registered to an environ.




